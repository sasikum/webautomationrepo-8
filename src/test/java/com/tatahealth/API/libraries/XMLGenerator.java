package com.tatahealth.API.libraries;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.testng.TestNG;
import org.testng.annotations.Test;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

public class XMLGenerator {
	

	@Test(priority=0)
	public static void generator() throws Exception {
		
		
		XmlSuite suite = new XmlSuite();
		suite.setName("Appium Suite");

		XmlTest test = new XmlTest(suite);
		test.setName("Appium");
		test.setPreserveOrder(false);
		List<XmlClass> classes = new ArrayList<XmlClass>();
		
		List<String> list = new ArrayList<String>();
		
		//int day = Calendar.DAY_OF_YEAR;
		int day = Calendar.getInstance().get(Calendar.DAY_OF_YEAR);
		
		System.out.println(day);
		/*if(day%2==0) {
			list = SheetsAPI.getTestsToRunOnDay1();
			
		}else if(day%2==1) {
			list = SheetsAPI.getTestsToRunOnDay2();
		}
		*/
		//System.out.println(list);
		for(int i=0;i<list.size();i++) {
			classes.add(new XmlClass(list.get(i)));
		}
		
		test.setXmlClasses(classes) ;
		
		List<XmlSuite> suites = new ArrayList<XmlSuite>();
		suites.add(suite);
		TestNG tng = new TestNG();
		tng.setXmlSuites(suites);
		
		try {
			tng.run();
		}catch(Exception e) {
			e.printStackTrace();
		}
		 
		
		
		
		
		
	}
}
