package com.tatahealth.EMR.pages.PracticeManagement;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;

import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class SummmaryFlowPages {

	public WebElement moveToSummaryPDFPreference(WebDriver driver,ExtentTest logger){
		
		String xpath = "//a[contains(text(),'Summary PDF Preference')]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
		
	}
	
	public WebElement saveSummaryPDFPreference(WebDriver driver,ExtentTest logger){
		
		String xpath = "//div[@id='summaryPDFPref']//button[@class='btn btn-default takephotobtn greenbtn'][contains(text(),'Save')]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getMessage(WebDriver driver,ExtentTest logger){
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));	
		
	}
	
	
	public WebElement getGeneralAdvice(WebDriver driver,ExtentTest logger){
		
		String xpath = "//li[@id='General Advice']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));	
	
	}
	
	
	public WebElement getDiagnosis(WebDriver driver,ExtentTest logger){
		
		String xpath = "//li[@id='Diagnosis']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));	
		
	}
	
	public WebElement getPrescription(WebDriver driver,ExtentTest logger){
		
		String xpath = "//li[@id='Prescription']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getCaseSheet(WebDriver driver,ExtentTest logger) {
		String xpath ="//li[@id='Examination']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getLabTests(WebDriver driver,ExtentTest logger) {
		String xpath ="//li[@id='Lab Tests']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getFollowup(WebDriver driver,ExtentTest logger) {
		String xpath ="//li[@id='Follow up']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	

	public WebElement getVitals(WebDriver driver,ExtentTest logger) {
		String xpath ="//li[@id='Vitals']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getSymptoms(WebDriver driver,ExtentTest logger) {
		String xpath ="//li[@id='Symptoms']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getMedicalHistory(WebDriver driver,ExtentTest logger) {
		String xpath ="//li[@id='Medical History']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getReferral(WebDriver driver,ExtentTest logger) {
		String xpath ="//li[@id='Referral']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	
	}
	
	public List<String> getAllSummaryPdfElementNamesAsList(WebDriver driver,ExtentTest logger) throws Exception {
		String xpath ="//div[@id='summaryPDFPref']//ul/li";
		List<String> summaryPdfElements = new ArrayList<>();
		List<WebElement> summaryPdfWebElements = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
		for(WebElement element : summaryPdfWebElements) {
			summaryPdfElements.add(Web_GeneralFunctions.getText(element,"extracting elements text" , driver, logger).trim());
		}
		return summaryPdfElements;
		
	}
	
	public List<WebElement> getAllSummaryPdfElementsAsList(WebDriver driver,ExtentTest logger) {
		String xpath ="//div[@id='summaryPDFPref']//ul/li";
		
		return(Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger));
		
		
		
	}
	
	public WebElement getReferralElementInConsultationPage(WebDriver driver,ExtentTest logger) {
		return (Web_GeneralFunctions.findElementbyXPath("//ul[@id='fixedmenu']//li[@data-input='Refer']", driver, logger));
	}
	
	public WebElement getFollowupCheckbox(WebDriver driver,ExtentTest logger) {
		return (Web_GeneralFunctions.findElementbyXPath("//span[@class='checkmarkfollowup']", driver, logger));
	}
	
	public WebElement getFollowupInputField(WebDriver driver,ExtentTest logger) {
		String xpath ="//input[@id='consultationFollowUpInput']";
		return Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
	}
	
	public WebElement getFirstDoctor(WebDriver driver,ExtentTest logger) {
		return (Web_GeneralFunctions.findElementbyXPath("(//ul[@role='listbox' and @aria-expanded='true']//li)[2]", driver, logger));
	}
	
	public WebElement getGeneralNotesTextArea(WebDriver driver,ExtentTest logger) {
		return (Web_GeneralFunctions.findElementbyXPath("(//label[contains(text(),'Notes')]//ancestor::div[@class='examinationCasesheet']//textarea)[1]", driver, logger));
	}
	
	public WebElement getDuplicateMedicineDialogBox(WebDriver driver,ExtentTest logger) {
		return (Web_GeneralFunctions.findElementbyXPath("//div[@aria-describedby='duplicateMedicineDialogBox']", driver, logger));
	}
	
	public WebElement getDuplicateMedicineNoButton(WebDriver driver,ExtentTest logger) {
		return (Web_GeneralFunctions.findElementbyXPath("//div[@aria-describedby='duplicateMedicineDialogBox']//button[contains(text(),'No')]", driver, logger));
	}
	
	public WebElement getPrintinConsultationPage(WebDriver driver,ExtentTest logger) {
		return (Web_GeneralFunctions.findElementbyXPath("//li[@data-input='Print']", driver, logger));
	}
	
	public WebElement getPrintAllinConsultationPage(WebDriver driver,ExtentTest logger) {
		return (Web_GeneralFunctions.findElementbyXPath("//a[contains(text(),' Print All ')]", driver, logger));
	}
	
	public WebElement getFirstSpecility(WebDriver driver,ExtentTest logger) {
		return (Web_GeneralFunctions.findElementbyXPath("(//ul[@role='listbox']//li)[1]", driver, logger));
	}
	
}
