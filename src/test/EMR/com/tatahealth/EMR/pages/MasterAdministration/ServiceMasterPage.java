package com.tatahealth.EMR.pages.MasterAdministration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class ServiceMasterPage extends MedicalCertificatesPage{

	
	public WebElement getServiceMasterTab(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("//a[contains(text(),'Service Master ')]", driver, logger);
		return tablink;	
	}
	
	
	public WebElement getAddServicelink(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("//a[contains(text(),'Add New Service')]", driver, logger);
		return tablink;	
	}
	
	public WebElement getServiceCategorySelBox(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("//select[@id='serviceCategory']", driver, logger);
		return tablink;	
	}
	
	public WebElement getServiceClassifSelBox(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("//select[@id='classificationId']", driver, logger);
		return tablink;	
	}
	
	public WebElement getServiceNametxtBox(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("//input[@id='serviceName']", driver, logger);
		return tablink;	
	}
	
	public WebElement getServiceNametxtBox_opts(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("(//div[contains(text(),'Cervical spine (AP / Leteral) Each')])[1]", driver, logger);
		return tablink;	
	}
	
	public WebElement getServiceTypeSelBox(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("//select[@id='serviceType']", driver, logger);
		return tablink;	
	}
	
	public WebElement getService_ChargableSbx(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("//select[@id='chargable']", driver, logger);
		return tablink;	
	}
	
	public WebElement getServiceResultApplSbx(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("//select[@id='resultApplicable']", driver, logger);
		return tablink;	
	}
	
	public WebElement getServiceStatusSbx(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("//select[@id='status']", driver, logger);
		return tablink;	
	}
	
	public WebElement getServiceTariffAmttxtbx(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("//input[@id='tariff']", driver, logger);
		return tablink;	
	}
	
	public WebElement getServiceDoctorMultiSelect(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("//button[@data-id='doctorMultiSelect']", driver, logger);
		return tablink;	
	}
	
	public WebElement getServicedoctorSearchbx(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("//input[@class='form-control' and @role='textbox']", driver, logger);
		return tablink;	
	}
	
	public WebElement getServiceDoctorOptions(WebDriver driver,ExtentTest logger, String doctor) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("//span[contains(text(),'"+doctor+"')]", driver, logger);
		return tablink;	
	}
	
	public WebElement getServiceSaveBtn(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("(//button[contains(text(),'Save')])[2]", driver, logger);
		return tablink;	
	}
	
	public WebElement getServiceClosebtn(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("(//button[contains(text(),'Close')])[4]", driver, logger);
		return tablink;	
	}
	
	public WebElement getServiceSelectAllbtn(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("//button[contains(text(),'Select All')]", driver, logger);
		return tablink;	
	}
	

	public WebElement getServiceDeSelectAllbtn(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("//button[contains(text(),'Deselect All')]", driver, logger);
		return tablink;	
	}
	
	public WebElement getServiceNoResulttxt(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("//li[contains(text(),'No results matched')]", driver, logger);
		return tablink;	
	}
	
	public WebElement getServiceList_Search(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("//input[@placeholder='Search for service']", driver, logger);
		return tablink;	
	}
	
	public WebElement getServiceList_filter_service(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("//th[contains(text(),'Service')]", driver, logger);
		return tablink;	
	}
	
	public WebElement getServiceList_filter_cat(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("//th[contains(text(),'Category')]", driver, logger);
		return tablink;	
	}
	
	public WebElement getServiceList_filter_class(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("//th[contains(text(),'Classification')]", driver, logger);
		return tablink;	
	}
	
	public WebElement getServiceList_filter_tariff(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("//th[contains(text(),'Tariff')]", driver, logger);
		return tablink;	
	}
	
	public WebElement getServiceList_Edit(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("(//a[contains(text(),'Edit')])[1]", driver, logger);
		return tablink;	
	}
	
	public WebElement getServiceList_dropdown(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("//select[@name='servicemastertable_length']", driver, logger);
		return tablink;	
	}
	
	public WebElement getServiceList_UpdateServicePoPup(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("//h4[contains(text(),'Update Service')]", driver, logger);
		return tablink;	
	}
	
	public WebElement getServiceList_popupClosebtn(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("(//button[contains(text(),'Close')])[5]", driver, logger);
		return tablink;	
	}
	
	public WebElement getServiceList_PoPupSavebtn(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("(//button[contains(text(),'Save')])[3]", driver, logger);
		return tablink;	
	}
	
	public WebElement getServiceList_nextbtn(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("//a[contains(text(),'Next')]", driver, logger);
		return tablink;	
	}
	
	public WebElement getServiceList_previousbtn(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("//a[contains(text(),'Previous')]", driver, logger);
		return tablink;	
	}
	
	public WebElement getService_toastmsg(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("//div[contains(text(),'Service Name already exists.')]", driver, logger);
		return tablink;	
	}
	
	public WebElement getService_mandatorycheck(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("//div[contains(text(),'Please enter a valid service Name')]", driver, logger);
		return tablink;	
	}
	
	public WebElement getService_searchresult(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("(//td[contains(text(),' Cervical')])[1]", driver, logger);
		return tablink;	
	}
	
	public WebElement getService_searchresult1(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("//td[contains(text(),'No matching records found')]", driver, logger);
		return tablink;	
	}
	
	public WebElement getService_ServiceName_edit(WebDriver driver,ExtentTest logger, String servicename) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("//td[contains(text(),'"+servicename+"')]//following-sibling::td//following-sibling::td//following-sibling::td//following-sibling::td//a", driver, logger);
		return tablink;	
	}
	
	public WebElement getbilling(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("//li[@name='billing']//a", driver, logger);
		return tablink;	
	}
	
	public WebElement getbilling_servicebox(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("//input[@id='service0']", driver, logger);
		return tablink;	
	}
	
	public WebElement getbilling_servicebox_error(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("//li[contains(text(),'No matching service found')]", driver, logger);
		return tablink;	
	}
	
}
