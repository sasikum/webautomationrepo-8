package com.tatahealth.EMR.pages.Consultation;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class GeneralAdvicePage {

	public WebElement getGeneralAdvice(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//label[contains(text(),'General Advice')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getGeneralAdviceTextArea(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//textarea[@id='genericAdvice']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement saveGeneralAdvice(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//ul[@id='fixedmenu']/li[@data-input='Print']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getAddAsTemplate(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='generalAdviceTemplateId']/div[6]/button";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	
	public WebElement saveTemplatePopUp(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//h4[contains(text(),'Save General Advice Template')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement saveTemplateButton(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='generalAdviceTemplateNameModal']//button[@id='submitTemplateForm']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement generalAdviceTemplateName(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='generalAdviceTemplateName']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement generalAdviceTemplateClosePopUp(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='generalAdviceTemplateNameModal']//button[@class='close']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement generalAdviceTemplateDropDown(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//button[@id='dropdown_btn']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement selectTemplate(String value,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//button[@id='dropdown_btn']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);

		Web_GeneralFunctions.click(element, "Click drop down button", driver, logger);
		
		if(!value.isEmpty()) {
			xpath = "//input[@id='search']";
			element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
			Web_GeneralFunctions.sendkeys(element, value, "Pass input value", driver, logger);
		}
		
		xpath = "//ul[@id='dropdown']/li";
		List<WebElement> gaList = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
		Random r = new Random();
		System.out.println("list size : "+gaList.size());
		Integer index = r.nextInt(gaList.size());
		if(index == 0) {
			index+=1;
		}
		System.out.println("select template index : "+index);
		xpath = "//ul[@id='dropdown']/li["+index+"]/input";
		element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getAddButton(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//button[@id='toggleTemplate']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
     public WebElement getGlobalPrintTick(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//a[@class='thumbnail print-section global-print-section globalPrintTick' and @id='GenericAdvicePrintSelection']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	
	
	//public WebElement searchAndSelectTemplate(WebDriver driver,ExtentTest)
	public WebElement selectTemplateLabel(String value,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//button[@id='dropdown_btn']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		Web_GeneralFunctions generalFunctions = new Web_GeneralFunctions();
		generalFunctions.click(element, "Click drop down button", driver, logger);

		String text = "";
		
		/*	xpath = "//input[@id='search']";
			element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
			Web_GeneralFunctions.sendkeys(element, value, "Pass input value", driver, logger);*/
		
			xpath = "//ul[@id='dropdown']/li";
			//xpath = "//div[@id='Prescription']//li";
			List<WebElement> list = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
			
			int i = 0;
			int numb = 1;
			while(i<list.size()) {
				element = list.get(i);
				numb = i+1;
				xpath = "//ul[@id='dropdown']/li["+numb+"]/label"; 
				element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
				text = Web_GeneralFunctions.getText(element, "get label message", driver, logger);
				if(text.equalsIgnoreCase(value)) {
					xpath = "//ul[@id='dropdown']/li["+numb+"]/input";
					element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
					break;
				}
				i++;
				numb++;
				
			}
			
	
		return element;
	}
}
