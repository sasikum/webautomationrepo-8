package com.tatahealth.EMR.pages.Consultation;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class FollowUpPage {

	public WebElement moveToFollowUp(WebDriver driver,ExtentTest logger) throws Exception{
		
		String xpath = "//ul[@id='fixedmenu']/li[@data-input='Refer']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getFollowUpField(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='followUpAfter']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement saveFollowUpChanges(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//ul[@id='fixedmenu']/li[@data-input='Print']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getSuccessMessage(WebDriver driver,ExtentTest logger)throws Exception{
		

		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement selectFollowUpOption(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//select[@id='followUpUnitOptions']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
}
