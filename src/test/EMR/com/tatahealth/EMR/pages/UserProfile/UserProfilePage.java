package com.tatahealth.EMR.pages.UserProfile;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class UserProfilePage {
	
	public WebElement getUserInformationIcon(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement userInformationIcon = Web_GeneralFunctions.findElementbySelector("#userinformationpopup",driver,logger);
   			return userInformationIcon;	
   	}
  //********************************** Use Profile module ************************************************************************ //
	public WebElement getuserProfileLink(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement userProfileLink = Web_GeneralFunctions.findElementbyXPath("//a[contains(text(),'User Profile')]", driver, logger);
    		return userProfileLink;	
   	}
	public WebElement getServicesOfferedTbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement servicesOfferedTbx = Web_GeneralFunctions.findElementbySelector("#up_servicesOffered",driver,logger);
   			return servicesOfferedTbx;	
   	}
	
	public WebElement getSaveBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement saveBtn = Web_GeneralFunctions.findElementbySelector("#saveUserProfile",driver,logger);
   			return saveBtn;	
   	}
	
	public WebElement getUserProfileSuccessMessgage(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement userProfileSuccessMessgage = Web_GeneralFunctions.findElementbyXPath("//div[contains(text(),'User details have been updated successfully')]", driver, logger);
    		return userProfileSuccessMessgage;	
   	}
	
	public WebElement getUserProfileCloseBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement userProfileCloseBtn = Web_GeneralFunctions.findElementbySelector("#canceluserprofile",driver,logger);
   			return userProfileCloseBtn;	
   	}
	
	public WebElement getUserIdTbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement UerIdTbx = Web_GeneralFunctions.findElementbyXPath("//label[contains(text(),'User Id')]/following-sibling::input", driver, logger);
    		return UerIdTbx;	
   	}
	
	//********************************************************* ENDs **********************************************************************************//
	
	// ****************************************************** Add Timings ********************************************************************//
	public WebElement getMyTimingslink(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement myTimings = Web_GeneralFunctions.findElementbyXPath("//a[contains(text(),'My Timings')]", driver, logger);
    		return myTimings;	
   	}
	
	public WebElement getAddTimingBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement addTiming = Web_GeneralFunctions.findElementbyXPath("//a[@id='addtimengspop']", driver, logger);
    		return addTiming;	
   	}
	
	
	public WebElement getTimeFromDateTbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement expFromDate = Web_GeneralFunctions.findElementbyXPath("//input[@id='fromDateTimings']", driver, logger);
    		return expFromDate;	
   	}
	
	public WebElement getTimeToDateTbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement exp = Web_GeneralFunctions.findElementbyXPath("//input[@id='toDateTimings']", driver, logger);
    		return exp;	
   	}
	
	public WebElement getMonBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement exp = Web_GeneralFunctions.findElementbyXPath("//button[@id='monday_timing']", driver, logger);
    		return exp;	
   	}
	
	public WebElement getMonStartTimeBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement exp = Web_GeneralFunctions.findElementbyXPath("//input[@id='Monday_start_time']", driver, logger);
    		return exp;	
   	}
	
	public WebElement getMonEndTimeBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement exp = Web_GeneralFunctions.findElementbyXPath("//input[@id='end_time']", driver, logger);
    		return exp;	
   	}
	
	public WebElement getF2FCbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement exp = Web_GeneralFunctions.findElementbyXPath("//td[@class='Mondaycheckexists checkexists']//label[contains(text(),'F2F')]/../input", driver, logger);
    		return exp;	
   	}
	
	public WebElement getVCCbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement exp = Web_GeneralFunctions.findElementbyXPath("//td[@class='Mondaycheckexists checkexists']//label[contains(text(),'VC')]/../input", driver, logger);
    		return exp;	
   	}
	
	public WebElement getTueBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement exp = Web_GeneralFunctions.findElementbyXPath("//button[@id='tuesday_timing']", driver, logger);
    		return exp;	
   	}
	
	public WebElement getWedBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement exp = Web_GeneralFunctions.findElementbyXPath("//button[@id='wednesday_timing']", driver, logger);
    		return exp;	
   	}
	
	public WebElement getThuBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement exp = Web_GeneralFunctions.findElementbyXPath("//button[@id='thursday_timing']", driver, logger);
    		return exp;	
   	}
	public WebElement getFriBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement exp = Web_GeneralFunctions.findElementbyXPath("//button[@id='friday_timing']", driver, logger);
    		return exp;	
   	}
	
	public WebElement getSatBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement exp = Web_GeneralFunctions.findElementbyXPath("//button[@id='saturday_timing']", driver, logger);
    		return exp;	
   	}
	
	public WebElement getSunBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement exp = Web_GeneralFunctions.findElementbyXPath("//button[@id='sunday_timing']", driver, logger);
    		return exp;	
   	}
	
	public WebElement getAddRowTimeBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement exp = Web_GeneralFunctions.findElementbyXPath("//a[@id='add_row_timeings']", driver, logger);
    		return exp;	
   	}
	
	
	public WebElement getWeekdaysBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement weekDays = Web_GeneralFunctions.findElementbyXPath("//a[@id='copyTimingToAllWeekdays']", driver, logger);
    		return weekDays;	
   	}
	
	public WebElement getAllDaysBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement allDays = Web_GeneralFunctions.findElementbyXPath("//a[@id='copyTimingToAlldays']", driver, logger);
    		return allDays;	
   	}
	
	
	public WebElement getSaveTimingsBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement saveTime = Web_GeneralFunctions.findElementbyXPath("//a[@id='saveMyTimings']", driver, logger);
    		return saveTime;
    				}
	
	public WebElement getRemoveTimingsBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement removeTime = Web_GeneralFunctions.findElementbyXPath("//span[@class='glyphicon glyphicon-remove']", driver, logger);
    		return removeTime;	
   	}
	
	public WebElement getTimeDeleteBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement deleteTime = Web_GeneralFunctions.findElementbyXPath("//a[@id='delete_timing_row']//center", driver, logger);
    		return deleteTime;	
   	}
	
	public WebElement getTimeEditBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement editTime = Web_GeneralFunctions.findElementbyXPath("(//span[contains(@class,'glyphicon glyphicon-edit userPointer')])[1]", driver, logger);
    		return editTime;	
   	}
	
	
	
	// ****************************************************** ends ********************************************************************//

	
	public WebElement getYesBtnOnAlt(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement yesBtn = Web_GeneralFunctions.findElementbyXPath("//button[@class='confirm']", driver, logger);
    		return yesBtn;	
   	}
	
	// ****************************************************** Add  Qualification ********************************************************************//
	public WebElement getMyQualificationslink(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement MyQualifications = Web_GeneralFunctions.findElementbyXPath("//a[contains(text(),'My Qualifications')]", driver, logger);
    		return MyQualifications;	
   	}
	
	public WebElement getAddQualificationBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement addQualification = Web_GeneralFunctions.findElementbyXPath("//a[contains(text(),'Add Qualification')]", driver, logger);
    		return addQualification;	
   	}
	
	public WebElement getQuaInstitutionNameTbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement qua_institutionName = Web_GeneralFunctions.findElementbyXPath("//input[@id='Qua_institutionName']", driver, logger);
    		return qua_institutionName;	
   	}
	
	
	
	public WebElement getBoardTbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement BoardTbx = Web_GeneralFunctions.findElementbyXPath("//input[@id='Qua_board']", driver, logger);
    		return BoardTbx;	
   	}
	public WebElement getQuaFromDateTbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement QuaFromDateTbx = Web_GeneralFunctions.findElementbyXPath("//input[@id='Qua_fromDate']", driver, logger);
    		return QuaFromDateTbx;	
   	}
	
	public WebElement getQuaToDateTbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement qua_toDate = Web_GeneralFunctions.findElementbyXPath("//input[@id='Qua_toDate']", driver, logger);
    		return qua_toDate;	
   	}
	
	public WebElement getQuaUniversityTbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement qua_university = Web_GeneralFunctions.findElementbyXPath("//input[@id='Qua_university']", driver, logger);
    		return qua_university;	
   	}
	
	public WebElement getQuaCourseTbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement qua_course = Web_GeneralFunctions.findElementbyXPath("//input[@id='Qua_course']", driver, logger);
    		return qua_course;	
   	}
	
	public WebElement getQuaDegreeTbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement qua_degree = Web_GeneralFunctions.findElementbyXPath("//input[@id='Qua_degree']", driver, logger);
    		return qua_degree;	
   	}
	
	public WebElement getQuaSpecializationTbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement qua_specialization = Web_GeneralFunctions.findElementbyXPath("//input[@id='Qua_specialization']", driver, logger);
    		return qua_specialization;	
   	}
	
	public WebElement getQuaSaveBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement quaSaveBtn = Web_GeneralFunctions.findElementbyXPath("//a[@id='addQualificationDetails']", driver, logger);
    		return quaSaveBtn;	
   	}
	
	
	public WebElement getQuaDeleteeBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement quaDeleteBtn = Web_GeneralFunctions.findElementbyXPath("(//a[@id='delete_qua']//center)[1]", driver, logger);
    		return quaDeleteBtn;	
   	}
	
	
	public WebElement getQuaEditBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement quaEditBtn = Web_GeneralFunctions.findElementbyXPath("//a[@id='edit_qua_0']//center", driver, logger);
    		return quaEditBtn;	
   	}
	
	
	// ****************************************************** Add  Qualification ends********************************************************************//

	// ****************************************************** Add  Publication ********************************************************************//
	public WebElement getMyPublicationslink(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement myPublications = Web_GeneralFunctions.findElementbyXPath("//a[contains(text(),'My Publications')]", driver, logger);
    		return myPublications;	
   	}
	
	public WebElement getAddPublicationsBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement addPublications = Web_GeneralFunctions.findElementbyXPath("//a[contains(text(),'Add Publication')]", driver, logger);
    		return addPublications;	
   	}
	
	public WebElement getPubTopicTbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement pubTopic = Web_GeneralFunctions.findElementbyXPath("//input[@id='Pub_topic']", driver, logger);
    		return pubTopic;	
   	}
	
	public WebElement gePubPublicationDatetTbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement pubPublicationDate = Web_GeneralFunctions.findElementbyXPath("//input[@id='Pub_publicationDate']", driver, logger);
    		return pubPublicationDate;	
   	}
	
	
	
	public WebElement getPubJournalNameTbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement pubJournalName = Web_GeneralFunctions.findElementbyXPath("//input[@id='Pub_journalName']", driver, logger);
    		return pubJournalName;	
   	}
	
	public WebElement getPubInstitutionNameTbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement institutionName = Web_GeneralFunctions.findElementbyXPath("//input[@id='Pub_institutionName']", driver, logger);
    		return institutionName;	
   	}
	
	public WebElement getPubbstractTbAx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement pubAbstract = Web_GeneralFunctions.findElementbyXPath("//input[@id='Pub_abstract']", driver, logger);
    		return pubAbstract;	
   	}
	
	
	public WebElement getPubSaveBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement pubSave = Web_GeneralFunctions.findElementbyXPath("//a[@id='addPublicationDetails']", driver, logger);
    		return pubSave;	
   	}
	
	
	
	
	public WebElement getPubEditBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement pubEditBtn = Web_GeneralFunctions.findElementbyXPath("//a[@id='edit_pub_0']//center", driver, logger);
    		return pubEditBtn;	
   	}
	
	
	public WebElement getPubDeleteBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement pubDetele = Web_GeneralFunctions.findElementbyXPath("//a[@id='delete_pub']//center", driver, logger);
    		return pubDetele;	
   	}
	
	
	
	// ****************************************************** Add  Publication ends********************************************************************//
	
	// ****************************************************** add Experiences *****************************************************************//
	
	
	
	
	public WebElement getMyExperienceslink(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement myExperiences = Web_GeneralFunctions.findElementbyXPath("//a[contains(text(),'My Experiences')]", driver, logger);
    		return myExperiences;	
   	}
	
	
	public WebElement getAddExperienceBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement addExperience = Web_GeneralFunctions.findElementbyXPath("//a[contains(text(),'Add Experience')]", driver, logger);
    		return addExperience;	
   	}
	public WebElement getExpOrganisationTbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement expOrganisation = Web_GeneralFunctions.findElementbyXPath("//input[@id='Exp_org']", driver, logger);
    		return expOrganisation;	
   	}
	public WebElement getExpRoleTbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement expRole = Web_GeneralFunctions.findElementbyXPath("//input[@id='Exp_role']", driver, logger);
    		return expRole;	
   	}
	public WebElement getExpFromDateTbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement expFromDate = Web_GeneralFunctions.findElementbyXPath("//input[@id='Exp_fromDate']", driver, logger);
    		return expFromDate;	
   	}
	public WebElement getExpToDateTbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement exp = Web_GeneralFunctions.findElementbyXPath("//input[@id='Exp_toDate']", driver, logger);
    		return exp;	
   	}
	public WebElement getExpRemarksTbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement expRemarks = Web_GeneralFunctions.findElementbyXPath("//input[@id='Exp_remarks']", driver, logger);
    		return expRemarks;	
   	}
	
	public WebElement getExpSaveBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement expSave = Web_GeneralFunctions.findElementbyXPath("//a[@id='addExperienceDetails']", driver, logger);
    		return expSave;	
   	}
	
	public WebElement getExpEditBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement expEdit = Web_GeneralFunctions.findElementbyXPath("//a[@id='edit_exp_0']//center", driver, logger);
    		return expEdit;	
   	}
	
	public WebElement getExpDeleteBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement expDelete = Web_GeneralFunctions.findElementbyXPath("//a[@id='delete_exp']//center", driver, logger);
    		return expDelete;	
   	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// ************************************************** ******** ENDs Add Experience **********************************************************//

}
