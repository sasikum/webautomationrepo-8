
package com.tatahealth.EMR.Scripts.PracticeManagement;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;


import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.API.libraries.SheetsAPI;
import com.tatahealth.EMR.Scripts.Consultation.ConsultationTest;
import com.tatahealth.EMR.Scripts.Consultation.DiagnosisTest;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.AppointmentDashboard.AppointmentsPage;
import com.tatahealth.EMR.pages.Billing.Billing;
import com.tatahealth.EMR.pages.Consultation.DiagnosisPage;
import com.tatahealth.EMR.pages.Consultation.GlobalPrintPage;
import com.tatahealth.EMR.pages.Consultation.SymptomPage;
import com.tatahealth.EMR.pages.Consultation.VitalsPage;

import com.tatahealth.EMR.pages.PracticeManagement.MedicalKitPages;
import com.tatahealth.EMR.pages.PracticeManagement.PracticeManagementPages;
import com.tatahealth.EMR.pages.PracticeManagement.SummmaryFlowPages;
import com.tatahealth.EMR.pages.PracticeManagement.VitalMasterPages;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.ReusableModules.Web_Testbase;

public class VitalMasterTest {
	
	public static String checkedVitals = "";
	public static String vitalsInConsultationPage = "";
	VitalMasterPages vital = new VitalMasterPages();
	VitalsPage vp = new VitalsPage();
	DiagnosisPage dp = new DiagnosisPage();
	GlobalPrintPage gpp = new GlobalPrintPage();
	SymptomPage sp = new SymptomPage();
	AppointmentsPage ap = new AppointmentsPage();
	PracticeManagementTest pmt = new PracticeManagementTest();
	ExtentTest logger;
	
	
		@BeforeClass(alwaysRun=true,groups= {"Regression"})
		public static void beforeClass() throws Exception {
			Login_Doctor.executionName="EMR_PracticeManagement_VitalsMaster";
			Login_Doctor.LoginTest();
			new MedicalKItTest().closePreviousConsultations();
			new PracticeManagementTest().moveToPracticeManagement();
		} 
		
		@AfterClass(alwaysRun=true,groups = {"Regression"})
		public static void afterClass(){
			Login_Doctor.LogoutTest();
		}
		
		//Test case PM_67 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=472)
	public synchronized void validateVitalsMasterModule()throws Exception{
		logger = Reports.extent.createTest("EMR move to vital master module");
		Web_GeneralFunctions.isDisplayed(vital.getVitalMasterMenu(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(vital.getVitalMasterMenu(Login_Doctor.driver, logger), "Get vital master menu", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, vital.getSaveButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, vital.getBloodPressureCheckbox(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, vital.getBpSittingCheckbox(Login_Doctor.driver, logger));
	}
	
	//PM_68 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=473)
	public synchronized void validateVitalsInVitalsMaster() throws Exception {
		logger = Reports.extent.createTest("EMR validate vital names in vitals master");
		assertTrue(vital.getDisplayedVitalNamesAsList(Login_Doctor.driver, logger).equals(vital.getActualVitalNamesAsList()));
	}
	
	//PM_69 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=474)
	public synchronized void selectVitals()throws Exception{
		logger = Reports.extent.createTest("EMR select Vitals from vital master");
		if(!Web_GeneralFunctions.isChecked(vital.getHeadCircumferenceCheckbox(Login_Doctor.driver, logger), Login_Doctor.driver)) {
			Web_GeneralFunctions.click(vital.getHeadCircumFerence(Login_Doctor.driver, logger), "select head circumference from vital master menu", Login_Doctor.driver, logger);
			assertTrue(Web_GeneralFunctions.isChecked(vital.getHeadCircumferenceCheckbox(Login_Doctor.driver, logger), Login_Doctor.driver));
		}
		if(!Web_GeneralFunctions.isChecked(vital.getTemporalTemparatureCheckbox(Login_Doctor.driver, logger), Login_Doctor.driver)) {
			Web_GeneralFunctions.click(vital.getTemporalTemparatureCheckbox(Login_Doctor.driver, logger), "Select temporal temperature vital", Login_Doctor.driver, logger);
			assertTrue(Web_GeneralFunctions.isChecked(vital.getTemporalTemparatureCheckbox(Login_Doctor.driver, logger), Login_Doctor.driver));
		}
		if(!Web_GeneralFunctions.isChecked(vital.getBpSittingCheckbox(Login_Doctor.driver, logger),Login_Doctor.driver)) {
			Web_GeneralFunctions.click(vital.getBpSittingCheckbox(Login_Doctor.driver, logger),"select BP sitting vital",Login_Doctor.driver,logger);
			assertTrue(Web_GeneralFunctions.isChecked(vital.getBpSittingCheckbox(Login_Doctor.driver, logger),Login_Doctor.driver));
		}
		if(Web_GeneralFunctions.isChecked(vital.getBloodPressureCheckbox(Login_Doctor.driver, logger), Login_Doctor.driver)) {
			Web_GeneralFunctions.click(vital.getBloodPressureCheckbox(Login_Doctor.driver, logger), "uncheck bloodpressure checkbox", Login_Doctor.driver, logger);
			assertFalse(Web_GeneralFunctions.isChecked(vital.getBloodPressureCheckbox(Login_Doctor.driver, logger),Login_Doctor.driver));
		}
		if(Web_GeneralFunctions.isChecked(vital.getRespirationRateCheckbox(Login_Doctor.driver, logger), Login_Doctor.driver)) {
			Web_GeneralFunctions.click(vital.getRespirationRateCheckbox(Login_Doctor.driver, logger), "uncheck respiration checkbox", Login_Doctor.driver, logger);
			assertFalse(Web_GeneralFunctions.isChecked(vital.getRespirationRateCheckbox(Login_Doctor.driver, logger),Login_Doctor.driver));
		}
		if(Web_GeneralFunctions.isChecked(vital.getPulseRateCheckbox(Login_Doctor.driver, logger), Login_Doctor.driver)) {
			Web_GeneralFunctions.click(vital.getPulseRateCheckbox(Login_Doctor.driver, logger), "uncheck pulse rate checkbox", Login_Doctor.driver, logger);
			assertFalse(Web_GeneralFunctions.isChecked(vital.getPulseRateCheckbox(Login_Doctor.driver, logger),Login_Doctor.driver));
		}
	}
	
	//PM_73 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=475)
	public synchronized void saveSuccessfullyVitalMaster()throws Exception{
		logger = Reports.extent.createTest("EMR save vital master");
		saveVitalMaster();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, vital.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(vital.getMessage(Login_Doctor.driver, logger), "Get message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Vital Parameter selections updated successfully"));
	}

	//PM_74 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=476)
	public synchronized void verifyVitalsInConsultationPage()throws Exception{
		logger = Reports.extent.createTest("EMR vital master changes reflect in consultation page");
		ConsultationTest.UHID=SheetsAPI.getDataProperties(Web_Testbase.input+".patientUHID");
		pmt.clickOrgHeader();
		startConsultation();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.moveToDiagnosisModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
		assertTrue(vp.isVitalsDisplayed("Head Circumference", Login_Doctor.driver, logger));
		assertTrue(vp.isVitalsDisplayed("Temperature (Temporal)", Login_Doctor.driver, logger));
		assertTrue(vp.isVitalsDisplayed("BP - Sitting", Login_Doctor.driver, logger));
		assertFalse(vp.isVitalsDisplayed("Respiration Rate", Login_Doctor.driver, logger));
		assertFalse(vp.isVitalsDisplayed("Pulse Rate", Login_Doctor.driver, logger));
		assertFalse(vp.isVitalsDisplayed("Blood Pressure", Login_Doctor.driver, logger));
		backToVitalMaster();
	}
	
	// PM_70,PM_71 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=477)
	public synchronized void selectAllVitalsPresentInVitalsMaster()throws Exception{
		logger = Reports.extent.createTest("EMR select all elements in vital master");
		int i =0;
		int j =1;
		String value = "";
		int row = 1;
		while(i<22) {
			if(!vital.getVitalCheckedStatus(i, Login_Doctor.driver)) {
				
				Web_GeneralFunctions.click(vital.getVitalElements(i, Login_Doctor.driver, logger), "check vital", Login_Doctor.driver, logger);
				Web_GeneralFunctions.wait(1);
				assertTrue(Web_GeneralFunctions.isChecked(vital.getVitalElements(i, Login_Doctor.driver, logger), Login_Doctor.driver));
			}
			value = Web_GeneralFunctions.getText(vital.getVitalValues(row, j, Login_Doctor.driver, logger), "Get vital name", Login_Doctor.driver, logger);
			if(value.equalsIgnoreCase("SpO2%")) {
				value = "SpO2";
			}
			checkedVitals += value+" ";
			Web_GeneralFunctions.wait(2);
			
			i++;
			j++;
			if(j>2) {
				j=1;
				row++;
			}
		}
		saveSuccessfullyVitalMaster();
	}
	
	//PM_74 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=478)
	public synchronized void getDisplayedVitalElementInConsultationPage()throws Exception{
		
		logger = Reports.extent.createTest("EMR select all elements in vital master");
		ConsultationTest.UHID=SheetsAPI.getDataProperties(Web_Testbase.input+".patientUHID");
		pmt.clickOrgHeader();
		startConsultation();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.moveToDiagnosisModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver,sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger) );
		Web_GeneralFunctions.scrollElementToCenter(vp.getHeightElement(Login_Doctor.driver, logger), Login_Doctor.driver);
		int i =1;
		while(i<=22) {
			vitalsInConsultationPage += Web_GeneralFunctions.getText(vital.getVitalNameFromConsultationPage(i, Login_Doctor.driver, logger), "Vital Name from consultation page", Login_Doctor.driver, logger)+" ";
			Web_GeneralFunctions.wait(1);
			
			i++;
		}
		backToVitalMaster();
		assertTrue(checkedVitals.equalsIgnoreCase(vitalsInConsultationPage));
	}
	
	//PM_72 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=479)
	public synchronized void deselectAllVitals()throws Exception{
		logger = Reports.extent.createTest("EMR deselect all elements in vital master");
		int i = 0;
		while(i<22) {
			
			Web_GeneralFunctions.click(vital.getVitalElements(i, Login_Doctor.driver, logger), "deselect all element and trigger save", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(1);
			assertTrue(!Web_GeneralFunctions.isChecked(vital.getVitalElements(i, Login_Doctor.driver, logger), Login_Doctor.driver));
			i++;
		}
		//code to deselect OFC vital if already selected
		if(Web_GeneralFunctions.isChecked("//td//div//label[contains(text(),'OFC')]//input", Login_Doctor.driver)) {
			Web_GeneralFunctions.click(vital.getVitalElements(22, Login_Doctor.driver, logger), "unchecking OFC vital", Login_Doctor.driver, logger);
		}
		saveVitalMaster();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, vital.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(vital.getMessage(Login_Doctor.driver, logger), "Get message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Please select at least one vital parameter"));
	}
	
	//PM_69 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=480)
	public synchronized void selectdefaultVitals()throws Exception{
		logger = Reports.extent.createTest("EMR default vital elements in vital master");
		int i=0;
		while(i<10) {
			Web_GeneralFunctions.click(vital.getVitalElements(i, Login_Doctor.driver, logger), "select default vitals", Login_Doctor.driver, logger);
			
			i++;
		}
		saveVitalMaster();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, vital.getMessage(Login_Doctor.driver, logger));
	}
	
	//PM_77 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=481)
	public synchronized void verifyVitalsPersistenceForNextConsultation() throws Exception {
		logger = Reports.extent.createTest("EMR validate Vitals persistence for next consultation");
		pmt.clickOrgHeader();
		ConsultationTest.UHID=SheetsAPI.getDataProperties(Web_Testbase.input+".patientUHID");
		startConsultation();
		DiagnosisTest.row =0;
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.moveToDiagnosisModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver,sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger) );
		Web_GeneralFunctions.click(dp.moveToDiagnosisModule(Login_Doctor.driver, logger), "go to diagnosis section", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(dp.selectDiagnosisFromDropDown(Login_Doctor.driver, DiagnosisTest.row, "typhoid", logger),"select diagnosis", Login_Doctor.driver, logger);
		String firstConsultationHeight  = String.valueOf(155.0);
		Web_GeneralFunctions.click(gpp.getGlobalPrintModule(Login_Doctor.driver, logger), "click on global print", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.getMessage(Login_Doctor.driver, logger));
		Web_GeneralFunctions.wait(5);
		Web_GeneralFunctions.scrollElementToCenter(vp.getHeightElement(Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.clearWebElement(vp.getHeightElement(Login_Doctor.driver, logger), "clearing height element", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(vp.getHeightElement(Login_Doctor.driver, logger), firstConsultationHeight, "sending height value", Login_Doctor.driver, logger);
		Web_GeneralFunctions.clearWebElement(vp.getWeightElement(Login_Doctor.driver, logger), "clearing weight element", Login_Doctor.driver, logger);
		String firstConsultationWeight  = String.valueOf(55.15);
		Web_GeneralFunctions.sendkeys(vp.getWeightElement(Login_Doctor.driver, logger), firstConsultationWeight, "enter weight", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(gpp.getGlobalPrintModule(Login_Doctor.driver, logger), "click on global print", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver,dp.getMessage(Login_Doctor.driver, logger));
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.scrollElementToCenter(vp.getBMIElement(Login_Doctor.driver, logger), Login_Doctor.driver);
		String firstConsultationBMI = Web_GeneralFunctions.getAttribute(vp.getBMIElement(Login_Doctor.driver, logger),"value", "get BMI value", Login_Doctor.driver, logger);
		SummmaryFlowPages sfp = new SummmaryFlowPages();
		Web_GeneralFunctions.click(sfp.getPrintinConsultationPage(Login_Doctor.driver, logger), "click on print button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getCheckOutButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(gpp.getCheckOutButton(Login_Doctor.driver, logger), "click on checkout button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getViewSummaryButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getViewSummaryCloseButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(gpp.getViewSummaryCloseButton(Login_Doctor.driver, logger), "click on close button", Login_Doctor.driver, logger);
		Billing billingpage = new Billing();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, billingpage.getEmrCreateBillServiceSectionText(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, billingpage.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", 0));
		Web_GeneralFunctions.click(billingpage.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", 0),"click on service text field",Login_Doctor.driver,logger);
		Web_GeneralFunctions.sendkeys(billingpage.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", 0), "test", "sending text to service text box", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		Web_GeneralFunctions.click(billingpage.getEmrCreateBillServiceList(Login_Doctor.driver, logger).get(2), "select one service", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		Web_GeneralFunctions.click(billingpage.getEmrCreateBillPayementOption(Login_Doctor.driver, logger, "Cash"), "select cash option", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		Web_GeneralFunctions.click(billingpage.getEmrCreateBillGenerateButton(Login_Doctor.driver, logger), "click on generate bill", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, billingpage.getEmrGenerateBillCloseButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(billingpage.getEmrGenerateBillCloseButton(Login_Doctor.driver, logger), "click on close button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ap.getAllSlotsPage(Login_Doctor.driver, logger));
		pmt.clickOrgHeader();
		startConsultation();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.moveToDiagnosisModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver,sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger) );
		Web_GeneralFunctions.scrollElementToCenter(vp.getHeightElement(Login_Doctor.driver, logger), Login_Doctor.driver);
		String secondConsultationHeight = Web_GeneralFunctions.getAttribute(vp.getHeightElement(Login_Doctor.driver, logger), "value", "extract height value", Login_Doctor.driver, logger);
		String secondConsultationWeight = Web_GeneralFunctions.getAttribute(vp.getWeightElement(Login_Doctor.driver, logger), "value", "extract weight value", Login_Doctor.driver, logger);
		String secondConsultationBMI = Web_GeneralFunctions.getAttribute(vp.getBMIElement(Login_Doctor.driver, logger), "value", "extract bmi value", Login_Doctor.driver, logger);
		assertTrue(secondConsultationHeight.equals(firstConsultationHeight));
		assertTrue(secondConsultationWeight.equals(firstConsultationWeight));
		assertTrue(secondConsultationBMI.equals(firstConsultationBMI));
	}
	
	
	public synchronized void saveVitalMaster()throws Exception{
		logger = Reports.extent.createTest("EMR save vital master");
		Web_GeneralFunctions.click(vital.getSaveButton(Login_Doctor.driver, logger), "save vital master", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, vital.getMessage(Login_Doctor.driver, logger));
	}
	
	public synchronized void backToVitalMaster()throws Exception{
		logger = Reports.extent.createTest("EMR Back to vital master from consultation page");
		pmt.moveToPracticeManagement();
		validateVitalsMasterModule();
	}
	
	public synchronized void startConsultation() throws Exception {
		logger = Reports.extent.createTest("EMR start new Consultation");
		PracticeManagementPages pmp = new PracticeManagementPages();
		ConsultationTest consult = new ConsultationTest();
		if(pmp.isappointmentDisplayed(Login_Doctor.driver, logger) || vital.getActiveConsultationsOnAppointmentDashboard(Login_Doctor.driver, logger)==null ) {
			consult.startConsultation();
		}else {
			Web_GeneralFunctions.click(vital.getConsultingDropDownToggle(Login_Doctor.driver, logger), "click option", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, vital.getConsultingLinkFromDropDownToggle(Login_Doctor.driver, logger));
			Web_GeneralFunctions.click(vital.getConsultingLinkFromDropDownToggle(Login_Doctor.driver, logger), "Leads to consultation page", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.moveToDiagnosisModule(Login_Doctor.driver, logger));
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver,sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger) );
		}
	}
}

