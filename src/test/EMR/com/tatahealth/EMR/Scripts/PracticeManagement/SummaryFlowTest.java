
package com.tatahealth.EMR.Scripts.PracticeManagement;

import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.API.libraries.SheetsAPI;
import com.tatahealth.EMR.Scripts.Consultation.ConsultationTest;
import com.tatahealth.EMR.Scripts.Consultation.DiagnosisTest;
import com.tatahealth.EMR.Scripts.Consultation.GlobalPrintTest;
import com.tatahealth.EMR.Scripts.Consultation.SymptomTest;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Consultation.DiagnosisPage;
import com.tatahealth.EMR.pages.Consultation.FollowUpPage;
import com.tatahealth.EMR.pages.Consultation.GlobalPrintPage;
import com.tatahealth.EMR.pages.Consultation.LabTestPage;
import com.tatahealth.EMR.pages.Consultation.PrescriptionPage;
import com.tatahealth.EMR.pages.Consultation.ReferralPage;
import com.tatahealth.EMR.pages.Consultation.SymptomPage;
import com.tatahealth.EMR.pages.PracticeManagement.CaseSheetDesignerPages;

import com.tatahealth.EMR.pages.PracticeManagement.MedicalKitPages;
import com.tatahealth.EMR.pages.PracticeManagement.PracticeManagementPages;
import com.tatahealth.EMR.pages.PracticeManagement.SummmaryFlowPages;

import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.ReusableModules.Web_Testbase;

public class SummaryFlowTest {
	
	ExtentTest logger;
	PracticeManagementPages pm = new PracticeManagementPages();
	SummmaryFlowPages summary = new SummmaryFlowPages();
	DiagnosisPage dp = new DiagnosisPage();
	GlobalPrintPage gpp = new GlobalPrintPage();
	SymptomPage sp = new SymptomPage();
	FollowUpPage fp = new FollowUpPage();
	ReferralPage rp = new ReferralPage();
	GlobalPrintTest gpt = new GlobalPrintTest();
	CaseSheetDesignerPages cdp = new CaseSheetDesignerPages();
	public static List<String> summaryElementsListBeforeOrdering = new ArrayList<>();
	public static List<String> summaryElementsListAfterOrdering = new ArrayList<>();
	

	@BeforeClass(alwaysRun=true,groups= {"Regression"})
	public static void beforeClass() throws Exception {
		Login_Doctor.executionName="EMR_PracticeManagement_SummaryPDFPreference";
		Login_Doctor.LoginTest();
		new MedicalKItTest().closePreviousConsultations();
		new PracticeManagementTest().moveToPracticeManagement();
	} 
	
	@AfterClass(groups = { "Regression"})
	public static void afterClass() {
		Login_Doctor.LogoutTest();
	}
	
	
	@Test(groups= {"Regression","PracticeManagement"},priority=497)
	public synchronized void moveToSummaryPDFPreferenceAndValidate()throws Exception{
		logger = Reports.extent.createTest("EMR move to summary pdf preference");
		Web_GeneralFunctions.click(summary.moveToSummaryPDFPreference(Login_Doctor.driver, logger), "Move to summary pdf preference", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, summary.getCaseSheet(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, summary.getPrescription(Login_Doctor.driver, logger));
		assertTrue(Web_GeneralFunctions.isDisplayed(summary.getCaseSheet(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(summary.getLabTests(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(summary.getFollowup(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(summary.getGeneralAdvice(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(summary.getVitals(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(summary.getSymptoms(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(summary.getMedicalHistory(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(summary.getPrescription(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(summary.getDiagnosis(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(summary.getReferral(Login_Doctor.driver, logger)));
	}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=498)
	public synchronized void moveFifthElementToSecondPositionAndValidate()throws Exception{
		logger = Reports.extent.createTest("EMR Move Fifth Element to Second position validate");
		summaryElementsListBeforeOrdering = summary.getAllSummaryPdfElementNamesAsList(Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(pm.getLeftMainMenu(Login_Doctor.driver, logger), "click on left main menu", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		WebElement fromElement = summary.getAllSummaryPdfElementsAsList(Login_Doctor.driver, logger).get(4);
		WebElement toElement = summary.getAllSummaryPdfElementsAsList(Login_Doctor.driver, logger).get(1);
		Web_GeneralFunctions.dragAndDrop(fromElement, toElement, "Move fifth element to second position", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		summaryElementsListAfterOrdering = summary.getAllSummaryPdfElementNamesAsList(Login_Doctor.driver, logger);
		assertTrue(!(summaryElementsListBeforeOrdering.equals(summaryElementsListAfterOrdering)));
		
	}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=499)
	public synchronized void moveSixthElementToThirdPositionAndValidate()throws Exception{
		logger = Reports.extent.createTest("EMR Sixth Element to Thrid position And validate");
		summaryElementsListBeforeOrdering = summary.getAllSummaryPdfElementNamesAsList(Login_Doctor.driver, logger);
		WebElement fromElement  =summary.getAllSummaryPdfElementsAsList(Login_Doctor.driver, logger).get(5);
		WebElement toElement = summary.getAllSummaryPdfElementsAsList(Login_Doctor.driver, logger).get(2);
		Web_GeneralFunctions.dragAndDrop(fromElement, toElement, "move sixth element to thrid position", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		summaryElementsListAfterOrdering = summary.getAllSummaryPdfElementNamesAsList(Login_Doctor.driver, logger);
		assertTrue(!(summaryElementsListBeforeOrdering.equals(summaryElementsListAfterOrdering)));
	}
	
	
	@Test(groups= {"Regression","PracticeManagement"},priority=500)
	public synchronized void moveLastElementToFirstPositionAndValidate()throws Exception{
		logger = Reports.extent.createTest("EMR Move Last Element to first position and validate");
		summaryElementsListBeforeOrdering = summary.getAllSummaryPdfElementNamesAsList(Login_Doctor.driver, logger);
		WebElement fromElement  =summary.getAllSummaryPdfElementsAsList(Login_Doctor.driver, logger).get(9);
		WebElement toElement = summary.getAllSummaryPdfElementsAsList(Login_Doctor.driver, logger).get(1);
		Web_GeneralFunctions.dragAndDrop(fromElement, toElement, "move Last Element to first position", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		summaryElementsListAfterOrdering = summary.getAllSummaryPdfElementNamesAsList(Login_Doctor.driver, logger);
		assertTrue(!(summaryElementsListBeforeOrdering.equals(summaryElementsListAfterOrdering)));
	}

	@Test(groups= {"Regression","PracticeManagement"},priority=501)
	public synchronized void saveSummaryPDFPreference()throws Exception{
		logger = Reports.extent.createTest("EMR save summary pdf preference");
		Web_GeneralFunctions.click(summary.saveSummaryPDFPreference(Login_Doctor.driver, logger), "Save summary pdf preference", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, summary.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(summary.getMessage(Login_Doctor.driver, logger), "Get success message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Consultation Summary Preference saved successfully"));
	}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=502)
	public synchronized void validatePdfPreferenceInConsultationPage() throws Exception {
		List<String> sectionsOrderInPdf ;
		logger = Reports.extent.createTest("EMR validate pdf preference in consultation page");
		PracticeManagementTest pmt = new PracticeManagementTest();
		MedicalKItTest mkt = new MedicalKItTest();
		MedicalKitPages mk = new MedicalKitPages();
		SymptomPage sPage = new SymptomPage();
		DiagnosisTest dTest = new DiagnosisTest();
		PrescriptionPage pPage = new PrescriptionPage();
		LabTestPage lPage = new LabTestPage();
		VitalMasterTest vmt = new VitalMasterTest();
		pmt.clickOrgHeader();
		pmt.moveToPracticeManagement();
		mkt.moveToMedicalKitAndVerify();
		mkt.clickCreateKit();
		MedicalKItTest.kitName =RandomStringUtils.randomAlphabetic(6);
		Web_GeneralFunctions.sendkeys(mk.getKitNameField(Login_Doctor.driver, logger), MedicalKItTest.kitName, "sending freetext in kit name field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		//add value in symptom
		SymptomTest.row = 0;
		Web_GeneralFunctions.click(sPage.selectSymptomFromDropDown(Login_Doctor.driver,SymptomTest.row,"",logger), "select symptom from drop down", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		//add value in diagnosis field
		DiagnosisTest.row = 0;
		dTest.selectDiagnosis();
		//add medicine from template
		Web_GeneralFunctions.click(pPage.getTemplateFromDropDown("", Login_Doctor.driver, logger), "select any one of the template", Login_Doctor.driver, logger);
		//Web_GeneralFunctions.wait(1);
		Web_GeneralFunctions.click(mk.getGeneralAdviceDropDown(Login_Doctor.driver, logger), "click general advice drop down", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getGeneralAdviceSearchElement(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(mk.select1stDropDown(Login_Doctor.driver, logger), "select 1st template from drop down", Login_Doctor.driver, logger);
		//Web_GeneralFunctions.wait(1);
		Web_GeneralFunctions.click(mk.getAddGeneralAdviceTemplate(Login_Doctor.driver, logger), "click add button", Login_Doctor.driver, logger);
		//Web_GeneralFunctions.wait(1);
		//add lab test from template
		Web_GeneralFunctions.click(lPage.getLabTestTemplateFromDropDown("", Login_Doctor.driver, logger), "select lab test template", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		Web_GeneralFunctions.click(mk.getSaveKitButton(Login_Doctor.driver, logger), "click save medical kit", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getMessage(Login_Doctor.driver, logger));
		ConsultationTest.UHID=SheetsAPI.getDataProperties(Web_Testbase.input+".patient2UHID");
		pmt.clickOrgHeader();
		vmt.startConsultation();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.moveToDiagnosisModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver,sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger) );
		Web_GeneralFunctions.click(mk.getSelectMedicalKit(MedicalKItTest.kitName, Login_Doctor.driver, logger), "select medical kit", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getApplyMedicalKit(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(mk.getApplyMedicalKit(Login_Doctor.driver, logger), "click apply medical kit button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getMessage(Login_Doctor.driver, logger));
		Web_GeneralFunctions.wait(5);
		Web_GeneralFunctions.click(fp.moveToFollowUp(Login_Doctor.driver, logger), "click on Referral menu in consultation page", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.selectSpeciality(Login_Doctor.driver, logger));
		Web_GeneralFunctions.wait(5);
		Web_GeneralFunctions.click(rp.selectSpeciality(Login_Doctor.driver, logger), "click on special serach box", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, summary.getFirstSpecility(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(summary.getFirstSpecility(Login_Doctor.driver, logger), "select one speciality", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.getReferToSelectElement(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(fp.moveToFollowUp(Login_Doctor.driver, logger), "click on Referral menu in consultation page", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getMessage(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(rp.getReferToSelectElement(Login_Doctor.driver, logger), "click on refer to", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, summary.getFirstDoctor(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(summary.getFirstDoctor(Login_Doctor.driver, logger), "select first doctor", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		String comment = RandomStringUtils.randomAlphabetic(20);
		Web_GeneralFunctions.sendkeys(rp.getComment(Login_Doctor.driver, logger), comment, "sending random text to comment field", Login_Doctor.driver, logger);
		//Web_GeneralFunctions.wait(2);
		if(!summary.getFollowupInputField(Login_Doctor.driver, logger).isEnabled()) {
			Web_GeneralFunctions.click(summary.getFollowupCheckbox(Login_Doctor.driver, logger), "click on followup", Login_Doctor.driver, logger);
		}else {
			Web_GeneralFunctions.click(summary.getFollowupCheckbox(Login_Doctor.driver, logger), "click on followup", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(cdp.getCaseSheetElementInConsultationPage(Login_Doctor.driver, logger), "click on casesheet option", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, summary.getMessage(Login_Doctor.driver, logger));
			Web_GeneralFunctions.wait(2);
			Web_GeneralFunctions.click(fp.moveToFollowUp(Login_Doctor.driver, logger), "click on Referral menu in consultation page", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(summary.getFollowupCheckbox(Login_Doctor.driver, logger), "click on followup", Login_Doctor.driver, logger);
		}
		//Web_GeneralFunctions.wait(1);
		Web_GeneralFunctions.click(cdp.getCaseSheetElementInConsultationPage(Login_Doctor.driver, logger), "click on case sheet element", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, summary.getMessage(Login_Doctor.driver, logger));
		Web_GeneralFunctions.wait(4);
		Web_GeneralFunctions.sendkeys(summary.getGeneralNotesTextArea(Login_Doctor.driver, logger), RandomStringUtils.randomAlphabetic(15), "sending random text to notes area", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(dp.moveToDiagnosisModule(Login_Doctor.driver, logger), "click on diagnosis module", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, summary.getMessage(Login_Doctor.driver, logger));
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(summary.getPrintinConsultationPage(Login_Doctor.driver, logger), "click on print button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver,summary.getPrintAllinConsultationPage(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(summary.getPrintAllinConsultationPage(Login_Doctor.driver, logger), "click on printall button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(4);
		String pdfContent = gpt.getPDFPage();
		sectionsOrderInPdf =getFilteredSectionNamesFromPdf(pdfContent);
		assertTrue(summaryElementsListAfterOrdering.equals(sectionsOrderInPdf));
	}
	
	public synchronized List<String> getFilteredSectionNamesFromPdf(String pdfContent) {
		List<String> filteredList = new ArrayList<>();
		List<String> finalSectionsOrderList = new ArrayList<>();
		String pdfcontent = pdfContent.replaceAll("[\\t\\n\\r]+","");
		
		String[] arrayOfContent = pdfcontent.split("\\s+");
		for(String x :arrayOfContent) {
			if(x.contains("MedicalHistory") || x.contains("Vitals") ||x.contains("Symptoms") || x.contains("Referral") || x.contains("Prescription")
					||x.contains("GeneralAdvice") || x.contains("Follow") ||x.contains("Lab")|| x.contains("Case") ||x.contains("Diagnosis")) {
				filteredList.add(x);
			}
		}
		for(String s :filteredList) {
			if(s.contains("MedicalHistory")) {
				finalSectionsOrderList.add("Medical History");
			}
			else if(s.contains("Vitals")) {
				finalSectionsOrderList.add("Vitals");
			}
			else if(s.contains("Symptoms")) {
				finalSectionsOrderList.add("Symptoms");
			}
			else if(s.contains("Referral")) {
				finalSectionsOrderList.add("Referral");
			}
			else if(s.contains("Prescription")) {
				finalSectionsOrderList.add("Prescription");
			}
			else if(s.contains("GeneralAdvice")) {
				finalSectionsOrderList.add("General Advice");
			}
			else if(s.contains("Follow")) {
				finalSectionsOrderList.add("Follow up");
			} 
			else if(s.contains("Lab")) {
				finalSectionsOrderList.add("Lab Tests");
			}
			else if(s.contains("Case")) {
				finalSectionsOrderList.add("Case Sheet");
			}
			else if(s.contains("Diagnosis")) {
				finalSectionsOrderList.add("Diagnosis");
			}
		}
		
		return finalSectionsOrderList;
	}
	
}

