package com.tatahealth.EMR.Scripts.Billing;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Consultation.ConsultationTest;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Billing.Billing;
import com.tatahealth.EMR.pages.Consultation.ConsultationPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class ConsumerPackagePayClinicTest {
	
	ConsultationTest appt=new ConsultationTest();
	BillingTest bs=new BillingTest();
	Billing bill = new Billing();
	ConsultationPage consultation = new ConsultationPage();
	ConsumerAppointmentTest consumer= new ConsumerAppointmentTest();
	public static ExtentTest logger;
	
	
	@BeforeClass(alwaysRun = true) public static void beforeClass() throws Exception { 
		  Reports.reports();
		  Login_Doctor.executionName="ConsumerPackagePayClinicTest";
		  BillingTest.doctorName="Don";
		  Login_Doctor.LoginTestwithDiffrentUser(BillingTest.doctorName);
		  }
	  
	  @AfterClass(alwaysRun = true) public static void afterClass() throws Exception {
		  Login_Doctor.LogoutTest();
			Reports.extent.flush(); 
		  }
	
	/*Billing_C_PP_2-pay at clinic is enabled from doctor side*/
	/*  Mention test case covered in all test below :RGB_5 ,VB_1-VB_24,Biiling_C_P_27-Biiling_C_P_33,Biiling_C_P_18,Biiling_C_P_23-Biiling_C_P_26
	 * Biiling_C_P_47-Biiling_C_P_48
	 */
	  
	  /*Test case:Billing_C_PP_13,Biiling_C_P_4*/
		@Test(priority=93,groups = {"G2"})
		public void consumerPPcPaymentCashWalletEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Package+Pay at Clinic Payment Emr Validation with Cash Wallet");
			String paymentCheck="Cash, Wallet, Package";
			String paidStatus="Cash, Wallet";
			consumer.consumerBookAppointment("Package + Pay At Clinic");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModePPcStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerPPcBillingPageValidation();
			 bs.paymentCashWallet();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashWalletPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModePPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCashWalletOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCashWallet();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashWalletPdfValidation();
		 }
		@Test(priority=94,groups = {"G1"})
		public void consumerPPcPaymentCashDebitCardEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Package+Pay at Clinic Payment Emr Validation with Cash Debit Card");
			String paymentCheck="Cash, Debit card, Package";
			String paidStatus="Cash, Debit card";
			consumer.consumerBookAppointment("Package + Pay At Clinic");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModePPcStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerPPcBillingPageValidation();
			 bs.paymentCashDebitCard();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashCardPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModePPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCashCardOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCashDebitCard();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashCardPdfValidation();
		 }
		@Test(priority=95,groups = {"G3"})
		public void consumerPPcPaymentCashCreditCardEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Package+Pay at Clinic Payment Emr Validation with Cash Credit Card");
			String paymentCheck="Cash, Credit card, Package";
			String paidStatus="Cash, Credit card";
			consumer.consumerBookAppointment("Package + Pay At Clinic");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModePPcStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerPPcBillingPageValidation();
			 bs.paymentCashCreditCard();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashCardPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModePPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCashCardOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCashCreditCard();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashCardPdfValidation();
		 }
		@Test(priority=96,groups = {"G1"})
		public void consumerPPcPaymentCashEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Package+Pay at Clinic Payment Emr Validation with Cash");
			String paymentCheck="Cash, Package";
			String paidStatus="Cash";
			consumer.consumerBookAppointment("Package + Pay At Clinic");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModePPcStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerPPcBillingPageValidation();
			 bs.paymentCash();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModePPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCashOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCash();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashPdfValidation();
		 }
		@Test(priority=97,groups = {"G2"})
		public void consumerPPcPaymentCreditCardEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Package+Pay at Clinic Payment Emr Validation with Credit Card");
			String paymentCheck="Credit card, Package";
			String paidStatus="Credit card";
			consumer.consumerBookAppointment("Package + Pay At Clinic");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModePPcStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerPPcBillingPageValidation();
			 bs.paymentCreditCard();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCardPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModePPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCardOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCreditCard();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCardPdfValidation();
		 }
		@Test(priority=98,groups = {"G3"})
		public void consumerPPcPaymentWalletEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Package+Pay at Clinic Payment Emr Validation with Wallet");
			String paymentCheck="Wallet, Package";
			String paidStatus="Wallet";
			consumer.consumerBookAppointment("Package + Pay At Clinic");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModePPcStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerPPcBillingPageValidation();
			 bs.paymentWallet();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingWalletPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModePPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedWalletOption();
		  bs.billingCreatePaymentOptionDisplay();
		  bs.paymentWallet();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingWalletPdfValidation();
		 }
		
		//No Service 
		
		@Test(priority=99,groups = {"G3"})
		public void consumerPPcPaymentCashWalletNoServiceEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Package+Pay at Clinic Payment Emr Validation with Cash Wallet No Service");
			String paymentCheck="Cash, Wallet, Package";
			String paidStatus="Cash, Wallet";
			consumer.consumerBookAppointment("Package + Pay At Clinic");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModePPcStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerPPcBillingPageNoServiceValidation();
			 bs.paymentCashWallet();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashWalletNoServicePdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModePPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCashWalletOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCashWallet();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashWalletNoServicePdfValidation();
		 }
		@Test(priority=100,groups = {"G2"})
		public void consumerPPcPaymentCashDebitCardNoServiceEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Package+Pay at Clinic Payment Emr Validation with Cash Debit Card No Service");
			String paymentCheck="Cash, Debit card, Package";
			String paidStatus="Cash, Debit card";
			consumer.consumerBookAppointment("Package + Pay At Clinic");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModePPcStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerPPcBillingPageNoServiceValidation();
			 bs.paymentCashDebitCard();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashCardNoServicePdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModePPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCashCardOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCashDebitCard();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashCardNoServicePdfValidation();
		 }
		@Test(priority=101,groups = {"G1"})
		public void consumerPPcPaymentCashNoServiceEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Package+Pay at Clinic Payment Emr Validation with Cash No Service");
			String paymentCheck="Cash, Package";
			String paidStatus="Cash";
			consumer.consumerBookAppointment("Package + Pay At Clinic");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModePPcStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerPPcBillingPageNoServiceValidation();
			 bs.paymentCash();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashNoServicePdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModePPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCashOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCash();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashNoServicePdfValidation();
		 }
		@Test(priority=102,groups = {"G1"})
		public void consumerPPcPaymentCreditCardNoServiceEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Package+Pay at Clinic Payment Emr Validation with Credit Card No Service");
			String paymentCheck="Credit card, Package";
			String paidStatus="Credit card";
			consumer.consumerBookAppointment("Package + Pay At Clinic");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModePPcStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerPPcBillingPageNoServiceValidation();
			 bs.paymentCreditCard();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCardNoServicePdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModePPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCardOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCreditCard();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCardNoServicePdfValidation();
		 }
		@Test(priority=103,groups = {"G2"})
		public void consumerPPcPaymentWalletNoServiceEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Package+Pay at Clinic Payment Emr Validation with Wallet No Service");
			String paymentCheck="Wallet, Package";
			String paidStatus="Wallet";
			consumer.consumerBookAppointment("Package + Pay At Clinic");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModePPcStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerPPcBillingPageNoServiceValidation();
			 bs.paymentWallet();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingWalletNoServicePdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModePPcStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedWalletOption();
		  bs.billingCreatePaymentOptionDisplay();
		  bs.paymentWallet();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingWalletNoServicePdfValidation();
		 }
}
