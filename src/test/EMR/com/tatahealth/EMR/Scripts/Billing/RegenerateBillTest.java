package com.tatahealth.EMR.Scripts.Billing;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Consultation.ConsultationTest;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.AppointmentDashboard.AllSlotsPage;
import com.tatahealth.EMR.pages.Billing.Billing;
import com.tatahealth.EMR.pages.Consultation.ConsultationPage;
import com.tatahealth.EMR.pages.Login.LoginPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class RegenerateBillTest {
	BillingTest bs=new BillingTest();
	Billing bill = new Billing();
	LoginPage loginPage = new LoginPage();
	MasterAdministrationTest masterAdmin=new MasterAdministrationTest();
	ConsultationTest appt=new ConsultationTest();
	AllSlotsPage ASpage = new AllSlotsPage();
	ConsultationPage consultation = new ConsultationPage();
	public static ExtentTest logger;
	
	  @BeforeClass(alwaysRun = true) public static void beforeClass() throws Exception { 
		  Reports.reports();
		  Login_Doctor.executionName="RegenerateBillTest";
		  BillingTest.doctorName="Topaa";
		  Login_Doctor.LoginTestwithDiffrentUser(BillingTest.doctorName);
			 ConsultationTest.patientName="Ballu";
		  }
	  
	  @AfterClass(alwaysRun = true) public static void afterClass() throws Exception {
		  Login_Doctor.LogoutTest();
		  Reports.extent.flush(); 
		  }
	
	/*Test case:RGB_4*/
	@Test(priority=48) 
		  public synchronized void generateAndRegenerateViewBillValidation() throws Exception
		 {
			 logger = Reports.extent.createTest("EMR _Generate And Regenerate Bill Validation");
			 bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
				Web_GeneralFunctions.wait(2);
			  bs.rowCount=0;
			  bs. expectedNetTotalAmount=0;
			  bs.patientName=ConsultationTest.patientName;
			  appt.startBookingAppointment();
			  appt.getAppointmentDropdown();
			  appt.bookedAppointmentOptionValidation();
			  appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			  bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
			  bs.billPageDoctorAndPatientFieldValidation();
			  bs.billDefaultServiceValidation();
			  bs.billDefaultServiceLevelChangeValidation();
			  bs.deleteService();
			  bs.emrAppointmentGenerateBillValidation();
			  bs.paymentCashWallet();
			  bs. billingCreateBillPopUpValidation();
			  bs. billingCreateBillPdfValidation();
			  Web_GeneralFunctions.wait(5);
			  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			  Web_GeneralFunctions.wait(5);
			  appt.getAppointmentDropdown();
			  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
			  appt.appointmentPaymentModeCashWalletStatus();
			  bs.billingPageLaunch();
			  bill.getEmrBillTab(Login_Doctor.driver, logger,"View Bill").click();
			  bs.viewBillSearchAllfieldDisplayValidation();
				bill.getEmrViewBillPatientNameSearchTextBox(Login_Doctor.driver, logger).clear();
				bill.getEmrViewBillPatientNameSearchTextBox(Login_Doctor.driver, logger).sendKeys(bs.patientName);
			    bs.searchBillButtonClick();
				bs.viewBillSearchResultColumnDisplayValidation();
				bs.viewBillSearchResultBillSearchCancel();
				Web_GeneralFunctions.wait(5);
				 bill.getEmrTopMenuElement(Login_Doctor.driver, logger, "Appointments").click();
				 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
				 Web_GeneralFunctions.wait(5);
			  appt.getAppointmentDropdown();
			  appt.bookedAppointmentOptionAfterCheckinOrCheckinGenerateBillCancelValidation();
			  appt.getAppointmentDropDownValue(3,"Click on Generate Bill");
			  bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
			  bs.billPageDoctorAndPatientFieldValidation();
			  bs.billSaveServiceValidation();
			  bs.paymentRemoveSelectedCashWalletOption();
			  bs. billingCreateBillPaymentValidation();
			  bs.paymentCashWallet();
			  bs. billingCreateBillPopUpValidation();
			 bs. billingCreateBillPdfValidation();
		  }
	 /*Test case:RGB_1*/
	 @Test(priority=49) 
	  public synchronized void generateAndRegenerateBillValidation() throws Exception
	 {
		 logger = Reports.extent.createTest("EMR _Generate And Regenerate Bill Validation");
		 bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
		  bs.rowCount=0;
		  bs. expectedNetTotalAmount=0;
		  bs.patientName=ConsultationTest.patientName;
		  appt.startBookingAppointment();
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionValidation();
		  appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		  bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		  bs.billPageDoctorAndPatientFieldValidation();
		  bs.billDefaultServiceValidation();
		  bs.billDefaultServiceLevelChangeValidation();
		  bs.deleteService();
		  bs.emrAppointmentGenerateBillValidation();
		  bs.paymentCashWallet();
		  bs. billingCreateBillPopUpValidation();
		  bs. billingCreateBillPdfValidation();
		  Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.getAppointmentDropDownValue(3,"Click on Cancel Bill");
		  String successMessage = bill.getEmrBillErrorMessage(Login_Doctor.driver, logger).getText().trim();
		  Assert.assertTrue("Bill Cancelled Successfully".equals(successMessage), "Cancel bill Message not expected");
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinOrCheckinGenerateBillCancelValidation();
		  appt.getAppointmentDropDownValue(3,"Click on Generate Bill");
		  bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		  bs.billPageDoctorAndPatientFieldValidation();
		  bs.billSaveServiceValidation();
		  bs.paymentRemoveSelectedCashWalletOption();
		  bs. billingCreateBillPaymentValidation();
		  bs.paymentCashWallet();
		  bs. billingCreateBillPopUpValidation();
		 bs. billingCreateBillPdfValidation();
	  }
	 /*Test case:RGB_6,RGB_10,RGB_11*/
	 @Test(priority=50) 
	  public synchronized void generateAndRegenerateBillCheckoutValidation() throws Exception
	 {
		 logger = Reports.extent.createTest("EMR _Generate And Regenerate Bill Checkout Validation");
		 bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
		  bs.rowCount=0;
		  bs. expectedNetTotalAmount=0;
		  bs.patientName=ConsultationTest.patientName;
		  bill.getEmrTopMenu(Login_Doctor.driver, logger).click();
		  masterAdmin.masterAdministrationBillType("Post-Billing");
		  Web_GeneralFunctions.wait(4);
		  Login_Doctor.LogoutTest();
		  Login_Doctor.LoginTestwithDiffrentUser(BillingTest.doctorName);
		  appt.startBookingAppointment();
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionValidation();
		  appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		  bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		  bs.billPageDoctorAndPatientFieldValidation();
		  bs.billDefaultServiceValidation();
		  bs.clickGenerateButtonAndClosePopUp();
		  Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.getAppointmentDropDownValue(3,"Click on Cancel Bill");
		  String successMessage = bill.getEmrBillErrorMessage(Login_Doctor.driver, logger).getText().trim();
		  Assert.assertTrue("Bill Cancelled Successfully".equals(successMessage), "Cancel bill Message not expected");
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinOrCheckinGenerateBillCancelValidation();
		  appt.getAppointmentDropDownValue(2,"Click on Consult");
		  appt.afterConsultClickValidation();
		  appt.checkOutAndBillPatientConsultPageClick();
		  bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		  bs.paymentRemoveSelectedCashOption();
		  bs.billPageDoctorAndPatientFieldValidation();
		  bs.billDefaultServiceValidation();
		  bs.billDefaultServiceLevelChangeValidation();
		  bs.deleteService();
		  bs.emrAppointmentGenerateBillValidation();
		  bs.paymentCashWallet();
		  bs.billingCreateBillPopUpValidation();
		  bs.billingCreateBillPdfValidation();
	  }

	 /*Test case:RGB_7,RGB_10,RGB_11*/
	 @Test(priority=51) 
	  public synchronized void generateAndRegenerateBillFollowUpValidation() throws Exception
	 {
		 logger = Reports.extent.createTest("EMR _Generate And Regenerate Bill FollowUp Validation");
		 bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
		  bs.rowCount=0;
		  bs. expectedNetTotalAmount=0;
		  bs.patientName=ConsultationTest.patientName;
		  bill.getEmrTopMenu(Login_Doctor.driver, logger).click();
		  masterAdmin.masterAdministrationBillType("Post-Billing");
		  Web_GeneralFunctions.wait(4);
		  Login_Doctor.LogoutTest();
		  Login_Doctor.LoginTestwithDiffrentUser(BillingTest.doctorName);
		  appt.startBookingAppointment();
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionValidation();
		  appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		  bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		  bs.billPageDoctorAndPatientFieldValidation();
		  bs.billDefaultServiceValidation();
		  bs.clickGenerateButtonAndClosePopUp();
		  Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.getAppointmentDropDownValue(3,"Click on Cancel Bill");
		  String successMessage = bill.getEmrBillErrorMessage(Login_Doctor.driver, logger).getText().trim();
		  Assert.assertTrue("Bill Cancelled Successfully".equals(successMessage), "Cancel bill Message not expected");
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinOrCheckinGenerateBillCancelValidation();
		  appt.getAppointmentDropDownValue(2,"Click on Consult");
		  appt.afterConsultClickValidation();
		  ASpage.getNotePreConsultPage(Login_Doctor.driver, logger).sendKeys("addNote");
		  appt.getShareUpFollowConsultPageClick();
		  Web_GeneralFunctions.wait(5);
		  bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		  bs.paymentRemoveSelectedCashOption();
		  bs.billPageDoctorAndPatientFieldValidation();
		  bs.billDefaultServiceValidation();
		  bs.billDefaultServiceLevelChangeValidation();
		  bs.deleteService();
		  bs.emrAppointmentGenerateBillValidation();
		  bs.paymentCashWallet();
		  bs.billingCreateBillPopUpValidation();
		  bs.billingCreateBillPdfValidation();
	  }

	

}
