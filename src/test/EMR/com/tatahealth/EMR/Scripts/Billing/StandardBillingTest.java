package com.tatahealth.EMR.Scripts.Billing;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.tatahealth.API.libraries.CommonAPI;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Consultation.ConsultationTest;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.AppointmentDashboard.AllSlotsPage;
import com.tatahealth.EMR.pages.Billing.Billing;
import com.tatahealth.EMR.pages.Consultation.ConsultationPage;
import com.tatahealth.EMR.pages.Login.LoginPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class StandardBillingTest {
	BillingTest bs=new BillingTest();
	Billing bill = new Billing();
	LoginPage loginPage = new LoginPage();
	MasterAdministrationTest masterAdmin=new MasterAdministrationTest();
	ConsultationTest appt=new ConsultationTest();
	AllSlotsPage ASpage = new AllSlotsPage();
	ConsultationPage consultation = new ConsultationPage();
	public static ExtentTest logger;
	
	  @BeforeClass(alwaysRun = true) public static void beforeClass() throws Exception { 
		  Reports.reports();
		  Login_Doctor.executionName="StandardBillingTest";
		  BillingTest.doctorName="Kasu";
		  Login_Doctor.LoginTestwithDiffrentUser(BillingTest.doctorName);
			 ConsultationTest.patientName="Ballu";
		  }
	  @AfterClass(alwaysRun = true) public static void afterClass() throws Exception {
		  Login_Doctor.LogoutTest();
		  Reports.extent.flush(); 
		  }
	@Test(priority=38) 
	  public synchronized void standardBillingMasterValidation() throws Exception
	 {
	  logger = Reports.extent.createTest("EMR_Standard Billing Master Validation");
	  bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
	  bill.getEmrTopMenu(Login_Doctor.driver, logger).click();
	  masterAdmin.masterAdministrationBillType("Standard Billing");
	  Web_GeneralFunctions.wait(4);
	  Login_Doctor.Logout();
	  Login_Doctor.LoginTestwithoutNewInstance(BillingTest.doctorName);
		  }
	 /*Test case:EMR_Standard_26*/
	/*EMR_Standard_28 user specific*/
	@Test(priority=39) 
	 public synchronized void standardBillingRescheduleAppointmentValidation() throws Exception
	 {
		 logger = Reports.extent.createTest("EMR_Standard Billing Appointment Validation");
		 bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
		 appt.startBookingAppointment();
		 appt.getAppointmentDropdown();
		 appt.appointmentBookedStatus();
		 appt.bookedAppointmentOptionValidation();
		 appt.getAppointmentDropDownValue(4,"Click on Reschedule");
		 appt.appointmentReschedule();
		 Web_GeneralFunctions.wait(5);
		 appt.getAppointmentDropdown();
		 appt.appointmentBookedStatus();
		 appt.bookedAppointmentOptionValidation();
		 appt.getAppointmentDropDownValue(1,"Click on check in");
		 Web_GeneralFunctions.wait(5);
		 appt.getAppointmentDropdown();
		 appt.appointmentCheckInStatus();
		 appt.bookedAppointmentOptionAfterCheckinOrCheckinGenerateBillCancelValidation();
		 appt.getAppointmentDropDownValue(1,"Click on  Preconsultaion");
		 Web_GeneralFunctions.wait(5);;
		 bill.getEmrTopMenu(Login_Doctor.driver, logger).click();
		  bill.getEmrTopMenuElement(Login_Doctor.driver, logger, "Appointments").click();
		  Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
			 appt.getAppointmentDropdown();
			 appt.appointmentPreConsultingStatus();
			 appt.bookedAppointmentOptionCheckinAndNotGenerateBillAfterPreConsultationValidation();
			 appt.getAppointmentDropDownValue(1,"Click on  Preconsulting");
			 ASpage.getNotePreConsultPage(Login_Doctor.driver, logger).sendKeys("addNote");
			 ASpage.getSavePreConsultPage(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
		 appt.getAppointmentDropdown();
		 appt.appointmentPreConsultedStatus();
		 appt.bookedAppointmentOptionCheckinAndNotGenerateBillAfterPreConsultingValidation();
		 appt.getAppointmentDropDownValue(1,"Click on consult");
		 appt.afterConsultClickValidation();
		 ASpage.getNotePreConsultPage(Login_Doctor.driver, logger).sendKeys("addNote");
		 appt.getShareUpFollowConsultPageClick();
		 Web_GeneralFunctions.wait(5);
		 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		 Web_GeneralFunctions.wait(5);
		 appt.getAppointmentDropdown();
		 appt.appointmentFollowUpStatus();
		 appt.bookedAppointmentOptionPreBillAfterConsultShareFollowUpValidation();
		 appt.getAppointmentDropDownValue(1,"Click on FollowUp");
		 ASpage.getNotePreConsultPage(Login_Doctor.driver, logger).sendKeys("addNote");
		  appt.checkOutAndPrintConsultPageClick();
		  Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5); 
		  appt.getAppointmentDropdown();
		  appt.appointmentCheckedOutStatus();
		  appt.bookedAppointmentOptionAfterConsultCancelBillValidation();
		 	  }
	 /*Test case:Bill_cond_003,Bill_cond_009,EMR_Standard_15-EMR_Standard_17*/
	@Test(priority=40) 
	 public synchronized void standardBillingAppointmentValidation() throws Exception
	 {
		 logger = Reports.extent.createTest("EMR_Standard Billing Appointment Validation");
		 bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
		 appt.startBookingAppointment();
		 appt.getAppointmentDropdown();
		 appt.appointmentBookedStatus();
		 appt.bookedAppointmentOptionValidation();
		 appt.getAppointmentDropDownValue(1,"Click on check in");
		 Web_GeneralFunctions.wait(5);
		 appt.getAppointmentDropdown();
		 appt.appointmentCheckInStatus();
		 appt.bookedAppointmentOptionAfterCheckinOrCheckinGenerateBillCancelValidation();
		 appt.getAppointmentDropDownValue(1,"Click on  Preconsultaion");
		 bill.getEmrTopMenu(Login_Doctor.driver, logger).click();
		  bill.getEmrTopMenuElement(Login_Doctor.driver, logger, "Appointments").click();
		  Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
			 appt.getAppointmentDropdown();
			 appt.appointmentPreConsultingStatus();
			 appt.bookedAppointmentOptionCheckinAndNotGenerateBillAfterPreConsultationValidation();
			 appt.getAppointmentDropDownValue(1,"Click on  Preconsulting");
			 ASpage.getNotePreConsultPage(Login_Doctor.driver, logger).sendKeys("addNote");
			 ASpage.getSavePreConsultPage(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
		 appt.getAppointmentDropdown();
		 appt.appointmentPreConsultedStatus();
		 appt.bookedAppointmentOptionCheckinAndNotGenerateBillAfterPreConsultingValidation();
		 appt.getAppointmentDropDownValue(1,"Click on consult");
		 appt.afterConsultClickValidation();
		 ASpage.getNotePreConsultPage(Login_Doctor.driver, logger).sendKeys("addNote");
		 appt.getShareUpFollowConsultPageClick();
		 Web_GeneralFunctions.wait(5);
		 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		 Web_GeneralFunctions.wait(5);
		 appt.getAppointmentDropdown();
		 appt.appointmentFollowUpStatus();
		 appt.bookedAppointmentOptionPreBillAfterConsultShareFollowUpValidation();
		 appt.getAppointmentDropDownValue(1,"Click on FollowUp");
		 ASpage.getNotePreConsultPage(Login_Doctor.driver, logger).sendKeys("addNote");
		 appt.checkOutAndPrintConsultPageClick(); 
		 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.appointmentCheckedOutStatus();
		  appt.bookedAppointmentOptionAfterConsultCancelBillValidation();	 
	 }
		
	/*Test case:EMR_Standard_18*/
	 @Test(priority=41) 
	  public synchronized void  standardBillingCheckInAndGenerateBillValidation() throws Exception
	 {
		 logger = Reports.extent.createTest("EMR_Standard Billing check in and Generate Bill Validation");
		 bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
		  bs.rowCount=0;
		  bs. expectedNetTotalAmount=0;
		  bs.patientName=ConsultationTest.patientName;
		  appt.startBookingAppointment();
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionValidation();
		  appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		  bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		  bs.billPageDoctorAndPatientFieldValidation();
		  bs.billDefaultServiceValidation();
		  bs.billDefaultServiceLevelChangeValidation();
		  bs.deleteService();
		  bs.emrAppointmentGenerateBillValidation();
		  bs.paymentCashWallet();
		  bs.billingCreateBillPopUpValidation();
		  bs.billingCreateBillPdfValidation();
		  Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
	  }
	 
	 /*Test case:EMR_Standard_19*/
	 /*EMR_Standard_20  taken care in all test case*/
	@Test(priority=42) 
	  public synchronized void  standardBillingCheckInAndGenerateBillCancelValidation() throws Exception
	 {
		 logger = Reports.extent.createTest("EMR_Standard Billing check in and Generate Bill Cancel Validation");
		 bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
		  appt.startBookingAppointment();
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionValidation();
		  appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		  bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		  bill.getEmrCreateBillCancelButton(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinOrCheckinGenerateBillCancelValidation();
	 }
	@Test(priority=43) 
	  public synchronized void  standardBillingCheckInAndGenerateBillSaveDraftValidation() throws Exception
	 {
		 logger = Reports.extent.createTest("EMR_Standard Billing check in and Generate Bill Save Draft Validation");
		 bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
		  bs.rowCount=0;
		  bs. expectedNetTotalAmount=0;
		  bs.patientName=ConsultationTest.patientName;
		  appt.startBookingAppointment();
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionValidation();
		  appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		  bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		  bs.deleteService();
		  bs. emrAppointmentGenerateBillSaveDraftValidation();
		  bill.getEmrCreateBillSaveDraftButton(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinOrCheckinGenerateBillCancelValidation();
		  appt.getAppointmentDropDownValue(3,"Click on Generate Bill");
		  bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		  bs.billPageDoctorAndPatientFieldValidation();
		  bs.billSaveServiceValidation();
	  }
	
	 /*Test case:EMR_Standard_21*/
    @Test(priority=44) 
	  public synchronized void  standardBillingCheckInAndGenerateBillPreConsultationValidation() throws Exception
	 {

		 logger = Reports.extent.createTest("EMR_Standard Billing checkIn and Generate Bill PreConsultation Validation");
		 bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
		  bs.rowCount=0;
		  bs. expectedNetTotalAmount=0;
		  bs.patientName=ConsultationTest.patientName;
		  appt.startBookingAppointment();
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionValidation();
		  appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		  bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		  bs.billPageDoctorAndPatientFieldValidation();
		  bs.billDefaultServiceValidation();
		  bs.clickGenerateButtonAndClosePopUp();
		  Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.getAppointmentDropDownValue(1,"Click on Pre Consulation");
		  bill.getEmrTopMenu(Login_Doctor.driver, logger).click();
		  bill.getEmrTopMenuElement(Login_Doctor.driver, logger, "Appointments").click();
		  Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionCheckinAndGenerateBillAfterPreConsultationValidation();
	  }
	 
    /*Test case:EMR_Standard_22*/
	@Test(priority=45) 
	  public synchronized void  standardBillingCheckInAndGenerateBillConsultValidation() throws Exception
	 {

		 logger = Reports.extent.createTest("EMR_Standard Billing checkIn and Generate Bill Consult Validation");
		 bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
		  bs.rowCount=0;
		  bs. expectedNetTotalAmount=0;
		  bs.patientName=ConsultationTest.patientName;
		  appt.startBookingAppointment();
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionValidation();
		  appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		  bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		  bs.billPageDoctorAndPatientFieldValidation();
		  bs.billDefaultServiceValidation();
		  bs.clickGenerateButtonAndClosePopUp();
		  Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.getAppointmentDropDownValue(2,"Click on Consult");
		  appt.afterConsultClickValidation();
		  bill.getEmrTopMenu(Login_Doctor.driver, logger).click();
		  bill.getEmrTopMenuElement(Login_Doctor.driver, logger, "Appointments").click();
		  Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionCheckinAndGenerateBillAfterConsultValidation();
	  }
	 
	 /*Test case:EMR_Standard_23,RGB_9*/
	 @Test(priority=46) 
	  public synchronized void  standardBillingCheckInGenerateBillAndConsultShareFollowUpValidation() throws Exception
	 {

		 logger = Reports.extent.createTest("EMR_Standard Billing checkIn Generate Bill And Consult Share follow Up Validation");
		 bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
		  bs.rowCount=0;
		  bs. expectedNetTotalAmount=0;
		  bs.patientName=ConsultationTest.patientName;
		  appt.startBookingAppointment();
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionValidation();
		  appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		  bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		  bs.billPageDoctorAndPatientFieldValidation();
		  bs.billDefaultServiceValidation();
		  bs.clickGenerateButtonAndClosePopUp();
		  Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.getAppointmentDropDownValue(2,"Click on Consult");
		  appt.afterConsultClickValidation();
		  ASpage.getNotePreConsultPage(Login_Doctor.driver, logger).sendKeys("addNote");
		  appt.getShareUpFollowConsultPageClick();
		  Web_GeneralFunctions.wait(5);
		 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		 Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionPreBillGenerateAfterConsultShareFollowUpValidation();
		  
	  }
	 /*Test case:EMR_Standard_24,RGB_8*/
	@Test(priority=47) 
	  public synchronized void standardBillingCheckInGenerateBillAndConsultCheckout() throws Exception
	 {

		 logger = Reports.extent.createTest("EMR_Standard Billing checkIn Generate Bill And Consult Checkout Validation");
		 bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
		  bs.rowCount=0;
		  bs. expectedNetTotalAmount=0;
		  bs.patientName=ConsultationTest.patientName;
		  appt.startBookingAppointment();
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionValidation();
		  appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		  bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		  bs.billPageDoctorAndPatientFieldValidation();
		  bs.billDefaultServiceValidation();
		  bs.clickGenerateButtonAndClosePopUp();
		  Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.getAppointmentDropDownValue(2,"Click on Consult");
		  appt.afterConsultClickValidation();
		  appt.checkOutAndPrintConsultPageClick();
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterConsultGenerateBillValidation();	

}
	 public synchronized void standardBillingAppointmentConsumerValidation() throws Exception
	 {
	  logger = Reports.extent.createTest("EMR_Standard Billing Appointment Consumer Validation");
	  appt.getAppointmentDropdown();
		 appt.appointmentBookedStatus();
		 appt.bookedAppointmentOptionValidation();
		 appt.getAppointmentDropDownValue(1,"Click on check in");
		 Web_GeneralFunctions.wait(5);
		 appt.getAppointmentDropdown();
		 appt.appointmentCheckInStatus();
		 appt.bookedAppointmentOptionAfterCheckinOrCheckinGenerateBillCancelValidation();
		 appt.getAppointmentDropDownValue(1,"Click on  Preconsultaion");
		 bill.getEmrTopMenu(Login_Doctor.driver, logger).click();
		  bill.getEmrTopMenuElement(Login_Doctor.driver, logger, "Appointments").click();
		  Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
			 appt.getAppointmentDropdown();
			 appt.appointmentPreConsultingStatus();
			 appt.bookedAppointmentOptionCheckinAndNotGenerateBillAfterPreConsultationValidation();
			 appt.getAppointmentDropDownValue(1,"Click on  Preconsulting");
			 ASpage.getNotePreConsultPage(Login_Doctor.driver, logger).sendKeys("addNote");
			 ASpage.getSavePreConsultPage(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
		 appt.getAppointmentDropdown();
		 appt.appointmentPreConsultedStatus();
		 appt.bookedAppointmentOptionCheckinAndNotGenerateBillAfterPreConsultingValidation();
		 appt.getAppointmentDropDownValue(1,"Click on consult");
		 appt.afterConsultClickValidation();
		 ASpage.getNotePreConsultPage(Login_Doctor.driver, logger).sendKeys("addNote");
		 appt.getShareUpFollowConsultPageClick();
		 Web_GeneralFunctions.wait(5);
		 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		 Web_GeneralFunctions.wait(5);
		 appt.getAppointmentDropdown();
		 appt.appointmentFollowUpStatus();
		 appt.bookedAppointmentOptionPreBillAfterConsultShareFollowUpValidation();
		 appt.getAppointmentDropDownValue(1,"Click on FollowUp");
		  appt.checkOutAndPrintConsultPageClick();
		  Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.appointmentCheckedOutStatus();
		  appt.bookedAppointmentOptionAfterConsultCancelBillValidation();			 
	 }


}
