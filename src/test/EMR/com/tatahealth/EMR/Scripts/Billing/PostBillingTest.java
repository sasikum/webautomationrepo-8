package com.tatahealth.EMR.Scripts.Billing;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Consultation.ConsultationTest;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.AppointmentDashboard.AllSlotsPage;
import com.tatahealth.EMR.pages.Billing.Billing;
import com.tatahealth.EMR.pages.Consultation.ConsultationPage;
import com.tatahealth.EMR.pages.Login.LoginPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class PostBillingTest {
	BillingTest bs=new BillingTest();
	Billing bill = new Billing();
	LoginPage loginPage = new LoginPage();
	MasterAdministrationTest masterAdmin=new MasterAdministrationTest();
	ConsultationTest appt=new ConsultationTest();
	AllSlotsPage ASpage = new AllSlotsPage();
	ConsultationPage consultation = new ConsultationPage();
	public static ExtentTest logger;
	
	  @BeforeClass(alwaysRun = true) public static void beforeClass() throws Exception { 
		  Reports.reports();
		  Login_Doctor.executionName="PostBillingTest";
		  BillingTest.doctorName="Topaa";
		  Login_Doctor.LoginTestwithDiffrentUser(BillingTest.doctorName);
			System.out.println("Initialized Driver");
			ConsultationTest.patientName="Ballu";
		  }
	  
	  @AfterClass(alwaysRun = true) public static void afterClass() throws Exception {
		  Login_Doctor.LogoutTest();
			System.out.println("Closed Driver");
		  Reports.extent.flush(); 
		  }
	@Test(priority=28) 
	  public synchronized void postBillingMasterValidation() throws Exception
	 {
	  logger = Reports.extent.createTest("EMR_Post Billing Master Validation");
	  bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
	  bill.getEmrTopMenu(Login_Doctor.driver, logger).click();
	  masterAdmin.masterAdministrationBillType("Post-Billing");
	  Web_GeneralFunctions.wait(4);
	  Login_Doctor.Logout();
	  Login_Doctor.LoginTestwithoutNewInstance(BillingTest.doctorName);
	  }
	
	/*Test case:EMR_Bill_086-EMR_Bill_087,EMR_Post billing_39*/
	/*EMR_Post billing_41 user specific*/
	@Test(priority=29) 
	 public synchronized void postBillingRescheduleAppointmentValidation() throws Exception
	 {
		 logger = Reports.extent.createTest("EMR_Post Billing Reschedule Appointment Validation");
		 bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 appt.startBookingAppointment();
			 appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(4,"Click on Reschedule");
			 appt.appointmentReschedule();
			 Web_GeneralFunctions.wait(5);
			 appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(1,"Click on check in");
			 Web_GeneralFunctions.wait(5);
			 appt.getAppointmentDropdown();
			 appt.appointmentCheckInStatus();
			 appt.bookedAppointmentOptionAfterCheckinOrCheckinGenerateBillCancelValidation();
			 appt.getAppointmentDropDownValue(1,"Click on  Preconsultaion");
			 bill.getEmrTopMenu(Login_Doctor.driver, logger).click();
			  bill.getEmrTopMenuElement(Login_Doctor.driver, logger, "Appointments").click();
			  Web_GeneralFunctions.wait(5);
			  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			  Web_GeneralFunctions.wait(5);
				 appt.getAppointmentDropdown();
				 appt.appointmentPreConsultingStatus();
				 appt.bookedAppointmentOptionCheckinAndNotGenerateBillAfterPreConsultationValidation();
				 appt.getAppointmentDropDownValue(1,"Click on  Preconsulting");
				 ASpage.getNotePreConsultPage(Login_Doctor.driver, logger).sendKeys("addNote");
				 ASpage.getSavePreConsultPage(Login_Doctor.driver, logger).click();
				 Web_GeneralFunctions.wait(5);;
			 appt.getAppointmentDropdown();
			 appt.appointmentPreConsultedStatus();
			 appt.bookedAppointmentOptionCheckinAndNotGenerateBillAfterPreConsultingValidation();
			 appt.getAppointmentDropDownValue(1,"Click on consult");
			 appt.afterConsultClickValidation();
			 ASpage.getNotePreConsultPage(Login_Doctor.driver, logger).sendKeys("addNote");
			 appt.getShareUpFollowConsultPageClick();
			 Web_GeneralFunctions.wait(5);
			 bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
			  bill.getEmrCreateBillCancelButton(Login_Doctor.driver, logger).click();
			  Web_GeneralFunctions.wait(5);
			 appt.getAppointmentDropdown();
			 appt.appointmentFollowUpStatus();
			 appt.bookedAppointmentOptionPreBillAfterConsultShareFollowUpValidation();
			 appt.getAppointmentDropDownValue(1,"Click on FollowUp");
			 ASpage.getNotePreConsultPage(Login_Doctor.driver, logger).sendKeys("addNote");
		    appt.checkOutAndBillPatientConsultPageClick();
		    bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		   bill.getEmrCreateBillCancelButton(Login_Doctor.driver, logger).click();
		   Web_GeneralFunctions.wait(5); 
		   appt.getAppointmentDropdown();
		  appt.appointmentCheckedOutStatus();
		  appt.bookedAppointmentOptionAfterConsultCancelBillValidation();
		 
			 
	 }
	
	
	 /*Test case:Bill_cond_006-Bill_cond_007,EMR_Post billing_29-EMR_Post billing_31,EMR_Post billing_42*/
	@Test(priority=30) 
	 public synchronized void postBillingAppointmentValidation() throws Exception
	 {
		 logger = Reports.extent.createTest("EMR_Post Billing Appointment Validation");
		 bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 appt.startBookingAppointment();
			 appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(1,"Click on check in");
			 Web_GeneralFunctions.wait(5);
			 appt.getAppointmentDropdown();
			 appt.appointmentCheckInStatus();
			 appt.bookedAppointmentOptionAfterCheckinOrCheckinGenerateBillCancelValidation();
			 appt.getAppointmentDropDownValue(1,"Click on  Preconsultaion");
			 bill.getEmrTopMenu(Login_Doctor.driver, logger).click();
			  bill.getEmrTopMenuElement(Login_Doctor.driver, logger, "Appointments").click();
			  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			  Web_GeneralFunctions.wait(5);
				 appt.getAppointmentDropdown();
				 appt.appointmentPreConsultingStatus();
				 appt.bookedAppointmentOptionCheckinAndNotGenerateBillAfterPreConsultationValidation();
				 appt.getAppointmentDropDownValue(1,"Click on  Preconsulting");
				 ASpage.getNotePreConsultPage(Login_Doctor.driver, logger).sendKeys("addNote");
				 ASpage.getSavePreConsultPage(Login_Doctor.driver, logger).click();
				 Web_GeneralFunctions.wait(5);
			 appt.getAppointmentDropdown();
			 appt.appointmentPreConsultedStatus();
			 appt.bookedAppointmentOptionCheckinAndNotGenerateBillAfterPreConsultingValidation();
			 appt.getAppointmentDropDownValue(1,"Click on consult");
			 appt.afterConsultClickValidation();
			 ASpage.getNotePreConsultPage(Login_Doctor.driver, logger).sendKeys("addNote");
			 appt.getShareUpFollowConsultPageClick();
			 Web_GeneralFunctions.wait(5);
			 bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
			  bill.getEmrCreateBillCancelButton(Login_Doctor.driver, logger).click();
			  Web_GeneralFunctions.wait(5);
			 appt.getAppointmentDropdown();
			 appt.appointmentFollowUpStatus();
			 appt.bookedAppointmentOptionPreBillAfterConsultShareFollowUpValidation();
			 appt.getAppointmentDropDownValue(1,"Click on FollowUp");
			 ASpage.getNotePreConsultPage(Login_Doctor.driver, logger).sendKeys("addNote");
		    appt.checkOutAndBillPatientConsultPageClick();
		    bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		    bill.getEmrCreateBillCancelButton(Login_Doctor.driver, logger).click();
		    Web_GeneralFunctions.wait(5); appt.getAppointmentDropdown();
		    appt.appointmentCheckedOutStatus();
		      appt.bookedAppointmentOptionAfterConsultCancelBillValidation();
		
			 
	 }
	
	 /*Test case:EMR_Post billing_32-EMR_Post billing_33*/
	 @Test(priority=31) 
	  public synchronized void postBillingCheckInAndGenerateBillValidation() throws Exception
	 {
		 logger = Reports.extent.createTest("EMR_Post Billing check in and Generate Bill Validation");
		 bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
		  bs.rowCount=0;
		  bs. expectedNetTotalAmount=0;
		  bs.patientName=ConsultationTest.patientName;
		  appt.startBookingAppointment();
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionValidation();
		  appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		  bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		  bs.billPageDoctorAndPatientFieldValidation();
		  bs.billDefaultServiceValidation();
		  bs.billDefaultServiceLevelChangeValidation();
		  bs.deleteService();
		  bs.emrAppointmentGenerateBillValidation();
		  bs.paymentCashWallet();
		  bs.billingCreateBillPopUpValidation();
		  bs.billingCreateBillPdfValidation();
		  Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
	  }
	@Test(priority=32) 
	  public synchronized void postBillingCheckInAndGenerateBillCancelValidation() throws Exception
	 {
		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		  appt.startBookingAppointment();
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionValidation();
		  appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		  bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		  bill.getEmrCreateBillCancelButton(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinOrCheckinGenerateBillCancelValidation();
	  }
	 /*Test case:Bill_cond_008*/
	 @Test(priority=33) 
	  public synchronized void postBillingCheckInAndGenerateBillSaveDraftValidation() throws Exception
	 {
		 logger = Reports.extent.createTest("EMR_Post Billing check in and Generate Bill Save Draft Validation");
		 bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
		  bs.patientName=ConsultationTest.patientName;
		  bs.expectedNetTotalAmount=0;
		  bs.rowCount=0;
		  appt.startBookingAppointment();
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionValidation();
		  appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		  bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		  Web_GeneralFunctions.wait(5);
		  bs.deleteService();
		  bs. emrAppointmentGenerateBillSaveDraftValidation();
		  bill.getEmrCreateBillSaveDraftButton(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinOrCheckinGenerateBillCancelValidation();
		  appt.getAppointmentDropDownValue(3,"Click on Generate Bill");
		  bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		  bs.billPageDoctorAndPatientFieldValidation();
		  bs.billSaveServiceValidation();
	  }
	 /*Test case:EMR_Post billing_34*/
	@Test(priority=34) 
	  public synchronized void postBillingCheckInAndGenerateBillPreConsultationValidation() throws Exception
	 {

		 logger = Reports.extent.createTest("EMR_Post Billing check in and Generate Bill PreConsultation Validation");
		 bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
		  bs.rowCount=0;
		  bs. expectedNetTotalAmount=0;
		  bs.patientName=ConsultationTest.patientName;
		  appt.startBookingAppointment();
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionValidation();
		  appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		  bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		  bs.billPageDoctorAndPatientFieldValidation();
		  bs.billDefaultServiceValidation();
		  bs.clickGenerateButtonAndClosePopUp();
		  Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.getAppointmentDropDownValue(1,"Click on Pre Consulation");
		  bill.getEmrTopMenu(Login_Doctor.driver, logger).click();
		  bill.getEmrTopMenuElement(Login_Doctor.driver, logger, "Appointments").click();
		  Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionCheckinAndGenerateBillAfterPreConsultationValidation();
	  }
	 
	 /*Test case:EMR_Bill_088-EMR_Bill_092,Bill_cond_005,EMR_Post billing_35*/
      @Test(priority=35) 
	  public synchronized void postBillingCheckInAndGenerateBillConsultValidation() throws Exception
	 {

		 logger = Reports.extent.createTest("EMR_Post Billing checkIn and Generate Bill Consult Validation");
		 bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
		  bs.rowCount=0;
		  bs. expectedNetTotalAmount=0;
		  bs.patientName=ConsultationTest.patientName;
		  appt.startBookingAppointment();
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionValidation();
		  appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		  bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		  bs.billPageDoctorAndPatientFieldValidation();
		  bs.billDefaultServiceValidation();
		  bs.clickGenerateButtonAndClosePopUp();
		  Web_GeneralFunctions.wait(5);;
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.getAppointmentDropDownValue(2,"Click on Consult");
		  appt.afterConsultClickValidation();
		  bill.getEmrTopMenu(Login_Doctor.driver, logger).click();
		  bill.getEmrTopMenuElement(Login_Doctor.driver, logger, "Appointments").click();
		  Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionCheckinAndGenerateBillAfterConsultValidation();
	  }
	 
      /*Test case:EMR_Post billing_36,RGB_9*/
	@Test(priority=36) 
	  public synchronized void postBillingCheckInGenerateBillAndConsultShareFollowUpValidation() throws Exception
	 {

		 logger = Reports.extent.createTest("EMR_Post Billing checkIn Generate Bil And Consult Share FollowUp Validation");
		 bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
		  bs.rowCount=0;
		  bs. expectedNetTotalAmount=0;
		  bs.patientName=ConsultationTest.patientName;
		  appt.startBookingAppointment();
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionValidation();
		  appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		  bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		  bs.billPageDoctorAndPatientFieldValidation();
		  bs.billDefaultServiceValidation();
		  bs.clickGenerateButtonAndClosePopUp();
		  Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.getAppointmentDropDownValue(2,"Click on Consult");
		  appt.afterConsultClickValidation();
		  ASpage.getNotePreConsultPage(Login_Doctor.driver, logger).sendKeys("addNote");
		  appt.getShareUpFollowConsultPageClick();
		  Web_GeneralFunctions.wait(5);
		 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		 Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionPreBillGenerateAfterConsultShareFollowUpValidation();
		  
	  }
	 
	/*Test case:EMR_Post billing_37,EMR_Post billing_43,RGB_8*/
	/*,EMR_Post billing_44 taken care in every scenario*/
	 @Test(priority=37) 
	  public synchronized void postBillingCheckInGenerateBillAndConsultCheckout() throws Exception
	 {

		 logger = Reports.extent.createTest("EMR_Post Billing checkIn Generate Bill And Consultation checkout Validation");
		 bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
		  bs.rowCount=0;
		  bs. expectedNetTotalAmount=0;
		  bs.patientName=ConsultationTest.patientName;
		  appt.startBookingAppointment();
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionValidation();
		  appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		  bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		  bs.billPageDoctorAndPatientFieldValidation();
		  bs.billDefaultServiceValidation();
		  bs.clickGenerateButtonAndClosePopUp();
		  Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.getAppointmentDropDownValue(2,"Click on Consult");
		  appt.afterConsultClickValidation();
		  appt.checkOutAndPrintConsultPageClick();
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterConsultGenerateBillValidation();	
	  }
	  
	  public synchronized void billingAppointmentConsumerValidation() throws Exception
		 {
		  logger = Reports.extent.createTest("EMR_Post Billing Appointment Consumer Validation");
		  bs.rowCount=0;
				 appt.getAppointmentDropdown();
				 appt.appointmentBookedStatus();
				 appt.bookedAppointmentOptionValidation();
				 appt.getAppointmentDropDownValue(1,"Click on check in");
				 Web_GeneralFunctions.wait(5);
				 appt.getAppointmentDropdown();
				 appt.appointmentCheckInStatus();
				 appt.bookedAppointmentOptionAfterCheckinOrCheckinGenerateBillCancelValidation();
				 appt.getAppointmentDropDownValue(1,"Click on  Preconsultaion");
				 bill.getEmrTopMenu(Login_Doctor.driver, logger).click();
				  bill.getEmrTopMenuElement(Login_Doctor.driver, logger, "Appointments").click();
				  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
				  Web_GeneralFunctions.wait(5);
					 appt.getAppointmentDropdown();
					 appt.appointmentPreConsultingStatus();
					 appt.bookedAppointmentOptionCheckinAndNotGenerateBillAfterPreConsultationValidation();
					 appt.getAppointmentDropDownValue(1,"Click on  Preconsulting");
					 ASpage.getNotePreConsultPage(Login_Doctor.driver, logger).sendKeys("addNote");
					 ASpage.getSavePreConsultPage(Login_Doctor.driver, logger).click();
					 Web_GeneralFunctions.wait(5);
				 appt.getAppointmentDropdown();
				 appt.appointmentPreConsultedStatus();
				 appt.bookedAppointmentOptionCheckinAndNotGenerateBillAfterPreConsultingValidation();
				 appt.getAppointmentDropDownValue(1,"Click on consult");
				 appt.afterConsultClickValidation();
				 ASpage.getNotePreConsultPage(Login_Doctor.driver, logger).sendKeys("addNote");
				 appt.getShareUpFollowConsultPageClick();
				 Web_GeneralFunctions.wait(5);
				 bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
				 bs.billPageDoctorAndPatientFieldValidation();
				 bs.billingCreateBillServiceSectionColumnDisplay();
				 bs.billConsumerServiceReadOnlyValiadtion();
				  bs.billConsumerCFPcServiceValidation();
				  bill.getEmrCreateBillCancelButton(Login_Doctor.driver, logger).click();
				  Web_GeneralFunctions.wait(5);
				 appt.getAppointmentDropdown();
				 appt.appointmentFollowUpStatus();
				 appt.bookedAppointmentOptionPreBillAfterConsultShareFollowUpValidation();
				 appt.getAppointmentDropDownValue(1,"Click on FollowUp");
				 ASpage.getNotePreConsultPage(Login_Doctor.driver, logger).sendKeys("addNote");
		         appt.checkOutAndBillPatientConsultPageClick();
		          bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		        bs.billPageDoctorAndPatientFieldValidation();
		       bs.billingCreateBillServiceSectionColumnDisplay();
		         bs.billConsumerServiceReadOnlyValiadtion();
		  bs.billConsumerCFPcServiceValidation();
		  bill.getEmrCreateBillCancelButton(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.appointmentCheckedOutStatus();
		  appt.bookedAppointmentOptionAfterConsultCancelBillValidation();
		 
		 }

}
