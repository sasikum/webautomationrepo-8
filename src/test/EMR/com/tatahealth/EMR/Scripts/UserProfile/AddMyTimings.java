package com.tatahealth.EMR.Scripts.UserProfile;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.AppointmentDashboard.CommonsPage;
import com.tatahealth.EMR.pages.Login.LoginPage;
import com.tatahealth.EMR.pages.Login.SweetAlertPage;
import com.tatahealth.EMR.pages.UserAdministration.UserAdministration;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class AddMyTimings extends UserProfile {

	public static WebDriver driver;

	@BeforeClass(alwaysRun = true)
	public static void beforeClass() throws Exception {
		// Login_Doctor.LoginTestwithDiffrentUser("SonalinQAEMROne");
		Login_Doctor.LoginTestwithDiffrentUser("DontBooKApptsFromThisDoc"); // staging

		System.out.println("Initialized Driver");
	}

	@AfterClass(groups = { "Regression", "AddMyTimings " })
	public static void afterClass() throws Exception {

		Login_Doctor.LogoutTest();
		System.out.println("Closed Driver");
	}

	ExtentTest logger;
	UserAdministration userAdmin = new UserAdministration();
	LoginPage loginPage = new LoginPage();
	CommonsPage commonPage = new CommonsPage();
	SweetAlertPage swaPage = new SweetAlertPage();

	public static Date addDate(int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DATE, days); // minus number would decrement the days
		return cal.getTime();
	}

	public static String formatDate(Date date, String dateFormat) {
		SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
		return formatter.format(date);
	}

	public static String formatDate(Date date) {
		return formatDate(date, "dd-MM-yyyy");
	}

	public synchronized void clickOnMyTimingsLink() {
		try {
			Web_GeneralFunctions.click(getMyTimingslink(Login_Doctor.driver, logger), "Clicked on MyTimings  link",
					Login_Doctor.driver, logger);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public synchronized void clickOnAddTimingsBtnInTimings() {
		try {
			Web_GeneralFunctions.click(getAddTimingBtn(Login_Doctor.driver, logger),
					"Clicked on save button in Timings ", Login_Doctor.driver, logger);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public synchronized void clickOnAddTimingsRowBtnInTimings() {
		try {
			Web_GeneralFunctions.click(getAddRowTimeBtn(Login_Doctor.driver, logger),
					"Clicked on save button in Timings ", Login_Doctor.driver, logger);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public synchronized void clickOnSaveBtnInTimings() {
		try {
			Web_GeneralFunctions.click(getSaveTimingsBtn(Login_Doctor.driver, logger),
					"Clicked on save button in Timings ", Login_Doctor.driver, logger);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public synchronized void clickOnEditTimings() {
		try {
			Web_GeneralFunctions.click(getTimeEditBtn(Login_Doctor.driver, logger),
					"Clicked on edit button in Timings ", Login_Doctor.driver, logger);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public synchronized void clickOnDeletTimings() {
		try {
			Web_GeneralFunctions.click(getTimeDeleteBtn(Login_Doctor.driver, logger),
					"Clicked on edit button in Timings ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(1);

			Web_GeneralFunctions.click(getYesBtnOnAlt(Login_Doctor.driver, logger), "Clicked on yes button in  alrt ",
					Login_Doctor.driver, logger);

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public synchronized void fillDates(String currentDate, String futureDate) {
		try {
			Web_GeneralFunctions.click(getTimeFromDateTbx(Login_Doctor.driver, logger),
					"Clciked on from date in Timings ", Login_Doctor.driver, logger);

			Web_GeneralFunctions.sendkeys(getTimeFromDateTbx(Login_Doctor.driver, logger), Keys.BACK_SPACE,
					"filling currentDate in timings", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(getTimeFromDateTbx(Login_Doctor.driver, logger), Keys.BACK_SPACE,
					"filling currentDate in timings", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(getTimeFromDateTbx(Login_Doctor.driver, logger), Keys.BACK_SPACE,
					"filling currentDate in timings", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(getTimeFromDateTbx(Login_Doctor.driver, logger), Keys.BACK_SPACE,
					"filling currentDate in timings", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(getTimeFromDateTbx(Login_Doctor.driver, logger), Keys.BACK_SPACE,
					"filling currentDate in timings", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(getTimeFromDateTbx(Login_Doctor.driver, logger), Keys.BACK_SPACE,
					"filling currentDate in timings", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(getTimeFromDateTbx(Login_Doctor.driver, logger), Keys.BACK_SPACE,
					"filling currentDate in timings", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(getTimeFromDateTbx(Login_Doctor.driver, logger), Keys.BACK_SPACE,
					"filling currentDate in timings", Login_Doctor.driver, logger);
			// Web_GeneralFunctions.clear(getTimeFromDateTbx(Login_Doctor.driver,
			// logger),"cleared from date in Timings ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(getTimeFromDateTbx(Login_Doctor.driver, logger), currentDate,
					"filling currentDate in timings", Login_Doctor.driver, logger);
			System.out.println("...." + currentDate);
			Web_GeneralFunctions.sendkeys(getTimeFromDateTbx(Login_Doctor.driver, logger), Keys.TAB,
					"filling currentDate in timings", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(getTimeToDateTbx(Login_Doctor.driver, logger), "Clciked on to date in Timings ",
					Login_Doctor.driver, logger);

			Web_GeneralFunctions.clear(getTimeToDateTbx(Login_Doctor.driver, logger), "cleared to date in Timings ",
					Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(getTimeToDateTbx(Login_Doctor.driver, logger), futureDate,
					"filling futureDate in timings", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(getTimeToDateTbx(Login_Doctor.driver, logger), Keys.TAB,
					"filling futureDate in timings", Login_Doctor.driver, logger);

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public synchronized void fillTimingsForMonday(String satartTiming, String endTimigs) {
		try {

			Web_GeneralFunctions.click(getMonBtn(Login_Doctor.driver, logger), "Clciked on monday in Timings ",
					Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(1);

			Web_GeneralFunctions.click(getMonStartTimeBtn(Login_Doctor.driver, logger),
					"Clciked on from date in Timings ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.clear(getMonStartTimeBtn(Login_Doctor.driver, logger),
					"Clciked on from date in Timings ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(getMonStartTimeBtn(Login_Doctor.driver, logger), satartTiming,
					"filling satartTiming in timings", Login_Doctor.driver, logger);

			Web_GeneralFunctions.click(getMonEndTimeBtn(Login_Doctor.driver, logger),
					"Clciked on endTimigs tbx in Timings ", Login_Doctor.driver, logger);

			Web_GeneralFunctions.clear(getMonEndTimeBtn(Login_Doctor.driver, logger),
					"Clciked on endTimigs tbx in Timings ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(getMonEndTimeBtn(Login_Doctor.driver, logger), endTimigs,
					"filling endTimigs in timings", Login_Doctor.driver, logger);

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public synchronized void fillConsultTypeForMonday() {
		try {

			Web_GeneralFunctions.click(getF2FCbx(Login_Doctor.driver, logger), "Clciked on monday in Timings ",
					Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(1);

			Web_GeneralFunctions.click(getVCCbx(Login_Doctor.driver, logger), "Clciked on endTimigs tbx in Timings ",
					Login_Doctor.driver, logger);

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	@Test(priority = 1020, groups = { "Regression", "AddMyTimings" })

	public synchronized void saveDateWithOutData() throws Exception {

		logger = Reports.extent.createTest("AddMyTimings");
		Web_GeneralFunctions.wait(1);
		Web_GeneralFunctions.click(getUserInformationIcon(Login_Doctor.driver, logger),
				"Clicked on  UserInformationIcon", Login_Doctor.driver, logger);
		// Web_GeneralFunctions.waitForElement(driver,
		// getMyQualificationslink(Login_Doctor.driver, logger));
		clickOnMyTimingsLink();
		clickOnAddTimingsBtnInTimings();
		Web_GeneralFunctions.wait(1);
		clickOnSaveBtnInTimings();
		// validated error message below
	}

	// @Test(priority =1021, groups = { "Regression", "AddMyTimings" })

	public synchronized void fillPriviousDate() throws Exception {

		logger = Reports.extent.createTest("");
		Web_GeneralFunctions.wait(1);
		fillDates(formatDate(addDate(-2)), "");
		System.out.println(".................1021");

	}

	@Test(priority = 1022, groups = { "Regression", "AddMyTimings" })

	public synchronized void fillDates() throws Exception {

		logger = Reports.extent.createTest(" ");
		Web_GeneralFunctions.wait(1);
		fillDates(formatDate(addDate(0)), formatDate(addDate(2)));
		System.out.println(".................1022");

	}

	@Test(priority = 1023, groups = { "Regression", "AddMyTimings" })

	public synchronized void fillTimings() throws Exception {

		logger = Reports.extent.createTest(" ");
		Web_GeneralFunctions.wait(1);
		clickOnAddTimingsRowBtnInTimings();
		Web_GeneralFunctions.wait(1);
		fillTimingsForMonday("10:00 AM", "12:00 PM");
		fillConsultTypeForMonday();
	}

	@Test(priority = 1024, groups = { "Regression", "AddMyTimings" })

	public synchronized void SaveTimings() throws Exception {

		logger = Reports.extent.createTest(" ");
		Web_GeneralFunctions.wait(1);
		fillTimingsForMonday("10:00 AM", "12:00 PM");
		fillConsultTypeForMonday();
	}

	@Test(priority = 1023, groups = { "Regression", "AddMyTimings" })

	public synchronized void EditTime() throws Exception {

		logger = Reports.extent.createTest("AddMyTimings");
		Web_GeneralFunctions.wait(1);
		clickOnEditTimings();

		clickOnSaveBtnInTimings();
		Web_GeneralFunctions.wait(3);

	}

	@Test(priority = 1025, groups = { "Regression", "AddMyTimings" })

	public synchronized void DeleteMyTime() throws Exception {

		System.out.println("...............................................................");
		logger = Reports.extent.createTest("Delete my time");
		Web_GeneralFunctions.wait(1);
		clickOnDeletTimings();

		System.out.println("...............................................................");

		// Validate error message
	}

}
