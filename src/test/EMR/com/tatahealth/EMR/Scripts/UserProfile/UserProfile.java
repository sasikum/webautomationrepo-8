package com.tatahealth.EMR.Scripts.UserProfile;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.AppointmentDashboard.CommonsPage;
import com.tatahealth.EMR.pages.Login.LoginPage;
import com.tatahealth.EMR.pages.UserAdministration.UserAdministration;
import com.tatahealth.EMR.pages.UserProfile.UserProfilePage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class UserProfile extends UserProfilePage

{
	public static WebDriver driver;

	
	 
	@BeforeClass(alwaysRun = true)
	public static void beforeClass() throws Exception {
		Login_Doctor.LoginTestwithDiffrentUser("StgDocforstaff");
		// Login_Doctor.LoginTestwithDiffrentUser("Digitizer");
		// Login_Doctor.LoginTestwithDiffrentUser("Doctor");
		System.out.println("Initialized Driver");
	}

	@AfterClass(groups = { "Regression", "Appointment_Doctor" })
	public static void afterClass() throws Exception {

		Login_Doctor.LogoutTest();
		System.out.println("Closed Driver");
	}

	ExtentTest logger;
	UserAdministration userAdmin = new UserAdministration();
	LoginPage loginPage = new LoginPage();
	CommonsPage commonPage = new CommonsPage();

	@Test(priority = 1, groups = { "Regression", "User_Profile" })
	// please run the below test case with
	// Login_Doctor.LoginTestwithDiffrentUser("StgDocforstaff");

	public synchronized void userProfile_01() throws Exception {
		String serviceOfferedByClinic2;
		String serviceOfferedByClinic1;

		logger = Reports.extent.createTest("userProfile_01");
		Thread.sleep(1000);
		Web_GeneralFunctions.click(commonPage.getSecondClinic(Login_Doctor.driver, logger), "Clicked on  2nd Clinic",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(getUserInformationIcon(Login_Doctor.driver, logger),
				"Clicked on  UserInformationIcon", Login_Doctor.driver, logger);
		Thread.sleep(10000);
		Web_GeneralFunctions.click(getuserProfileLink(Login_Doctor.driver, logger),
				"Clicked on userAdministarion profile link", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Assert.assertTrue(getServicesOfferedTbx(Login_Doctor.driver, logger).isDisplayed(),
				"ServicesOfferedTbx is present");

		getServicesOfferedTbx(Login_Doctor.driver, logger).clear();
		Web_GeneralFunctions.sendkeys(getServicesOfferedTbx(Login_Doctor.driver, logger), "Auto_doctor_user_profile",
				"filled ServicesOffered name", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(getSaveBtn(Login_Doctor.driver, logger), "clicked on  Save", Login_Doctor.driver,
				logger);
		Thread.sleep(1000);

		serviceOfferedByClinic2 = getServicesOfferedTbx(Login_Doctor.driver, logger).getAttribute("value");
		System.out.println("............." + serviceOfferedByClinic2);
		Thread.sleep(10000);

		Web_GeneralFunctions.click(getUserProfileCloseBtn(Login_Doctor.driver, logger),
				"clicked on  Close button of user profile", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(userAdmin.getEmrMenu(Login_Doctor.driver, logger), "Clicked on  emrMenu",
				Login_Doctor.driver, logger);
		Thread.sleep(10000);
		Web_GeneralFunctions.click(userAdmin.getUserAdministrationLnk(Login_Doctor.driver, logger),
				"Clicked on userAdministarion link ", Login_Doctor.driver, logger);
		Thread.sleep(1000);

		Web_GeneralFunctions.click(userAdmin.getSearchUserTextBox(Login_Doctor.driver, logger),
				"clicked on  SearchUserTextBox", Login_Doctor.driver, logger);

		Web_GeneralFunctions.sendkeys(userAdmin.getSearchUserTextBox(Login_Doctor.driver, logger), "StgDocforstaff",
				"filled doctor name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getSearchUserBtn(Login_Doctor.driver, logger), "clicked on  SearchUserBtn",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(userAdmin.getEditUserBtn(Login_Doctor.driver, logger), "Clicked on getEditUserBtn",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);

		Assert.assertEquals(userAdmin.getClinicServicesTextBox(Login_Doctor.driver, logger).getAttribute("value"),
				serviceOfferedByClinic2, "Clinic service offered is not matching ");

		Web_GeneralFunctions.click(commonPage.getlogOutDropdown(Login_Doctor.driver, logger),
				"Clicked on  getlogOutImg", Login_Doctor.driver, logger);
		Thread.sleep(1000);

		Web_GeneralFunctions.click(commonPage.getChooseClinic(Login_Doctor.driver, logger), "Clicked on  ChooseClinic",
				Login_Doctor.driver, logger);
		Thread.sleep(10000);

		Web_GeneralFunctions.click(commonPage.getFristClinic(Login_Doctor.driver, logger), "Clicked on  1st Clinic",
				Login_Doctor.driver, logger);
		System.out.println("Clicked on 1st clicnic");
		Thread.sleep(1000);
		Web_GeneralFunctions.click(getUserInformationIcon(Login_Doctor.driver, logger),
				"Clicked on  UserInformationIcon", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(getuserProfileLink(Login_Doctor.driver, logger), "Clicked on userAdministarion",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Assert.assertTrue(getServicesOfferedTbx(Login_Doctor.driver, logger).isDisplayed(),
				"ServicesOfferedTbx is present");
		serviceOfferedByClinic1 = getServicesOfferedTbx(Login_Doctor.driver, logger).getAttribute("value");
		System.out.println("............." + serviceOfferedByClinic1);
		Thread.sleep(1000);
		Assert.assertNotSame(serviceOfferedByClinic1, serviceOfferedByClinic2,
				":) the doctor is providing same service in both the clinic..");
		 
		
		Thread.sleep(500);
	}


	@Test(priority = 2, groups = { "Regression", "User_Profile" })
	// please run the below test case with
	// Login_Doctor.LoginTestwithDiffrentUser("StgDocforstaff");

	public synchronized void userProfile_02() throws Exception {
		String serviceOfferedByClinic2;

		logger = Reports.extent.createTest("userProfile_02");
		
		Web_GeneralFunctions.click(commonPage.getlogOutDropdown(Login_Doctor.driver, logger),
				"Clicked on  getlogOutImg", Login_Doctor.driver, logger);
		Thread.sleep(1000);

		Web_GeneralFunctions.click(commonPage.getChooseClinic(Login_Doctor.driver, logger), "Clicked on  ChooseClinic",
				Login_Doctor.driver, logger);
		Thread.sleep(10000);
 		Web_GeneralFunctions.click(commonPage.getSecondClinic(Login_Doctor.driver, logger), "Clicked on  2nd Clinic",Login_Doctor.driver, logger);
		Thread.sleep(1000);
	Web_GeneralFunctions.click(getUserInformationIcon(Login_Doctor.driver, logger),"Clicked on  UserInformationIcon", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(getuserProfileLink(Login_Doctor.driver, logger),
				"Clicked on userAdministarionProfile", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Assert.assertTrue(getServicesOfferedTbx(Login_Doctor.driver, logger).isDisplayed(),
				"ServicesOfferedTbx is present");
		serviceOfferedByClinic2 = getServicesOfferedTbx(Login_Doctor.driver, logger).getAttribute("value");
		System.out.println("............." + serviceOfferedByClinic2);
		Thread.sleep(1000);

		Web_GeneralFunctions.click(getUserProfileCloseBtn(Login_Doctor.driver, logger),
				"clicked on  Close button of user profile", Login_Doctor.driver, logger);
		Thread.sleep(10000);
		Web_GeneralFunctions.click(userAdmin.getEmrMenu(Login_Doctor.driver, logger), "Clicked on  emrMenu",
				Login_Doctor.driver, logger);
		Thread.sleep(10000);
		Web_GeneralFunctions.click(userAdmin.getUserAdministrationLnk(Login_Doctor.driver, logger),
				"Clicked on userAdministarion", Login_Doctor.driver, logger);
		Thread.sleep(1000);

		Web_GeneralFunctions.click(userAdmin.getSearchUserTextBox(Login_Doctor.driver, logger),
				"clicked on  SearchUserTextBox", Login_Doctor.driver, logger);

		Web_GeneralFunctions.sendkeys(userAdmin.getSearchUserTextBox(Login_Doctor.driver, logger), "StgDocforstaff",
				"filled doctor name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getSearchUserBtn(Login_Doctor.driver, logger), "clicked on  SearchUserBtn",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(userAdmin.getEditUserBtn(Login_Doctor.driver, logger), "Clicked on getEditUserBtn",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);

		Assert.assertEquals(userAdmin.getClinicServicesTextBox(Login_Doctor.driver, logger).getAttribute("value"),
				serviceOfferedByClinic2, "Clinic service offered is not matching ");
		
		
		Thread.sleep(500);

	}


	@Test(priority = 3, groups = { "Regression", "User_Profile" })
	// please run the below test case with
	// Login_Doctor.LoginTestwithDiffrentUser("StgDocforstaff");
	public synchronized void userProfile_03() throws Exception {
		String serviceOfferedByClinic1;

		logger = Reports.extent.createTest("userProfile_03");
		Web_GeneralFunctions.click(commonPage.getlogOutDropdown(Login_Doctor.driver, logger),
				"Clicked on  getlogOutImg", Login_Doctor.driver, logger);
		Thread.sleep(1000);

		Web_GeneralFunctions.click(commonPage.getChooseClinic(Login_Doctor.driver, logger), "Clicked on  ChooseClinic",
				Login_Doctor.driver, logger);
		Thread.sleep(10000);
 		Web_GeneralFunctions.click(commonPage.getFristClinic(Login_Doctor.driver, logger), "Clicked on  1st Clinic",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(getUserInformationIcon(Login_Doctor.driver, logger),
				"Clicked on  UserInformationIcon", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(getuserProfileLink(Login_Doctor.driver, logger), "Clicked on userAdministarion",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Assert.assertTrue(getServicesOfferedTbx(Login_Doctor.driver, logger).isDisplayed(),
				"ServicesOfferedTbx is present");
		serviceOfferedByClinic1 = getServicesOfferedTbx(Login_Doctor.driver, logger).getAttribute("value");
		System.out.println("............." + serviceOfferedByClinic1);
		Thread.sleep(1000);

		Web_GeneralFunctions.click(getUserProfileCloseBtn(Login_Doctor.driver, logger),
				"clicked on  Close button of user profile", Login_Doctor.driver, logger);
		Thread.sleep(10000);
		Web_GeneralFunctions.click(userAdmin.getEmrMenu(Login_Doctor.driver, logger), "Clicked on  emrMenu",
				Login_Doctor.driver, logger);
		Thread.sleep(10000);
		Web_GeneralFunctions.click(userAdmin.getUserAdministrationLnk(Login_Doctor.driver, logger),
				"Clicked on userAdministarion", Login_Doctor.driver, logger);
		Thread.sleep(1000);

		Web_GeneralFunctions.click(userAdmin.getSearchUserTextBox(Login_Doctor.driver, logger),
				"clicked on  SearchUserTextBox", Login_Doctor.driver, logger);

		Web_GeneralFunctions.sendkeys(userAdmin.getSearchUserTextBox(Login_Doctor.driver, logger), "StgDocforstaff",
				"filled doctor name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getSearchUserBtn(Login_Doctor.driver, logger), "clicked on  SearchUserBtn",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(userAdmin.getEditUserBtn(Login_Doctor.driver, logger), "Clicked on getEditUserBtn",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);

		Assert.assertEquals(userAdmin.getClinicServicesTextBox(Login_Doctor.driver, logger).getAttribute("value"),
				serviceOfferedByClinic1, "Clinic service offered is not matching ");
		//Web_GeneralFunctions.click(commonPage.getlogOutDropdown(Login_Doctor.driver, logger), "Clicked on  getlogOutImg",Login_Doctor.driver, logger);
	//	Web_GeneralFunctions.click(commonPage.getLogOut(Login_Doctor.driver, logger), "Clicked on  getlogOutImg",Login_Doctor.driver, logger);
		
		
		Thread.sleep(500);
	}


	@Test(priority = 4, groups = { "Regression", "User_Profile" })
	// please run the below test case with
	// Login_Doctor.LoginTestwithDiffrentUser("StgDocforstaff");

	public synchronized void userProfile_04() throws Exception {
		String serviceOfferedByClinic1;
		String serviceOfferedByClinic2;

		logger = Reports.extent.createTest("userProfile_04");
		Thread.sleep(100);

		Web_GeneralFunctions.click(commonPage.getlogOutDropdown(Login_Doctor.driver, logger),
				"Clicked on  getlogOutImg", Login_Doctor.driver, logger);
		Thread.sleep(1000);

		Web_GeneralFunctions.click(commonPage.getChooseClinic(Login_Doctor.driver, logger), "Clicked on  ChooseClinic",
				Login_Doctor.driver, logger);
		Thread.sleep(10000);
		Web_GeneralFunctions.click(commonPage.getFristClinic(Login_Doctor.driver, logger), "Clicked on  1st Clinic",
				Login_Doctor.driver, logger);

		Thread.sleep(1000);
		Web_GeneralFunctions.click(getUserInformationIcon(Login_Doctor.driver, logger),
				"Clicked on  UserInformationIcon", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(getuserProfileLink(Login_Doctor.driver, logger), "Clicked on userAdministarion",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Assert.assertTrue(getServicesOfferedTbx(Login_Doctor.driver, logger).isDisplayed(),
				"ServicesOfferedTbx is present");
		serviceOfferedByClinic1 = getServicesOfferedTbx(Login_Doctor.driver, logger).getAttribute("value");
		System.out.println("............." + serviceOfferedByClinic1);
		Thread.sleep(1000);

		Web_GeneralFunctions.click(getUserProfileCloseBtn(Login_Doctor.driver, logger),
				"clicked on  Close button of user profile", Login_Doctor.driver, logger);
		Thread.sleep(10000);

		Web_GeneralFunctions.click(commonPage.getlogOutDropdown(Login_Doctor.driver, logger),
				"Clicked on  getlogOutImg", Login_Doctor.driver, logger);
		Thread.sleep(1000);

		Web_GeneralFunctions.click(commonPage.getChooseClinic(Login_Doctor.driver, logger), "Clicked on  ChooseClinic",
				Login_Doctor.driver, logger);
		Thread.sleep(10000);
		Web_GeneralFunctions.click(commonPage.getSecondClinic(Login_Doctor.driver, logger), "Clicked on  2nd Clinic",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(getUserInformationIcon(Login_Doctor.driver, logger),
				"Clicked on  UserInformationIcon", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(getuserProfileLink(Login_Doctor.driver, logger),
				"Clicked on userAdministarionProfile", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Assert.assertTrue(getServicesOfferedTbx(Login_Doctor.driver, logger).isDisplayed(),
				"ServicesOfferedTbx is present");
		serviceOfferedByClinic2 = getServicesOfferedTbx(Login_Doctor.driver, logger).getAttribute("value");
		Assert.assertNotSame(serviceOfferedByClinic1, serviceOfferedByClinic2,
				":) the doctor is providing same service in both the clinic..this is not an issue");

	}

	/*
	 * @Test(priority = 1, groups = { "Regression", "User_Profile" },enabled=false)
	 * public synchronized void APP_DOC_01() throws Exception {
	 * 
	 * EMR_CS_19, EMR_CS_20,EMR_CS_23,EMR_CS_24,EMR_CS_26,EMR_CS_27
	 * 
	 * logger = Reports.extent.createTest("EMR_CS_8"); Thread.sleep(10000);
	 * 
	 * Web_GeneralFunctions.click(getUserInformationIcon(Login_Doctor.driver,
	 * logger), "Clicked on  UserInformationIcon", Login_Doctor.driver, logger);
	 * Thread.sleep(10000);
	 * Web_GeneralFunctions.click(getuserProfileLink(Login_Doctor.driver, logger),
	 * "Clicked on userAdministarion", Login_Doctor.driver, logger);
	 * Thread.sleep(1000);
	 * Assert.assertTrue(getServicesOfferedTbx(Login_Doctor.driver,
	 * logger).isDisplayed(), "ServicesOfferedTbx is present");
	 * 
	 * getServicesOfferedTbx(Login_Doctor.driver, logger).clear();
	 * Web_GeneralFunctions.sendkeys(getServicesOfferedTbx(Login_Doctor.driver,
	 * logger), "Auto_doctor_user_profile", "filled ServicesOffered name",
	 * Login_Doctor.driver, logger);
	 * 
	 * Web_GeneralFunctions.click(getSaveBtn(Login_Doctor.driver, logger),
	 * "clicked on  Save", Login_Doctor.driver, logger); Thread.sleep(1000); String
	 * ActualServicesOffered = getServicesOfferedTbx(Login_Doctor.driver,
	 * logger).getAttribute("value");
	 * 
	 * Web_GeneralFunctions.waitForElement(Login_Doctor.driver,
	 * getUserProfileSuccessMessgage(Login_Doctor.driver, logger)); //
	 * Assert.assertEquals(getUserProfileSuccessMessgage(Login_Doctor.driver, //
	 * logger).getText(), "User details have been updated successfully", "Success //
	 * message is not matching. Please Check"); String userId =
	 * getUserIdTbx(Login_Doctor.driver, logger).getAttribute("value");
	 * Thread.sleep(1000);
	 * 
	 * Web_GeneralFunctions.click(getUserProfileCloseBtn(Login_Doctor.driver,
	 * logger), "clicked on  Close button of user profile", Login_Doctor.driver,
	 * logger); Web_GeneralFunctions.click(userAdmin.getEmrMenu(Login_Doctor.driver,
	 * logger), "Clicked on  emrMenu", Login_Doctor.driver, logger);
	 * Thread.sleep(10000);
	 * Web_GeneralFunctions.click(userAdmin.getUserAdministrationLnk(Login_Doctor.
	 * driver, logger), "Clicked on userAdministarion", Login_Doctor.driver,
	 * logger); Thread.sleep(1000);
	 * 
	 * Web_GeneralFunctions.click(userAdmin.getSearchUserIdTbx(Login_Doctor.driver,
	 * logger), "clicked on  SearchUserIDTextBox", Login_Doctor.driver, logger);
	 * System.out.println("userId  === " + userId);
	 * Web_GeneralFunctions.sendkeys(userAdmin.getSearchUserIdTbx(Login_Doctor.
	 * driver, logger), userId, "filled doctor id", Login_Doctor.driver, logger);
	 * Web_GeneralFunctions.click(userAdmin.getEditUserBtn(Login_Doctor.driver,
	 * logger), "Clicked on getEditUserBtn", Login_Doctor.driver, logger);
	 * Thread.sleep(1000); Assert.assertEquals(ActualServicesOffered,
	 * userAdmin.getClinicServicesTextBox(Login_Doctor.driver,
	 * logger).getAttribute("value"), "ActualServicesOffered not matched");
	 * 
	 * }
	 */

	// singup code
	@Test(priority = 5, groups = { "Regression", "User_Profile" },enabled=false)
	public synchronized void APP_DOC_003() throws Exception {

		logger = Reports.extent.createTest("EMR_CS_3");
		Thread.sleep(1000);
		// create user
		Thread.sleep(10000);
		System.out.println("going to click menu");

		Web_GeneralFunctions.click(userAdmin.getEmrMenu(Login_Doctor.driver, logger), "Clicked on  emrMenu",
				Login_Doctor.driver, logger);
		Thread.sleep(10000);
		Web_GeneralFunctions.click(userAdmin.getUserAdministrationLnk(Login_Doctor.driver, logger),
				"Clicked on userAdministarion", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(userAdmin.GetAddNewUserLnk(Login_Doctor.driver, logger),
				"Clicked on userAdministarion", Login_Doctor.driver, logger);
		Thread.sleep(1000);

		Web_GeneralFunctions.selectElementByVisibleText(userAdmin.getTitleDdl(Login_Doctor.driver, logger), "Ms",
				"select the title as miss", Login_Doctor.driver, logger);

		Web_GeneralFunctions.sendkeys(userAdmin.getNameTextBox(Login_Doctor.driver, logger), "Auto_doctor",
				"filled doctor name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(userAdmin.getAddressTextBox(Login_Doctor.driver, logger),
				"Emr buliding, 2nd floor, near hsr", "filled doctor name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getDobCalender(Login_Doctor.driver, logger), "clicked on DOB",
				Login_Doctor.driver, logger);

		Web_GeneralFunctions.sendkeys(userAdmin.getDobCalender(Login_Doctor.driver, logger), "11-06-2000", "filled dob",
				Login_Doctor.driver, logger);
		Web_GeneralFunctions.selectElementByVisibleText(userAdmin.getroleTypeDdl(Login_Doctor.driver, logger), "Doctor",
				"select the title as miss", Login_Doctor.driver, logger);
		String mobileNumber = Web_GeneralFunctions.getRandomNumber1(10);
		Web_GeneralFunctions.sendkeys(userAdmin.getMobileNumberTextBox(Login_Doctor.driver, logger), mobileNumber,
				"filled doctor name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.selectElementByVisibleText(userAdmin.getbloodGroupDdl(Login_Doctor.driver, logger), "A+",
				"select the title as miss", Login_Doctor.driver, logger);

		Web_GeneralFunctions.sendkeys(userAdmin.getEmailTextBox(Login_Doctor.driver, logger), "test@tatahealth.com",
				"filled doctor name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getMultiSpecialityBtn(Login_Doctor.driver, logger),
				"clicked on Spesciality", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getAndrologistCbx(Login_Doctor.driver, logger),
				"clicked on AndrologistCbx", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getAYUSHCbx(Login_Doctor.driver, logger), "clicked on AYUSHCbx",
				Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getMultiSpecialityBtn(Login_Doctor.driver, logger),
				"clicked on Spesciality", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getOperationRoleListMulBtn(Login_Doctor.driver, logger),
				"clicked on OperationRoleListMulBtn", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getSpecialityDoctorCbx(Login_Doctor.driver, logger),
				"clicked on SpecialityDoctorCbx", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getSupportAdminCbx(Login_Doctor.driver, logger),
				"clicked on SupportAdminCbx", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getCallCenterDoctorCbx(Login_Doctor.driver, logger),
				"clicked on CallCenterDoctorCbx", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getMedicalCertificateCbx(Login_Doctor.driver, logger),
				"clicked on MedicalCertificateCbx", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getLabResultEntryCbx(Login_Doctor.driver, logger),
				"clicked on LabResultEntryCbx", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getCallCentreNurseCbx(Login_Doctor.driver, logger),
				"clicked on CallCentreNurseCbx", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getBillingDeskCbx(Login_Doctor.driver, logger),
				"clicked on BillingDeskCbx", Login_Doctor.driver, logger);

		Web_GeneralFunctions.sendkeys(userAdmin.getMedicalRegNoTextBox(Login_Doctor.driver, logger), "123456789",
				"filled doctor name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(userAdmin.getClinicServicesTextBox(Login_Doctor.driver, logger), "123456789",
				"filled doctor name", Login_Doctor.driver, logger);// EMR_CS_9

		Web_GeneralFunctions.click(userAdmin.getOperationLocationListMulBtn(Login_Doctor.driver, logger),
				"clicked on OperationLocationListMulBtn", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getAutomationCbx(Login_Doctor.driver, logger), "clicked on AutomationCbx",
				Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getChatCbx(Login_Doctor.driver, logger), "clicked on ChatCbx",
				Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getVCCbx(Login_Doctor.driver, logger), "clicked on VCCbx",
				Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getSaveBtn(Login_Doctor.driver, logger), "clicked on  SaveBtn",
				Login_Doctor.driver, logger);

		// singUp page
		Web_GeneralFunctions.click(commonPage.getlogOutImg(Login_Doctor.driver, logger), "Clicked on  getlogOutImg",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(loginPage.getSignUpLink(Login_Doctor.driver, logger), "Clicked on  getSignUpLink",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(loginPage.getIAgreeCbx(Login_Doctor.driver, logger), "Clicked on  getlogOutImg",
				Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(loginPage.getProceedBtn(Login_Doctor.driver, logger), "Clicked on  getlogOutImg",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);

		Web_GeneralFunctions.sendkeys(loginPage.getSignupClinicUserIdTbx(Login_Doctor.driver, logger), "883",
				"filled   ClinicUserIdTbx", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(loginPage.getSignupOrgIdTbx(Login_Doctor.driver, logger), "84", "filled  ORGID",
				Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(loginPage.getvalidateSignUpBtn(Login_Doctor.driver, logger), "Clicked on  submit",
				Login_Doctor.driver, logger);

		// Web_GeneralFunctions.sendkeys(loginPage.getuserLoginNameTbx(Login_Doctor.driver,
		// logger), "auto", "filled userLoginName", Login_Doctor.driver, logger);
		// Web_GeneralFunctions.click(loginPage.getverifyLoginNameBtn(Login_Doctor.driver,
		// logger),"Clicked on verifyLoginNameBtn", Login_Doctor.driver, logger);
	}
}
