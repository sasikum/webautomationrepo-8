package com.tatahealth.EMR.Scripts.UserProfile;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.AppointmentDashboard.CommonsPage;
import com.tatahealth.EMR.pages.Login.LoginPage;
import com.tatahealth.EMR.pages.Login.SweetAlertPage;
import com.tatahealth.EMR.pages.UserAdministration.UserAdministration;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class AddMyQualifications extends UserProfile {

	public static WebDriver driver;

	@BeforeClass(alwaysRun = true)
	public static void beforeClass() throws Exception {
		// Login_Doctor.LoginTestwithDiffrentUser("SonalinQAEMROne");
		Login_Doctor.LoginTestwithDiffrentUser("DontBooKApptsFromThisDoc"); // staging

		System.out.println("Initialized Driver");
	}

	@AfterClass(groups = { "Regression", "AddMyQualifications" })
	public static void afterClass() throws Exception {

		Login_Doctor.LogoutTest();
		System.out.println("Closed Driver");
	}

	ExtentTest logger;
	UserAdministration userAdmin = new UserAdministration();
	LoginPage loginPage = new LoginPage();
	CommonsPage commonPage = new CommonsPage();
	SweetAlertPage swaPage = new SweetAlertPage();

	public synchronized void clickOnMyQualificationsLink() {
		try {
			Web_GeneralFunctions.click(getMyQualificationslink(Login_Doctor.driver, logger),
					"Clicked on MyQualifications link", Login_Doctor.driver, logger);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public synchronized void clickOnAddQualificationBtnInQualifications() {
		try {
			Web_GeneralFunctions.click(getAddQualificationBtn(Login_Doctor.driver, logger),
					"Clicked on save button in Qualifications ", Login_Doctor.driver, logger);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public synchronized void clickOnSaveBtnInQualifications() {
		try {
			Web_GeneralFunctions.click(getQuaSaveBtn(Login_Doctor.driver, logger),
					"Clicked on save button in Qualifications ", Login_Doctor.driver, logger);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public synchronized void clickOnEditQualifications() {
		try {
			Web_GeneralFunctions.click(getQuaEditBtn(Login_Doctor.driver, logger),
					"Clicked on edit button in Qualifications ", Login_Doctor.driver, logger);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public synchronized void clickOnDeletQualifications() {
		try {
			Web_GeneralFunctions.click(getQuaDeleteeBtn(Login_Doctor.driver, logger),
					"Clicked on edit button in Qualifications ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(1);

			Web_GeneralFunctions.click(getYesBtnOnAlt(Login_Doctor.driver, logger), "Clicked on yes button in  alrt ",
					Login_Doctor.driver, logger);

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public synchronized void fillQualifications(String nameOfInstitution, String board, String fromDate, String toDate,
			String university, String degree, String course, String specialization) {
		try {
			Web_GeneralFunctions.click(getQuaInstitutionNameTbx(Login_Doctor.driver, logger),
					"Clicked on QuaInstitutionName tbx in Qualifications ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(getQuaInstitutionNameTbx(Login_Doctor.driver, logger), nameOfInstitution,
					"filling Institutation Name", Login_Doctor.driver, logger);

			Web_GeneralFunctions.click(getBoardTbx(Login_Doctor.driver, logger),
					"Clicked on Board tbx in Qualifications ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(getBoardTbx(Login_Doctor.driver, logger), board, "filling Board Name",
					Login_Doctor.driver, logger);

			Web_GeneralFunctions.click(getQuaFromDateTbx(Login_Doctor.driver, logger),
					"Clicked on FromDate tbx in Qualifications ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(getBoardTbx(Login_Doctor.driver, logger), fromDate,
					"filling fromDate in *24-04-2020* formate", Login_Doctor.driver, logger);

			Web_GeneralFunctions.click(getQuaToDateTbx(Login_Doctor.driver, logger),
					"Clicked on ToDate tbx in Qualifications ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(getBoardTbx(Login_Doctor.driver, logger), toDate,
					"filling toDate in *24-04-2020* formate", Login_Doctor.driver, logger);

			Web_GeneralFunctions.click(getQuaUniversityTbx(Login_Doctor.driver, logger),
					"Clicked on University tbx in Qualifications ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(getQuaUniversityTbx(Login_Doctor.driver, logger), university,
					"filling university Name", Login_Doctor.driver, logger);

			Web_GeneralFunctions.click(getQuaDegreeTbx(Login_Doctor.driver, logger),
					"Clicked on Degree tbx in Qualifications ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(getQuaDegreeTbx(Login_Doctor.driver, logger), degree, "filling degree Name",
					Login_Doctor.driver, logger);

			Web_GeneralFunctions.click(getQuaCourseTbx(Login_Doctor.driver, logger),
					"Clicked on Course tbx in Qualifications ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(getQuaCourseTbx(Login_Doctor.driver, logger), course, "filling course Name",
					Login_Doctor.driver, logger);

			Web_GeneralFunctions.click(getQuaSpecializationTbx(Login_Doctor.driver, logger),
					"Clicked on Specialization tbx in Qualifications ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(getQuaSpecializationTbx(Login_Doctor.driver, logger), specialization,
					"filling specialization Name", Login_Doctor.driver, logger);

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	@Test(priority = 1001, groups = { "Regression", "AddMyQualifications" })

	public synchronized void saveQualificationsWithOutData() throws Exception {

		logger = Reports.extent.createTest("AddMyQualifications");
		Web_GeneralFunctions.wait(1);
		Web_GeneralFunctions.click(getUserInformationIcon(Login_Doctor.driver, logger),
				"Clicked on  UserInformationIcon", Login_Doctor.driver, logger);
		// Web_GeneralFunctions.waitForElement(driver,
		// getMyQualificationslink(Login_Doctor.driver, logger));
		clickOnMyQualificationsLink();
		clickOnAddQualificationBtnInQualifications();
		clickOnSaveBtnInQualifications();
		// validated error message below
	}

	@Test(priority = 1002, groups = { "Regression", "AddMyQualifications" })

	public synchronized void saveQualificationsWithData() throws Exception {

		logger = Reports.extent.createTest("saveQualificationsWithData");
		Web_GeneralFunctions.wait(1);
		Web_GeneralFunctions.click(getUserInformationIcon(Login_Doctor.driver, logger),
				"Clicked on  UserInformationIcon", Login_Doctor.driver, logger);
		// Web_GeneralFunctions.waitForElement(driver,
		// getMyQualificationslink(Login_Doctor.driver, logger));

		fillQualifications("RIT", "JIT", " ", " ", "uuuuuuuuuuufsjfj", "hsgdhhuidh", "Testing", "yrrrffffffffyyyyyy");
		clickOnSaveBtnInQualifications();
		Web_GeneralFunctions.wait(1);

	}

	@Test(priority = 1003, groups = { "Regression", "AddMyQualifications" })

	public synchronized void EditQualifications() throws Exception {

		logger = Reports.extent.createTest("EditQualifications");
		Web_GeneralFunctions.wait(1);
		clickOnEditQualifications();

		fillQualifications("RIT update", "JIT update", " ", " ", "uuuuuuuuuuufsjfj update", "hsgdhhuidh update",
				"Testing update", "yrrrffffffffyyyyyy update");
		clickOnSaveBtnInQualifications();
		Web_GeneralFunctions.wait(3);

		// Validate error message
	}

	@Test(priority = 1004, groups = { "Regression", "AddMyQualifications" })

	public synchronized void DeleteQualifications() throws Exception {

		System.out.println("...............................................................");
		logger = Reports.extent.createTest("DeleteQualifications");
		Web_GeneralFunctions.wait(1);
		clickOnDeletQualifications();

		System.out.println("...............................................................");

		// Validate error message
	}
}
