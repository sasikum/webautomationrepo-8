package com.tatahealth.EMR.Scripts.ReportsView;


import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;
import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.pages.ReportsView.ReportsView;

public class ReportsView_upload2 {
	ReportsView report = new ReportsView();
	public static ExtentTest logger;
	public static String UHID;
	Random random = new Random();
	@BeforeClass(alwaysRun = true)
	public static void beforeClass() throws Exception {
		Reports.reports();
		Login_Doctor.executionName = "reportsViewUpload";
		Login_Doctor.LoginTestwithDiffrentUser("Kasu");
	}
	@AfterClass(alwaysRun = true)
	public static void afterClass() throws Exception {
		Login_Doctor.LogoutTest();
		Reports.extent.flush();
	}
	public synchronized void reportsViewUploadLunch() throws Exception {
		logger = Reports.extent.createTest("EMR_Reports View/Upload Page");
		report.getEmrTopMenu(Login_Doctor.driver, logger).click();
		report.getEmrTopMenuElement(Login_Doctor.driver, logger, "Reports").click();
		report.getEmrTopMenu(Login_Doctor.driver, logger).click();
	}
	@Test(priority = 11, groups = { "Regression", "ReportView" })
	public void searchRegisteredUserByName() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to search patient  only with name");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		String name = "Ballu";
		report.getNameField(Login_Doctor.driver, logger).sendKeys(name);
		report.getSeachPatientBtn(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(3);
		Assert.assertTrue(report.searchedPatients(Login_Doctor.driver, logger).getText().contains(name));
	}
	@Test(priority = 12, groups = { "Regression", "ReportView" })
	public void searchUnregisteredUserByName() throws Exception {
		logger = Reports.extent.createTest("To check when user search with unregistered patient");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		String unregiseredUser = RandomStringUtils.randomAlphanumeric(8);
		report.getNameField(Login_Doctor.driver, logger).sendKeys(unregiseredUser);
		report.getSeachPatientBtn(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(3);
		String actualText = report.searchResult(Login_Doctor.driver, logger).getText();
		Assert.assertEquals(actualText, "No Result Found");
	}
	@Test(priority = 13, groups = { "Regression", "ReportView" })
	public void numberOfDigitsInNameField() throws Exception {
		logger = Reports.extent.createTest("To check how many digits user able to enter in name field ");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		String unregiseredUser = RandomStringUtils.randomAlphanumeric(1000);
		report.getNameField(Login_Doctor.driver, logger).sendKeys(unregiseredUser);

		if (report.getNameField(Login_Doctor.driver, logger).getAttribute("value").length() >= 800) {
			Assert.assertTrue(true, "Allow 800 or more characters");

		} else {
			Assert.assertTrue(false, "Allow less than 800 characters");
		}
	}

	@Test(priority = 14, groups = { "Regression", "ReportView" })
	public void dobCalenderSelection() throws Exception {
		logger = Reports.extent.createTest("To check what will happen when user click on DOB field ");
		reportsViewUploadLunch();
	}
	@Test(priority = 15, groups = { "Regression", "ReportView" })
	public void genderSelection() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to search patient with gender");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		//report.clickGenderDropdown(Login_Doctor.driver, logger).click();
		Select genderSelect = new Select(Login_Doctor.driver.findElement(By.id("gender")));
		List<WebElement> gender = genderSelect.getOptions();
		for (int j = 0; j < gender.size(); j++) {
			Assert.assertTrue(gender.get(j).getText().contains("Male") || gender.get(j).getText().contains("Female") || gender.get(j).getText().contains("Select"));
		}
	}
	@Test(priority = 16, groups = { "Regression", "ReportView" })
	public void maleSelectionGender() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to search patient with gender");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(3);
		//report.clickGenderDropdown(Login_Doctor.driver, logger).click();
		Select genderSelect = new Select(Login_Doctor.driver.findElement(By.id("gender")));
		Web_GeneralFunctions.wait(3);
		genderSelect.selectByVisibleText("Male");
		Web_GeneralFunctions.wait(3);
		Assert.assertTrue(report.searchedPatients(Login_Doctor.driver, logger).getText().contains("Male"));
	}
	@Test(priority = 17, groups = { "Regression", "ReportView" })
	public void verifyMultipleGenderSelection() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to search patient with gender");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		Web_GeneralFunctions.isVisible(report.clickGenderDropdown(Login_Doctor.driver, logger), Login_Doctor.driver);
		//report.clickGenderDropdown(Login_Doctor.driver, logger).click();
		Select genderSelect = new Select(Login_Doctor.driver.findElement(By.id("gender")));
		genderSelect.selectByVisibleText("Male");
		Web_GeneralFunctions.wait(3);
		genderSelect.selectByVisibleText("Female");
		Web_GeneralFunctions.wait(3);
		if(report.getGenderField(Login_Doctor.driver, logger).getText()=="Female") {
			Assert.assertTrue(true, "multiple not allowed");
		}
		else {
			Assert.assertTrue(true, "multiple not allowed");
		}
	}
	@Test(priority = 18, groups = { "Regression", "ReportView" })
	public void alphabetsInMobileField() throws Exception {
		logger = Reports.extent.createTest("User should not be able to enter alphabets in Mobile field ");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		String randomAplhabets = RandomStringUtils.randomAlphabetic(10);
		report.getMobileField(Login_Doctor.driver, logger).sendKeys(randomAplhabets);
		Web_GeneralFunctions.wait(3);
		if (report.getMobileField(Login_Doctor.driver, logger).getAttribute("value").isEmpty()) {
			Assert.assertTrue(true, "Mobile field cannot accept alphabets");
		} else {
			Assert.assertTrue(false, "Mobile field can accept alphabets");
		}	
	}
	@Test(priority = 19, groups = { "Regression", "ReportView" })
	public void numbersInMobileField() throws Exception {
		logger = Reports.extent.createTest("User should be able to enter numeric in Mobile field");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		String randomNumeric = RandomStringUtils.randomNumeric(10);
		report.getMobileField(Login_Doctor.driver, logger).sendKeys(randomNumeric);
		Web_GeneralFunctions.wait(3);
		//report.getEmrSeachPatientBtn(Login_Doctor.driver, logger).click();
		if (!report.getMobileField(Login_Doctor.driver, logger).getAttribute("value").isEmpty()) {
			Assert.assertTrue(true, "Mobile field accepts number");
		} else {
			Assert.assertTrue(false, "Mobile field cannot accept numeric");
		}	
	}

}
