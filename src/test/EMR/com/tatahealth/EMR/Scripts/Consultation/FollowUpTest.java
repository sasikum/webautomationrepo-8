
package com.tatahealth.EMR.Scripts.Consultation;

import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Consultation.FollowUpPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.ReusableModules.ReusableData;

public class FollowUpTest{
	
	public static Integer followUpDays = 0;
	ExtentTest logger;
	@BeforeClass(alwaysRun=true,groups= {"Regression","Appointment_Doctor"})
	public static void beforeClass() throws Exception {
		ConsultationTest currentTest = new ConsultationTest();
		currentTest.loginAndStartConsultation("Follow-up");
	}
	
	@AfterClass(groups= {"Regression","Appointment_Doctor"})
	public static void afterClass() throws Exception {
		Login_Doctor.LogoutTest();
		System.out.println("Closed Login_Doctor.driver");
		Reports.extent.flush();
	}
	
	public synchronized void saveFollowUpByRightMenu()throws Exception{
		
		logger = Reports.extent.createTest("EMR Save follow up By Right Menu");
		FollowUpPage followUp = new FollowUpPage();
		Web_GeneralFunctions.click(followUp.saveFollowUpChanges(Login_Doctor.driver, logger), "Save follow up by right menu", Login_Doctor.driver, logger);
		Thread.sleep(1000);
	}

	@Test(groups= {"Regression","Login"},priority=821)
	public synchronized void moveToFollowUp()throws Exception{
		
		logger = Reports.extent.createTest("EMR Move to follow up module");
		FollowUpPage followUp = new FollowUpPage();
		Web_GeneralFunctions.click(followUp.moveToFollowUp(Login_Doctor.driver, logger),"Move to follow up module", Login_Doctor.driver, logger);
		Thread.sleep(1000);
	}
	
	//Module has changed
	@Test(groups= {"Regression","Login"},priority=822)
	public synchronized void sendingNumbersInFollowUpField()throws Exception{
		
		logger = Reports.extent.createTest("EMR Move to follow up module");
		FollowUpPage followUp = new FollowUpPage();
		Random r = new Random();
		followUpDays = r.nextInt(10);
		Web_GeneralFunctions.clearWebElement(followUp.getFollowUpField(Login_Doctor.driver, logger), "Clear the element", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		Web_GeneralFunctions.sendkeys(followUp.getFollowUpField(Login_Doctor.driver, logger), followUpDays.toString(), "Sending follow up number in follow up field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
	}
	
	@Test(groups= {"Regression","Login"},priority=823)
	public synchronized void saveFollowUp()throws Exception{
		
		logger = Reports.extent.createTest("EMR Save Follow up");
		FollowUpPage followUp = new FollowUpPage();
		saveFollowUpByRightMenu();
		String text = Web_GeneralFunctions.getText(followUp.getSuccessMessage(Login_Doctor.driver, logger),"Get Follow up success message", Login_Doctor.driver, logger);
		//System.out.println("follow up text : "+text);
		if(text.equalsIgnoreCase("Follow-up visit has been set for the patient successfully.")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
	}
	
	@Test(groups= {"Regression","Login"},priority=824)
	public synchronized void printAllFollowUp() throws Exception{
		
		logger = Reports.extent.createTest("EMR Print All Follow up pdf");
		FollowUpPage followUp = new FollowUpPage();
		GlobalPrintTest gpt = new GlobalPrintTest();
		gpt.moveToPrintModule();
		gpt.printAll();
		String pdfContent = gpt.getPDFPage();
		System.out.println("pdf content : "+pdfContent);
		if(pdfContent.contains("Follow up")) {
			String followUpMessage = "After "+followUpDays.toString()+" Day(s)";
			System.out.println("follow up : "+followUpMessage);
			if(pdfContent.contains(followUpMessage)) {
				assertTrue(true);
			}else {
				assertTrue(false);
			}
		}else {
			assertTrue(false);
		}
		Thread.sleep(1000);
	}
	
	@Test(groups= {"Regression","Login"},priority=825)
	public synchronized void printFollowUp() throws Exception{
		
		logger = Reports.extent.createTest("EMR Print Follow up pdf");
		FollowUpPage followUp = new FollowUpPage();
		GlobalPrintTest gpt = new GlobalPrintTest();
	//	gpt.moveToPrintModule();
		gpt.print();
		String pdfContent = gpt.getPDFPage();
		System.out.println("pdf content : "+pdfContent);

		
		
		if(pdfContent.contains("Follow up")) {
			String followUpMessage = "After "+followUpDays.toString()+" Day(s)";
			if(pdfContent.contains(followUpMessage)) {
				assertTrue(true);
			}else {
				assertTrue(false);
			}
		}else {
			assertTrue(false);
		}
		Thread.sleep(1000);
		
	}
	
	@Test(groups= {"Regression","Login"},priority=826)
	public synchronized void updateFollowUp()throws Exception{
		
		logger = Reports.extent.createTest("EMR Update to follow up ");
		FollowUpPage followUp = new FollowUpPage();
		followUpDays+=1;
		moveToFollowUp();
		Web_GeneralFunctions.clearWebElement(followUp.getFollowUpField(Login_Doctor.driver, logger), "Clear the element", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.sendkeys(followUp.getFollowUpField(Login_Doctor.driver, logger), followUpDays.toString(), "Sending follow up number in follow up field", Login_Doctor.driver, logger);
		Thread.sleep(2000);
		
		Web_GeneralFunctions.selectElement(followUp.selectFollowUpOption(Login_Doctor.driver, logger), "Week(s)", "Select Weeks in follow p option", Login_Doctor.driver, logger);
		Thread.sleep(2000);
		
		saveFollowUp();
	}
	
	@Test(groups= {"Regression","Login"},priority=827)
	public synchronized void updatedChangesVerification()throws Exception{
		
		logger = Reports.extent.createTest("EMR Update follow up verification");
		FollowUpPage followUp = new FollowUpPage();
		moveToFollowUp();
		String attribute = Web_GeneralFunctions.getAttribute(followUp.getFollowUpField(Login_Doctor.driver, logger), "value", "Get saved Follow up", Login_Doctor.driver, logger);
		System.out.println("follow up attribute : "+attribute);
		if(attribute.equals(followUpDays.toString())) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Thread.sleep(1000);
	}
	
	@Test(groups= {"Regression","Login"},priority=828)
	public synchronized void updatedChangesVerificationInPDF()throws Exception{
		
		logger = Reports.extent.createTest("EMR update follow up verification in pdf");
		FollowUpPage followUp = new FollowUpPage();
		
		List<String> follow = new ArrayList<String>();
		GlobalPrintTest gpt = new GlobalPrintTest();
		gpt.moveToPrintModule();
		gpt.print();
		String pdfContent = gpt.getPDFPage();
		
		System.out.println("pdf content after follow up : "+pdfContent);
		if(pdfContent.contains("Follow up")) {
			String followUpMessage = "After "+followUpDays.toString()+" Week(s)";
			follow.add(followUpMessage);
			System.out.println("updated follow up : "+followUpMessage);
			if(pdfContent.contains(followUpMessage)) {
				assertTrue(true);
			}else {
				assertTrue(false);
			}
		}else {
			assertTrue(false);
		}
		ReusableData.savedConsultationData.put("FollowUp", (ArrayList<String>) follow);
	}
	
}
