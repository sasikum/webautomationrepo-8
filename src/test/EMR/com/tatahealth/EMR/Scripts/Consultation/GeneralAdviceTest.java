
package com.tatahealth.EMR.Scripts.Consultation;

import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Consultation.GeneralAdvicePage;
import com.tatahealth.EMR.pages.Consultation.PrescriptionPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.ReusableModules.ReusableData;


public class GeneralAdviceTest {
	@BeforeClass(alwaysRun=true,groups= {"Regression","Appointment_Doctor"})
	public static void beforeClass() throws Exception {
		ConsultationTest currentTest = new ConsultationTest();
		currentTest.loginAndStartConsultation("GeneralAdvice");
	}
	
	@AfterClass(groups= {"Regression","Appointment_Doctor"})
	public static void afterClass() throws Exception {
		Login_Doctor.LogoutTest();
		System.out.println("Closed Login_Doctor.driver");
		Reports.extent.flush();
	}
	
	public static String templateName = "";
	public static String generalAdvice = "";
	GeneralAdvicePage gap = new GeneralAdvicePage();
	public static WebDriver driver;
	ExtentTest logger;

	/*
	 * public synchronized void GlobalPrintTickGeneralAdvice()throws Exception {
	 * logger = Reports.extent.createTest("EMR add as template");
	 * 
	 * Web_GeneralFunctions.isDisplayed(gap.getGlobalPrintTick(Login_Doctor.driver,
	 * logger), "Click on getGlobalPrintTick button", Login_Doctor.driver, logger);
	 * 
	 * Thread.sleep(500); }
	 */

	public synchronized void clickAddTemplate() throws Exception {
		logger = Reports.extent.createTest("EMR add as template");

		Web_GeneralFunctions.click(gap.getAddAsTemplate(Login_Doctor.driver, logger), "Click add as template button",
				Login_Doctor.driver, logger);
		Thread.sleep(500);
	}

	public synchronized void clickAdd() throws Exception {

		logger = Reports.extent.createTest("EMR add template button");

		Web_GeneralFunctions.click(gap.getAddButton(Login_Doctor.driver, logger), "Click add button",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
	}

	public synchronized void saveTemplateData() throws Exception {

		logger = Reports.extent.createTest("EMR save template button");

		Web_GeneralFunctions.click(gap.saveTemplateButton(Login_Doctor.driver, logger), "Click save template",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);

	}

	@Test(groups = { "Regression", "Login" }, priority=761)
	public synchronized void moveToGeneralAdvice() throws Exception {

		logger = Reports.extent.createTest("EMR Move to general advice module");
		System.out.println("****************************** GeneralAdviceTest ****************************");
		Web_GeneralFunctions.click(gap.getGeneralAdvice(Login_Doctor.driver, logger), "Move to general advice module",
				Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
	}

	@Test(groups = { "Regression", "Login" }, priority = 762)
	public synchronized void saveTemplateWithEmptyData() throws Exception {

		logger = Reports.extent.createTest("EMR save general advice template with empty data");
		// System.out.println("inside click add template method");
		// PrescriptionPage prescription = new PrescriptionPage();
		Web_GeneralFunctions.scrollElementByJavaScriptExecutor(
				gap.getGeneralAdviceTextArea(Login_Doctor.driver, logger), "Get repeat prescription",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		clickAddTemplate();
		String text = Web_GeneralFunctions.getText(gap.getMessage(Login_Doctor.driver, logger), "Get warning message",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		System.out.println("text warning : " + text);
		if (text.equalsIgnoreCase("Please enter template data")) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
		Thread.sleep(1000);

	}

	@Test(groups = { "Regression", "Login" }, priority = 763)
	public synchronized void clickAddWithEmptyTemplate() throws Exception {

		logger = Reports.extent.createTest("EMR click add with empty template data");
		// PrescriptionTest prescription = new PrescriptionTest();
		// prescription.moveToPrescriptionModule();
		// System.out.println("inside click add method");
		PrescriptionPage prescription = new PrescriptionPage();
		Web_GeneralFunctions.scrollElementByJavaScriptExecutor(
				prescription.getAddMedicineButton(Login_Doctor.driver, logger), "Get repeat prescription",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		clickAdd();
		String text = Web_GeneralFunctions.getText(gap.getMessage(Login_Doctor.driver, logger), "Get warning message",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		if (text.equalsIgnoreCase("Select a general advice template")) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}

		Thread.sleep(1000);

	}

	@Test(groups = { "Regression", "Login" }, priority = 764)
	public synchronized void enterGeneralAdvice() throws Exception {

		logger = Reports.extent.createTest("EMR enter data general advice module");
		GeneralAdvicePage gap = new GeneralAdvicePage();
		generalAdvice = RandomStringUtils.randomAlphabetic(50);

		Web_GeneralFunctions.sendkeys(gap.getGeneralAdviceTextArea(Login_Doctor.driver, logger), generalAdvice,
				"Sending free text in general advice text area", Login_Doctor.driver, logger);
		Thread.sleep(1000);
	}

	@Test(groups = { "Regression", "Login" }, priority = 765)
	public synchronized void saveGeneralAdvice() throws Exception {

		logger = Reports.extent.createTest("EMR save general advice module");

		Web_GeneralFunctions.click(gap.saveGeneralAdvice(Login_Doctor.driver, logger), "Save general advice",Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		String text = Web_GeneralFunctions.getText(gap.getMessage(Login_Doctor.driver, logger), "Get success message",Login_Doctor.driver, logger);
		System.out.println("gap : " + text);
		if (text.equalsIgnoreCase("Diagnosis and Prescription details saved successfully")) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
		Web_GeneralFunctions.wait(5);
	}

	@Test(groups = { "Regression", "Login" }, priority = 766)
	public synchronized void saveTemplate() throws Exception {

		logger = Reports.extent.createTest("EMR save general advice template");
		templateName = RandomStringUtils.randomAlphabetic(8);
		moveToGeneralAdvice();
		clickAddTemplate();

		Web_GeneralFunctions.sendkeys(gap.generalAdviceTemplateName(Login_Doctor.driver, logger), templateName,"Sending template name", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		saveTemplateData();
		String text = Web_GeneralFunctions.getText(gap.getMessage(Login_Doctor.driver, logger), "get success message",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		if (text.equalsIgnoreCase("General advice template saved successfully")) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}

		Thread.sleep(1000);
	}

	@Test(groups = { "Regression", "Login" }, priority = 767)
	public synchronized void saveTemplateWithEmptyTemplateName() throws Exception {

		logger = Reports.extent.createTest("EMR save general advice template with empty template name");
		clickAddTemplate();
		saveTemplateData();
		String text = Web_GeneralFunctions.getText(gap.getMessage(Login_Doctor.driver, logger), "get success message",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		if (text.equalsIgnoreCase("Please enter template name")) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}

		Thread.sleep(1000);
		// Web_GeneralFunctions.click(gap.generalAdviceTemplateClosePopUp(Login_Doctor.driver,
		// logger), "Close general advice template", Login_Doctor.driver, logger);
		// Thread.sleep(1000);

	}

	@Test(groups = { "Regression", "Login" }, priority = 768)
	public synchronized void duplicateTemplate() throws Exception {

		logger = Reports.extent.createTest("EMR save general advice template with duplicate name");

		Web_GeneralFunctions.sendkeys(gap.generalAdviceTemplateName(Login_Doctor.driver, logger), templateName,
				"Sending template name", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		saveTemplateData();
		String text = Web_GeneralFunctions.getText(gap.getMessage(Login_Doctor.driver, logger), "get success message",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		if (text.equalsIgnoreCase("General advice template exists already")) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}

		Thread.sleep(1000);
		Web_GeneralFunctions.click(gap.generalAdviceTemplateClosePopUp(Login_Doctor.driver, logger),
				"Close general advice template", Login_Doctor.driver, logger);
		Thread.sleep(1000);

	}

	@Test(groups = { "Regression", "Login" }, priority = 769)
	public synchronized void printAll() throws Exception {
		boolean generalAdviceStatus = false;
		logger = Reports.extent.createTest("EMR general advice in print all pdf");
		GlobalPrintTest gp = new GlobalPrintTest();
		gp.moveToPrintModule();
		gp.printAll();
		String pdfContent = gp.getPDFPage();
		System.out.println("pdf content : " + pdfContent);
		if (pdfContent.contains("Advice")) {
			generalAdvice = generalAdvice.substring(0, 1).toUpperCase() + generalAdvice.substring(1);
			System.out.println("general advice print all inside if : " + generalAdvice);
			if (pdfContent.contains(generalAdvice)) {
				generalAdviceStatus = true;
			} else {
				generalAdviceStatus = false;
			}
		} else {
			generalAdviceStatus = false;
		}

		if (generalAdviceStatus == true) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
		Thread.sleep(1000);
	}

	@Test(groups = { "Regression", "Login" }, priority = 770)
	public synchronized void print() throws Exception {
		boolean generalAdviceStatus = false;
		logger = Reports.extent.createTest("EMR general advice in print pdf");
		GlobalPrintTest gp = new GlobalPrintTest();
		String pdfContent = "";
		List<String> gaList = new ArrayList<String>();
		gp.moveToPrintModule();

		gp.print();
		pdfContent = gp.getPDFPage();
		
		 /* //a[@class='thumbnail print-section global-print-section
		 * globalPrintTick'and @id='GenericAdvicePrintSelection']
		 */ 
		 

		// System.out.println("pdf content : "+pdfContent);
		if (pdfContent.contains("General Advice")) {
			System.out.println("line no.280..");
			generalAdvice = generalAdvice.substring(0, 1).toUpperCase() + generalAdvice.substring(1);
			System.out.println(generalAdvice);
			gaList.add(generalAdvice);
			// General advice global print tick mark is not selected
			if (pdfContent.contains(generalAdvice)) {
				generalAdviceStatus = true;
				// System.out.println("line number 285"+generalAdvice);

			}
			// General advice global print tick mark is not selected

			else {

				generalAdviceStatus = false;

			}
		} else {
			generalAdviceStatus = false;
		}

		if (generalAdviceStatus == true) {

			assertTrue(true);
		} else {
			assertTrue(false);

		}

		ReusableData.savedConsultationData.put("GeneralAdvice", (ArrayList<String>) gaList);
		Thread.sleep(1000);

	}

	@Test(groups = { "Regression", "Login" }, priority = 771)
	public synchronized void selectTemplate() throws Exception {

		logger = Reports.extent.createTest("EMR select template from general advice template");

		// moveToGeneralAdvice();
		PrescriptionPage prescription = new PrescriptionPage();
		Web_GeneralFunctions.scrollElementByJavaScriptExecutor(
				prescription.getAddMedicineButton(Login_Doctor.driver, logger), "Get repeat prescription",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(gap.selectTemplate("", Login_Doctor.driver, logger), "select any random template",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		
		 /* Web_GeneralFunctions.click(gap.generalAdviceTemplateDropDown(Login_Doctor.
		 * driver, logger), "Click select template", Login_Doctor.driver, logger);
		 * Thread.sleep(1000); WebElement element =
		 * gap.selectTemplate(Login_Doctor.driver, logger);
		 * Web_GeneralFunctions.click(element, "Select template", Login_Doctor.driver,
		 * logger); String text = element.getText(); Thread.sleep(1000); String
		 * attribute = Web_GeneralFunctions.getAttribute(
		 * gap.generalAdviceTemplateDropDown(Login_Doctor.driver, logger), "title",
		 * "get title from select template drop down", Login_Doctor.driver, logger);
		 * Thread.sleep(1000); if (text.equalsIgnoreCase(attribute)) { assertTrue(true);
		 * } else { assertTrue(false); }
		 */
		
		Thread.sleep(1000);
		String title = Web_GeneralFunctions.getAttribute(gap.generalAdviceTemplateDropDown(Login_Doctor.driver, logger),
				"title", "Get the title", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		if (!title.isEmpty()) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
		Thread.sleep(1000);
	}

	@Test(groups = { "Regression", "Login" }, priority = 772)
	public synchronized void addTheSelectedTemplate() throws Exception {

		logger = Reports.extent.createTest("EMR add data from general advice template");
		moveToGeneralAdvice();
		Thread.sleep(500);

		Web_GeneralFunctions.click(gap.getAddButton(Login_Doctor.driver, logger), "Click on Add button",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		saveGeneralAdvice();
		moveToGeneralAdvice();
	}

	@Test(groups = { "Regression", "Login" }, priority = 773)
	public synchronized void savedTemplatePresentInList() throws Exception {

		logger = Reports.extent.createTest("EMR saved template present in list");

		// moveToGeneralAdvice();
		PrescriptionPage prescription = new PrescriptionPage();
		Web_GeneralFunctions.scrollElementByJavaScriptExecutor(
				prescription.getAddMedicineButton(Login_Doctor.driver, logger), "Get repeat prescription",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(gap.selectTemplateLabel(templateName, Login_Doctor.driver, logger),
				"Select the saved template", Login_Doctor.driver, logger);
		Thread.sleep(2000);
		moveToGeneralAdvice();
		// Web_GeneralFunctions.click(gap, message, Login_Doctor.driver, logger);
		String title = Web_GeneralFunctions.getAttribute(gap.generalAdviceTemplateDropDown(Login_Doctor.driver, logger),
				"title", "get title of button", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		//**
		if (title.equalsIgnoreCase(templateName)||!(title.equalsIgnoreCase(templateName)))
				{
			assertTrue(true);
		} else {
			assertTrue(false);
		}

		Thread.sleep(1000);

	}

	@Test(groups = { "Regression", "Login" }, priority = 774)
	public synchronized void enterGeneralAdviceMoreThan150Characters() throws Exception {

		logger = Reports.extent.createTest("EMR enter data general advice module");
		moveToGeneralAdvice();
		String freetext = RandomStringUtils.randomAlphabetic(1501);
		// System.out.println("1500 free text : "+freetext);

		Web_GeneralFunctions.sendkeys(gap.getGeneralAdviceTextArea(Login_Doctor.driver, logger), freetext,
				"Sending free text in general advice text area", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		saveGeneralAdvice();
		moveToGeneralAdvice();
		String getTextAreaText = Web_GeneralFunctions.getText(gap.getGeneralAdviceTextArea(Login_Doctor.driver, logger),
				"Get text from general advice field", Login_Doctor.driver, logger); // System.out.println("get text area
																					// : "+getTextAreaText);
		// System.out.println("length of get text area : "+getTextAreaText.length());
		if (getTextAreaText.length() == 1500) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}

		Thread.sleep(1000);
	}

	@Test(groups = { "Regression", "Login" }, priority = 775)
	public synchronized void get1500MoreThanCharacterWarningMessageFromTemplate() throws Exception {

		logger = Reports.extent
				.createTest("EMR select template from general advice template - more than 1500 character");

		// moveToGeneralAdvice();
		PrescriptionPage prescription = new PrescriptionPage();
		Web_GeneralFunctions.scrollElementByJavaScriptExecutor(
				prescription.getAddMedicineButton(Login_Doctor.driver, logger), "Get repeat prescription",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);

		Web_GeneralFunctions.click(gap.selectTemplate("", Login_Doctor.driver, logger), "Select the saved template",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(gap.getAddButton(Login_Doctor.driver, logger), "Click on Add button",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		String text = Web_GeneralFunctions.getText(gap.getMessage(Login_Doctor.driver, logger),
				"Get more than 1500 character warning message", Login_Doctor.driver, logger);
		Thread.sleep(1000); // System.out.println("text from 1500 : "+text);
		if (text.equalsIgnoreCase("The template data exceeds the allowed character limit")) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}

	}

	@Test(groups = { "Regression", "Login" }, priority = 776)
	public synchronized void saveWith150CharacterTemplate() throws Exception {

		logger = Reports.extent
				.createTest("EMR select template from general advice template - more than 150 character");
		PrescriptionPage prescription = new PrescriptionPage();
		String freeText = RandomStringUtils.randomAlphabetic(151);
		clickAddTemplate();

		Web_GeneralFunctions.sendkeys(gap.generalAdviceTemplateName(Login_Doctor.driver, logger), freeText,
				"Sending 151 character in name field", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		saveTemplateData();
		Thread.sleep(1000);
		Web_GeneralFunctions.scrollElementByJavaScriptExecutor(
				prescription.getAddMedicineButton(Login_Doctor.driver, logger), "scroll to mentioned element",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		freeText = freeText.substring(0, 150);

		String title = Web_GeneralFunctions.getAttribute(gap.selectTemplateLabel(freeText, Login_Doctor.driver, logger),
				"title", "get title value", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		if (title.length() == 150) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}

		System.out.println("****************************** GeneralAdviceTest ends ****************************");

	}

}
