
package com.tatahealth.EMR.Scripts.Consultation;

import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Consultation.ConsultationPage;
import com.tatahealth.EMR.pages.Consultation.ReferralPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.ReusableModules.ReusableData;

public class ReferralTest {
	
	@BeforeClass(alwaysRun=true,groups= {"Regression","Appointment_Doctor"})
	public static void beforeClass() throws Exception {
		ConsultationTest currentTest = new ConsultationTest();
		currentTest.loginAndStartConsultation("Referral Test");
	}
	
	@AfterClass(groups= {"Regression","Appointment_Doctor"})
	public static void afterClass() throws Exception {
		Login_Doctor.LogoutTest();
		System.out.println("Closed Login_Doctor.driver");
		Reports.extent.flush();
	}
	
	public static String speciality = "";
	public static String specialityInputString = "Paediatrician";
	//public static String doctorInputString = "Doc Mind";
	//public static String doctorInputString = "Deiva Stg";
	//public static String doctorInputString = "Tik Tak";
	//public static String doctorInputString = "SonalinSTGEMR";
	public static String doctorInputString = "AutomationOne";
	public static String comment = "";
	public static String loggedInDoctor = "";
	
	
	ExtentTest logger;
	
	
	public synchronized void updateDoctorSpecialityList()throws Exception{
		
		logger = Reports.extent.createTest("EMR update doctor speciality in referral module");
		ReferralPage referral = new ReferralPage();
		
		Web_GeneralFunctions.click(referral.getReferTo(Login_Doctor.driver, logger),"Get refer to element", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		String text = Web_GeneralFunctions.getText(referral.getMessage(Login_Doctor.driver, logger), "Get Success Message", Login_Doctor.driver, logger);
		if(text.equalsIgnoreCase("Doctor list updated for selected specialties")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Web_GeneralFunctions.wait(1);
		
		
	}

	@Test(groups= {"Regression","Login"},priority=801)
	public synchronized void moveToReferral()throws Exception{
		
		logger = Reports.extent.createTest("EMR Move to referral module");
		ReferralPage referral = new ReferralPage();
		
		Web_GeneralFunctions.click(referral.moveToReferral(Login_Doctor.driver, logger), "Move to referral module", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		/*
		Web_GeneralFunctions.scrollElementByJavaScriptExecutor(referral.getReferralElement(Login_Doctor.driver, logger), "scroll to referral element", Login_Doctor.driver, logger);
		Thread.sleep(1000);*/
	}
	
	@Test(groups= {"Regression","Login"},priority=802)
	public synchronized void selectSpeciality()throws Exception{
		
		logger = Reports.extent.createTest("EMR speciality check in referral module");
		ReferralPage referral = new ReferralPage();
		
		Web_GeneralFunctions.click(referral.selectSpeciality(Login_Doctor.driver, logger), "click speciality", Login_Doctor.driver, logger);
		Thread.sleep(1000);
	}
	
	@Test(groups= {"Regression","Login"},priority=803)
	public synchronized void sendSpeciality()throws Exception{
		
		logger = Reports.extent.createTest("EMR sending speciality in referral module");
		ReferralPage referral = new ReferralPage();
		
		Web_GeneralFunctions.sendkeys(referral.searchSpeciality(Login_Doctor.driver, logger), specialityInputString, "sending speciality in input text box", Login_Doctor.driver, logger);
		Thread.sleep(2000);
	
	}
	
	@Test(groups= {"Regression","Login"},priority=804)
	public synchronized void selectingSpeciality()throws Exception{
		
		logger = Reports.extent.createTest("EMR sending speciality in referral module");
		ReferralPage referral = new ReferralPage();
		
		Web_GeneralFunctions.click(referral.searchAndSelectSpeciality(specialityInputString, Login_Doctor.driver, logger), "Select speciality ", Login_Doctor.driver, logger);
		Thread.sleep(2000);
		updateDoctorSpecialityList();
	
	}
	@Test(groups= {"Regression","Login"},priority=805)
	public synchronized void selectmultiSpeciality()throws Exception{
		
		logger = Reports.extent.createTest("EMR select multi speciality in referral module");
		ReferralPage referral = new ReferralPage();
		int i =0;
	//	String speciality = "";
		WebElement element;
		String text = "";
		String selectSpecialityParameter = "";
		//selectSpeciality();
		//selectingSpeciality();
		Web_GeneralFunctions.wait(1);
		Web_GeneralFunctions.click(referral.getSpecialityDropDownElement(Login_Doctor.driver, logger), "Click specilaity drop down", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(5);
		while(i<3) {
			element = referral.multispecialityFromArrayList(Login_Doctor.driver, logger);
			selectSpecialityParameter = element.getText();
			Web_GeneralFunctions.click(element, "Select speciality element", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(1);
			i++;
			text = Web_GeneralFunctions.getAttribute(referral.getSpecialityButton(Login_Doctor.driver, logger), "title","Get speciality title", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(5);
			if(text.contains(selectSpecialityParameter)) {
				speciality+=selectSpecialityParameter +",";
				assertTrue(true);
			}else {
				assertTrue(false);
			}
		}
		//System.out.println("speciality : "+speciality);
		
		updateDoctorSpecialityList();
	}
	
	@Test(groups= {"Regression","Login"},priority=806)
	public synchronized void deselectSpeciality()throws Exception{
		
		logger = Reports.extent.createTest("EMR Deselect selected speciality in referral module");
		ReferralPage referral = new ReferralPage();
		String[] special = speciality.split(",");
		int i=0;
		String text = "";
		WebElement element = null;
		while(i<special.length-1) {
			System.out.println("i : "+i+" speciality length : "+special.length);
			
			Web_GeneralFunctions.click(referral.getSpecialityDropDownElement(Login_Doctor.driver, logger), "Click specilaity drop down", Login_Doctor.driver, logger);
			Thread.sleep(1000);
			specialityInputString = special[i];
			System.out.println("speciality string : "+specialityInputString);
			
			Web_GeneralFunctions.click(referral.getDeselectSpeciality(specialityInputString, Login_Doctor.driver, logger), "Search and select speciality", Login_Doctor.driver, logger);
			Thread.sleep(1000);
			updateDoctorSpecialityList();
			text = Web_GeneralFunctions.getAttribute(referral.getSpecialityDropDownElement(Login_Doctor.driver, logger), "title","Get speciality title", Login_Doctor.driver, logger);
			Thread.sleep(1000);
			System.out.println("text : "+text);
			if(!text.contains(specialityInputString)) {
				System.out.println("true");
				assertTrue(true);
			}else {
				assertTrue(false);
			}
			i++;
		}
	}
	
	
	
	@Test(groups= {"Regression","Login"},priority=807)
	public synchronized void sendingFreeTextInSpecialitySearch()throws Exception{
		
		logger = Reports.extent.createTest("EMR sending free text in speciality field - referral module");
		ReferralPage referral = new ReferralPage();
		String freeText = RandomStringUtils.randomAlphabetic(6);
		//selectSpeciality();
		
		Web_GeneralFunctions.click(referral.getSpecialityDropDownElement(Login_Doctor.driver, logger), "Click specilaity drop down", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		
		Web_GeneralFunctions.sendkeys(referral.searchSpeciality(Login_Doctor.driver, logger), freeText, "sending speciality in input text box", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		String text = Web_GeneralFunctions.getText(referral.getNoMatch(Login_Doctor.driver, logger),"Get No macth drop down", Login_Doctor.driver, logger);
		freeText = "No results matched \""+freeText+"\"";
		System.out.println("free text in speciality : "+freeText);
		if(text.equalsIgnoreCase(freeText)) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Thread.sleep(1000);
	//	updateDoctorSpecialityList();
		Web_GeneralFunctions.clearWebElement(referral.searchSpeciality(Login_Doctor.driver, logger),"Clear the free text from speciality field", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(referral.getReferTo(Login_Doctor.driver, logger),"Get refer to element", Login_Doctor.driver, logger);
		Thread.sleep(2000);
	}
	
	
	
	
	
	
	@Test(groups= {"Regression","Login"},priority=808)
	public synchronized void selectDoctor()throws Exception{
		
		logger = Reports.extent.createTest("EMR select doctor  in referral module");
		ReferralPage referral = new ReferralPage();
		
		Web_GeneralFunctions.click(referral.getReferToSelectElement(Login_Doctor.driver, logger),"Click refer to doctor", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		
	}
	
	
	@Test(groups= {"Regression","Login"},priority=809)
	public synchronized void sendingFreeTextInDoctorSearch()throws Exception{
		
		logger = Reports.extent.createTest("EMR sending free text in doctor field - referral module");
		ReferralPage referral = new ReferralPage();
		String freeText = RandomStringUtils.randomAlphabetic(6);
		
		Web_GeneralFunctions.sendkeys(referral.sendReferDoctor(Login_Doctor.driver, logger), freeText, "sending freetext in doctor input text box", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		String text = Web_GeneralFunctions.getText(referral.getDoctorNoResult(Login_Doctor.driver, logger),"Get No macth drop down", Login_Doctor.driver, logger);
		freeText = "No results matched \""+freeText+"\"";
		System.out.println("free text in doctor :"+freeText);
		if(text.equalsIgnoreCase(freeText)) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Web_GeneralFunctions.wait(1);
		Web_GeneralFunctions.clearWebElement(referral.sendReferDoctor(Login_Doctor.driver, logger),"Clear the free text from speciality field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		Web_GeneralFunctions.click(referral.getReferTo(Login_Doctor.driver, logger), "click comment section", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
	}
	
	@Test(groups= {"Regression","Login"},priority=810)
	public synchronized void sendingDoctorInputString()throws Exception{
		
		logger = Reports.extent.createTest("EMR send doctor value in referral module");
		ReferralPage referral = new ReferralPage();
		
		selectDoctor();
		
		Web_GeneralFunctions.sendkeys(referral.sendReferDoctor(Login_Doctor.driver, logger), doctorInputString, "Sending referral doctor", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		
	}
	
	@Test(groups= {"Regression","Login"},priority=811)
	public synchronized void selectDoctorInDropDown()throws Exception{
		
		logger = Reports.extent.createTest("EMR select doctor from drop down in referral module");
		ReferralPage referral = new ReferralPage();
		ConsultationPage consultation = new ConsultationPage();
		
		Web_GeneralFunctions.click(referral.getSelectDoctor(doctorInputString, Login_Doctor.driver, logger), "click doctor", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		
		loggedInDoctor = Web_GeneralFunctions.getText(consultation.getUserProfileName(Login_Doctor.driver, logger), "get user profile nname", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
	}
	
	//@Test(groups= {"Regression","Login"},priority=91)
	public synchronized void saveReferral()throws Exception{
		
		logger = Reports.extent.createTest("EMR save referral");
		ReferralPage referral = new ReferralPage();
		
		Web_GeneralFunctions.click(referral.saveReferral(Login_Doctor.driver, logger), "Save referral", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		
	}
	
	@Test(groups= {"Regression","Login"},priority=812)
	public synchronized void saveReferralWithEmptyComment()throws Exception{
		
		logger = Reports.extent.createTest("EMR save referral with empty comment");
		ReferralPage referral = new ReferralPage();
		saveReferral();
		Web_GeneralFunctions.wait(1);
		String text = Web_GeneralFunctions.getText(referral.getMessage(Login_Doctor.driver, logger),"Get error message", Login_Doctor.driver, logger);
		if(text.equalsIgnoreCase("Please fill mandatory field before saving.")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Web_GeneralFunctions.wait(1);
	}
	
	@Test(groups= {"Regression","Login"},priority=813)
	public synchronized void sendFreeTextInCommentSection()throws Exception{
		
		logger = Reports.extent.createTest("EMR send values in comment section");
		ReferralPage referral = new ReferralPage();
		comment = RandomStringUtils.randomAlphabetic(50);
		
		Web_GeneralFunctions.sendkeys(referral.getComment(Login_Doctor.driver, logger), comment, "Sending free text in comment section", Login_Doctor.driver, logger);
		Thread.sleep(1000);
	}
	
	@Test(groups= {"Regression","Login"},priority=814)
	public synchronized void saveReferralSection()throws Exception{
		
		logger = Reports.extent.createTest("EMR Save referral with proper values");
		ReferralPage referral = new ReferralPage();
		saveReferral();
		String text = Web_GeneralFunctions.getText(referral.getMessage(Login_Doctor.driver, logger),"Get Success Message", Login_Doctor.driver, logger);
		if(text.contains("successfully.")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		
	}
	
	@Test(groups= {"Regression","Login"},priority=815)
	public synchronized void printAllPdf()throws Exception{
		
		logger = Reports.extent.createTest("EMR saved referral present in print all pdf");
		ReferralPage referral = new ReferralPage();
		GlobalPrintTest gpt = new GlobalPrintTest();
		gpt.moveToPrintModule();
		gpt.printAll();
		Web_GeneralFunctions.wait(1);
		String pdfContent = gpt.getPDFPage();
		Web_GeneralFunctions.wait(1);

		comment = comment.substring(0,1).toUpperCase()+comment.substring(1);
		List<String> referValue= new ArrayList<String>();
	System.out.println("pdf content : "+pdfContent);
		
		if(pdfContent.contains("Referral")) {
			String refer = "Dr. "+doctorInputString+" ("+comment+")";
			//System.out.println("referal : "+refer);
			referValue.add(refer);
			if(pdfContent.contains(refer)) {
				assertTrue(true);
			}else {
				assertTrue(false);
			}
			
			
		}else {
			assertTrue(false);
		}
		ReusableData.savedConsultationData.put("Referral",(ArrayList<String>) referValue);
	}
	
	@Test(groups= {"Regression","Login"},priority=816)
	public synchronized void printPdf()throws Exception{
		
		logger = Reports.extent.createTest("EMR saved referral present in print all pdf");
		ReferralPage referral = new ReferralPage();
		GlobalPrintTest gpt = new GlobalPrintTest();
		//gpt.moveToPrintModule();
		gpt.print();
		String pdfContent = gpt.getPDFPage();
		Web_GeneralFunctions.wait(1);
		//System.out.println("pdf content : "+pdfContent);
		comment = comment.substring(0,1).toUpperCase()+comment.substring(1);
		
		if(pdfContent.contains("Referral:")) {
			String refer = "Dr. "+doctorInputString+" ("+comment+")";
			System.out.println("referal : "+refer);
			if(pdfContent.contains(refer)) {
				assertTrue(true);
			}else {
				// Referral global print tick mark is not selected -hence true

				assertTrue(false);
			}
			
		}else {
			assertTrue(false);
		}
		
		
		
	}
	
	@Test(groups= {"Regression","Login"},priority=817)
	public synchronized void checkOutAndClose()throws Exception{
		
		logger = Reports.extent.createTest("EMR checkout and close");
		ReferralPage referral = new ReferralPage();
		GlobalPrintTest gpt = new GlobalPrintTest();
		gpt.moveToPrintModule();
		 
		gpt.checkout();
		Web_GeneralFunctions.wait(1);

		gpt.clickCheckOutClose();
	}
	
	//select clinic,referral header
}

