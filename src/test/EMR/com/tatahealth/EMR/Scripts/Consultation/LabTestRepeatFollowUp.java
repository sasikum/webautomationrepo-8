package com.tatahealth.EMR.Scripts.Consultation;
import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.util.List;
import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Consultation.LabPeriodicityPages;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class LabTestRepeatFollowUp {
	public static LabPeriodicityPages lab = new LabPeriodicityPages();
	public static ConsultationTest consult = new ConsultationTest();
	public static ExtentTest logger;
	public static String patientName = "Maha";
	public static Integer availableSlot = 0;
	public static String prevSlotId = "";
	public static String slotId = "";
	
	@BeforeClass(alwaysRun = true)
	public static void beforeClass() throws Exception {
		Reports.reports();
		Login_Doctor.executionName = "Lab Test Periodicity";
		Login_Doctor.LoginTestwithDiffrentUser("Kasu");
		consult.labStartConsultation("Mahamayi");
		Web_GeneralFunctions.scrollElementByJavaScriptExecutor(lab.getLabTest(Login_Doctor.driver, logger), "Scroll page till Lab test is visible on the page", 
				Login_Doctor.driver, logger);
	}
	@AfterClass(alwaysRun = true)
	public static void afterClass() throws Exception {
		Login_Doctor.driver.manage().deleteAllCookies();
		Web_GeneralFunctions.scrollDown(Login_Doctor.driver);
		Login_Doctor.LogoutTest();
		Reports.extent.flush();
	}	
	@Test(priority = 8, groups = { "Regression", "LabPeriodicity" }, description = "LTP_34")
	public void repeat_Follow_Up_lab_test_A_B_and_C() throws Exception {
		logger = Reports.extent.createTest("To verify when patient comes for appointment for the 3nd time and doctor prescribes the lab test A, B and C ");
		String number = RandomStringUtils.randomNumeric(2);
		String number2 = RandomStringUtils.randomNumeric(2);
		String number3 = RandomStringUtils.randomNumeric(2);
		JavascriptExecutor js = (JavascriptExecutor)Login_Doctor.driver;
		String labTamplateText = "EMR Automation Testing";
		lab.getlabTestTamplateSearch(Login_Doctor.driver, logger).sendKeys(labTamplateText);
		Web_GeneralFunctions.wait(2);
		js.executeScript("arguments[0].click()", lab.getSearchTestOrScan(Login_Doctor.driver, logger));
		js.executeScript("arguments[0].value='Urine Analysis';", lab.getSearchTestOrScan(Login_Doctor.driver, logger));
		lab.getNotesTestOrScan(Login_Doctor.driver, logger).sendKeys(labTamplateText);
		lab.getEveryTextfield(Login_Doctor.driver, logger).sendKeys(number);
		Select textDropdown = new Select(lab.getEveryDropdown(Login_Doctor.driver, logger));
		textDropdown.selectByIndex(2);
		lab.getForTextfield(Login_Doctor.driver, logger).sendKeys(number);
		Select forTextDropdown = new Select(lab.getForDropdown(Login_Doctor.driver, logger));
		forTextDropdown.selectByIndex(3);
		Web_GeneralFunctions.wait(3);
		lab.getPlusIcon(Login_Doctor.driver, logger).click();
		js.executeScript("arguments[0].value='Fasting Blood Sugar (FBS)';", lab.getSearchTestOrScan2(Login_Doctor.driver, logger));
		lab.getNotesTestOrScan2(Login_Doctor.driver, logger).sendKeys(labTamplateText);
		lab.getEveryTextfield2(Login_Doctor.driver, logger).sendKeys(number2);
		Select textDropdown2 = new Select(lab.getEveryDropdown2(Login_Doctor.driver, logger));
		textDropdown2.selectByIndex(2);
		Web_GeneralFunctions.wait(5);
		lab.getForTextfield2(Login_Doctor.driver, logger).sendKeys(number2);
		Select forTextDropdown2 = new Select(lab.getForDropdown2(Login_Doctor.driver, logger));
		forTextDropdown2.selectByIndex(3);
		Web_GeneralFunctions.wait(3);
		lab.getPlusIcon(Login_Doctor.driver, logger).click();
		js.executeScript("arguments[0].value='Uric Acid';", lab.getSearchTestOrScan3(Login_Doctor.driver, logger));
		lab.getNotesTestOrScan3(Login_Doctor.driver, logger).sendKeys(labTamplateText);
		lab.getEveryTextfield3(Login_Doctor.driver, logger).sendKeys(number3);
		Select textDropdown3 = new Select(lab.getEveryDropdown3(Login_Doctor.driver, logger));
		textDropdown3.selectByIndex(2);
		Web_GeneralFunctions.wait(2);
		lab.getForTextfield3(Login_Doctor.driver, logger).sendKeys(number3);
		Select forTextDropdown3 = new Select(lab.getForDropdown3(Login_Doctor.driver, logger));
		forTextDropdown3.selectByIndex(3);
		Web_GeneralFunctions.wait(2);
		Web_GeneralFunctions.scrollToElement(lab.getConsulationCheckbox(Login_Doctor.driver, logger), Login_Doctor.driver);
		js.executeScript("arguments[0].click()", lab.getConsulationCheckbox(Login_Doctor.driver, logger));
		Web_GeneralFunctions.scrollDown(Login_Doctor.driver);
		Web_GeneralFunctions.wait(3);
		String firstTest = lab.getSearchTestOrScan(Login_Doctor.driver, logger).getAttribute("value");
		String secondTest = lab.getSearchTestOrScan2(Login_Doctor.driver, logger).getAttribute("value");
		String thirdTest = lab.getSearchTestOrScan3(Login_Doctor.driver, logger).getAttribute("value");
		Assert.assertEquals(firstTest, "Urine Analysis");
		Assert.assertEquals(secondTest, "Fasting Blood Sugar (FBS)");
		Assert.assertEquals(thirdTest, "Uric Acid");	
	}
	@Test(priority = 9, groups = { "Regression", "LabPeriodicity" }, description = "LTP_35", dependsOnMethods = "repeat_Follow_Up_lab_test_A_B_and_C")
	public void repeat_Follow_Up_update_value() throws Exception {
		logger = Reports.extent.createTest("To verify doctor can able to update with the new values for all the 3 lab tests A, B and C");
		JavascriptExecutor js = (JavascriptExecutor)Login_Doctor.driver;
		js.executeScript("arguments[0].value='Fasting Blood Sugar (FBS)';", lab.getSearchTestOrScan(Login_Doctor.driver, logger));
		js.executeScript("arguments[0].value='Uric Acid';", lab.getSearchTestOrScan2(Login_Doctor.driver, logger));
		js.executeScript("arguments[0].value='Urine Analysis';", lab.getSearchTestOrScan3(Login_Doctor.driver, logger));
		Web_GeneralFunctions.scrollDown(Login_Doctor.driver);
		Web_GeneralFunctions.wait(3);
		String firstTest = lab.getSearchTestOrScan(Login_Doctor.driver, logger).getAttribute("value");
		String secondTest = lab.getSearchTestOrScan2(Login_Doctor.driver, logger).getAttribute("value");
		String thirdTest = lab.getSearchTestOrScan3(Login_Doctor.driver, logger).getAttribute("value");
		Assert.assertEquals(firstTest, "Fasting Blood Sugar (FBS)");
		Assert.assertEquals(secondTest, "Uric Acid");
		Assert.assertEquals(thirdTest, "Urine Analysis");	
	}
	@Test(priority = 10, groups = { "Regression", "LabPeriodicity" }, description = "LTP_36", dependsOnMethods = "repeat_Follow_Up_lab_test_A_B_and_C")
	public void repeat_Follow_Up_clearAll() throws Exception {
		logger = Reports.extent.createTest("To verify doctor can able to turn off all the reminders for all the 3 lab tests A, B and C");
		Web_GeneralFunctions.scrollToElement(lab.getCleanOptionBtn(Login_Doctor.driver, logger), Login_Doctor.driver);
		  List<WebElement> clearBtn=Login_Doctor.driver.findElements(By.xpath("//button[@class='text-center btn btn-danger deleteTestOrScan']")); 
		  for(WebElement btns : clearBtn) { 
			  if(lab.getSearchTestOrScan(Login_Doctor.driver, logger).getAttribute("value")==null) 
			  { 
				  btns.click(); 
				  break; 
				  } 
			  }
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.scrollDown(Login_Doctor.driver);
		Web_GeneralFunctions.scrollToElement(lab.getCleanOptionBtn(Login_Doctor.driver, logger), Login_Doctor.driver);
		lab.getSearchTestOrScan(Login_Doctor.driver, logger).clear();
		//lab.getCleanOptionBtn(Login_Doctor.driver, logger).click();
		System.out.println("=======================================================>"+lab.getSearchTestOrScan(Login_Doctor.driver, logger).getAttribute("value"));
		Assert.assertTrue(lab.getSearchTestOrScan(Login_Doctor.driver, logger).getAttribute("value").isEmpty(), "Doctor turn off reminders for all the lab tests A, B and C");
	}
}