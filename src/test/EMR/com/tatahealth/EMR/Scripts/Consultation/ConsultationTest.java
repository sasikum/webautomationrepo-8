package com.tatahealth.EMR.Scripts.Consultation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.API.libraries.SheetsAPI;
import com.tatahealth.EMR.Scripts.AppointmentDashboard.Appointment_Doctor;
import com.tatahealth.EMR.Scripts.Billing.BillingTest;
import com.tatahealth.EMR.Scripts.Billing.ConsumerAppointmentTest;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.AppointmentDashboard.AllSlotsPage;
import com.tatahealth.EMR.pages.AppointmentDashboard.AppointmentsPage;
import com.tatahealth.EMR.pages.AppointmentDashboard.PreConsultationPage;
import com.tatahealth.EMR.pages.CommonPages.CommonPage;
import com.tatahealth.EMR.pages.Consultation.ConsultationPage;
import com.tatahealth.EMR.pages.PracticeManagement.PracticeManagementPages;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.ReusableModules.DownloadPDF;
import com.tatahealth.ReusableModules.Web_Testbase;

public class ConsultationTest {
	
	public static Integer availableSlot = 0;
	public static String prevSlotId = "";
	public static String slotId = "";
	public static String UHID="";
	public static String patientName="";
	ConsultationPage consultation = new ConsultationPage();
	AllSlotsPage ASpage = new AllSlotsPage();
	/*@BeforeClass(alwaysRun=true,groups= {"Regression","Appointment_Doctor"})
	public static void beforeClass() throws Exception {
		Login_Doctor.LoginTest();
		System.out.println("Initialized Login_Doctor.driver");
	}*/
	
	/*@AfterClass(groups= {"Regression","Appointment_Doctor"})
	public static void afterClass() throws Exception {
		Login_Doctor.LogoutTest();
		System.out.println("Closed Login_Doctor.driver");
	}*/
	
	ExtentTest logger;
	
	public void loginAndStartConsultation(String TestName) throws Exception{
		Login_Doctor.executionName=TestName;
		UHID=SheetsAPI.getDataConfig(Web_Testbase.input + ".UHID");
		Login_Doctor.LoginTest();
		ConsultationTest currentTest = new ConsultationTest();
		currentTest.startConsultation();
		
	}
	
	public synchronized void startConsultation()throws Exception{
		//This is a flag to check if the given appointment is in consulting state
		boolean isConsulting = false;
		logger = Reports.extent.createTest("EMR All slots + booked + pre-consulting + preconsulted");
		ConsultationPage consultation = new ConsultationPage();
		Appointment_Doctor appt = new Appointment_Doctor();
		PracticeManagementPages pmp = new PracticeManagementPages();
		//Web_GeneralFunctions.click(pmp.getOrgHeader(Login_Doctor.driver, logger), "Move to appointment dashboard", Login_Doctor.driver, logger);
		AllSlotsPage ASpage = new AllSlotsPage();
		System.out.println("appointment home doctor");
		Web_GeneralFunctions.click(consultation.clickAllSlots(Login_Doctor.driver, logger), "Click all slots", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(10);	
		
		if(!slotId.isEmpty()){
			isConsulting = ASpage.getAppointmentStatus(slotId,Login_Doctor.driver, logger).getText().trim().equals("Consulting");
		}
		System.out.println("isConsulting------------------------------- "+isConsulting);
		if(!(slotId.isEmpty()&&prevSlotId.isEmpty())&&isConsulting){
			//Clicks on the drop-down of the appt
			getAppointmentDropdown();
			getAppointmentDropDownValue(1,"Clicking on Consulting");
			Web_GeneralFunctions.wait(3);	
		}else{
			//Getting Available Slot Code
			availableSlot = ASpage.getAvailableSlot(Login_Doctor.driver, logger);
			//availableSlot = n;
			System.out.println("available slot : "+availableSlot);
			slotId = ASpage.getSlotId(availableSlot, Login_Doctor.driver, logger);
			System.out.println("slotId : "+slotId);
			prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
			System.out.println("previous slot id : "+prevSlotId);
			//Scrolling to available slot and booking appointment
			ScrollAndStartConsultationAllSlots(prevSlotId, slotId);
			appt.ConsultPopupCheck();
			Web_GeneralFunctions.wait(3);
		}
	}

	public synchronized void openFollowUpConsultation()throws Exception{
		
		logger = Reports.extent.createTest("EMR open follow up consultation");
		//ConsultationPage consultation = new ConsultationPage();
		//Appointment_Doctor appt = new Appointment_Doctor();
		
		AppointmentsPage appoiPage = new AppointmentsPage();
		AllSlotsPage ASpage = new AllSlotsPage();
		//CommonPage CPage = new CommonPage();
		//PreConsultationPage PreconPage = new PreConsultationPage();
		loadsAppointmentDashboard();
		
		//loads all slot page
		Web_GeneralFunctions.click(appoiPage.getAllSlotsPage(Login_Doctor.driver, logger), "Moving to All slots page from Appointments Page", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(6);
		
		//open follow up page
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(availableSlot, Login_Doctor.driver, logger), "Clicking to get Appointment Dropdown", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(availableSlot,1, Login_Doctor.driver, logger), "Clicking on Reschedule in Dropdown", Login_Doctor.driver, logger);
			
	}

	public synchronized void ScrollAndStartConsultationAllSlots(String prevSlotId,String slotId) throws Exception {
		
		System.out.println("search and start consultation");
		
		AllSlotsPage ASpage = new AllSlotsPage();
		
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(ASpage.getPlusIconforAvailableSlot(slotId, Login_Doctor.driver, logger), "Clicking on Plus Icon", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(ASpage.getSearchBoxinAvailableSlot(slotId, Login_Doctor.driver, logger), "*"+UHID, "sending value in search box", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(ASpage.getFirstAvailableConsumerStartConsultation(Login_Doctor.driver, logger), "Clicking start consultation on first available consumer", Login_Doctor.driver, logger);
		//generalFunctions.sendkeys(ASpage.setReasonforVisit(Login_Doctor.driver, logger), "EMR Automation Testing", "Setting Visit Reason", Login_Doctor.driver, logger);
		//generalFunctions.click(ASpage.getBookAppointmentSubmitBtn(Login_Doctor.driver, logger), "Clicking to Submit Appointment", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(6);
		
	}
	
	public synchronized void loadsAppointmentDashboard()throws Exception{
		
		ConsultationPage consult = new ConsultationPage();
		
		Web_GeneralFunctions.click(consult.loadsAppointmentDashboard(Login_Doctor.driver, logger), "Loads appointemnt dashboard", Login_Doctor.driver, logger);
		Thread.sleep(1000);
	}
public synchronized void ScrollAndStartBookingAppointment(String prevSlotId,String slotId) throws Exception {
		
		System.out.println("search and start consultation");
		
		AllSlotsPage ASpage = new AllSlotsPage();
		
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(ASpage.getPlusIconforAvailableSlot(slotId, Login_Doctor.driver, logger), "Clicking on Plus Icon", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(ASpage.getSearchBoxinAvailableSlot(slotId, Login_Doctor.driver, logger), "*"+UHID, "sending value in search box", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(ASpage.getFirstAvailableConsumer(Login_Doctor.driver, logger), "Booking Appointment on first available consumer", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(ASpage.setReasonforVisit(Login_Doctor.driver, logger), "EMR Automation Testing", "Setting Visit Reason", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ASpage.getBookAppointmentSubmitBtn(Login_Doctor.driver, logger), "Clicking to Submit Appointment", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(8);
		
	}
public synchronized void startBookingAppointment()throws Exception{
	
	logger = Reports.extent.createTest("EMR All slots + booked + pre-consulting + preconsulted");
	ConsultationPage consultation = new ConsultationPage();
	Appointment_Doctor appt = new Appointment_Doctor();
	AllSlotsPage ASpage = new AllSlotsPage();
	Web_GeneralFunctions.click(consultation.clickAllSlots(Login_Doctor.driver, logger), "Click all slots", Login_Doctor.driver, logger);
	Web_GeneralFunctions.wait(10);
	//Getting Available Slot Code
	availableSlot = ASpage.getAvailableSlot(Login_Doctor.driver, logger);
	slotId = ASpage.getSlotId(availableSlot, Login_Doctor.driver, logger);
	prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
	//Scrolling to available slot and booking appointment
	Web_GeneralFunctions.wait(5);
	ScrollAndStartBookingAppointment(prevSlotId, slotId);
	
}

public synchronized void afterConsultClickValidation()throws Exception{
	logger = Reports.extent.createTest("EMR Post Consult Validation");
	AllSlotsPage ASpage = new AllSlotsPage();
	try
	{
	if(ASpage.getStartFollowUpButtonInConsultPage(Login_Doctor.driver, logger).isDisplayed())
	{
		ASpage.getStartFollowUpButtonInConsultPage(Login_Doctor.driver, logger).click();
	}
	}
	catch(Exception e)
	{
		System.out.println(e);
	}
	Web_GeneralFunctions.wait(4);
	}
public synchronized void bookedAppointmentOptionValidation()throws Exception{
	
	logger = Reports.extent.createTest("Booked appointment Option validation");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getListfromDropdownValue(slotId,1, Login_Doctor.driver, logger).isDisplayed();
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,1, Login_Doctor.driver, logger).getText().trim().equals("Check In"));
	ASpage.getListfromDropdownValue(slotId,2, Login_Doctor.driver, logger).isDisplayed();
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,2, Login_Doctor.driver, logger).getText().trim().equals("Check In and generate bill"));
	ASpage.getListfromDropdownValue(slotId,3, Login_Doctor.driver, logger).isDisplayed();
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,3, Login_Doctor.driver, logger).getText().trim().equals("Consult"));
	ASpage.getListfromDropdownValue(slotId,4, Login_Doctor.driver, logger).isDisplayed();
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,4, Login_Doctor.driver, logger).getText().trim().equals("Reschedule"));
	ASpage.getListfromDropdownValue(slotId,5, Login_Doctor.driver, logger).isDisplayed();
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,5, Login_Doctor.driver, logger).getText().trim().equals("Cancel"));	
}
public synchronized void bookedAppointmentOptionAfterCheckinOrCheckinGenerateBillCancelValidation()throws Exception{
	
	logger = Reports.extent.createTest("Booked appointment Option After Checkin or Checkin Generate Bill Cancel validation");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getListfromDropdownValue(slotId,1, Login_Doctor.driver, logger).isDisplayed();
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,1, Login_Doctor.driver, logger).getText().trim().equals("Pre Consultation"));
	ASpage.getListfromDropdownValue(slotId,2, Login_Doctor.driver, logger).isDisplayed();
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,2, Login_Doctor.driver, logger).getText().trim().equals("Consult"));
	ASpage.getListfromDropdownValue(slotId,3, Login_Doctor.driver, logger).isDisplayed();
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,3, Login_Doctor.driver, logger).getText().trim().equals("Generate bill"));
}
public synchronized void bookedAppointmentOptionAfterCheckinAndGenerateBillValidation()throws Exception{
	
	logger = Reports.extent.createTest("Booked appointment Option After Checkin and Checkin Generate Bill validation");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getListfromDropdownValue(slotId,1, Login_Doctor.driver, logger).isDisplayed();
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,1, Login_Doctor.driver, logger).getText().trim().equals("Pre Consultation"));
	ASpage.getListfromDropdownValue(slotId,2, Login_Doctor.driver, logger).isDisplayed();
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,2, Login_Doctor.driver, logger).getText().trim().equals("Consult"));
	ASpage.getListfromDropdownValue(slotId,3, Login_Doctor.driver, logger).isDisplayed();
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,3, Login_Doctor.driver, logger).getText().trim().equals("Cancel Bill"));
}
public synchronized void bookedAppointmentOptionPreBillAfterConsultValidation()throws Exception{
	
	logger = Reports.extent.createTest("Booked appointment Option PreBill AFter Consult validation");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getListfromDropdownValue(slotId,1, Login_Doctor.driver, logger).isDisplayed();
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,1, Login_Doctor.driver, logger).getText().trim().equals("Checked Out"));
	ASpage.getListfromDropdownValue(slotId,2, Login_Doctor.driver, logger).isDisplayed();
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,2, Login_Doctor.driver, logger).getText().trim().equals("Generate bill"));
}
public synchronized void bookedAppointmentOptionPreBillAfterCheckoutAndGenerateBillValidation()throws Exception{
	
	logger = Reports.extent.createTest("Booked appointment Option PreBill AFter Consult validation");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getListfromDropdownValue(slotId,1, Login_Doctor.driver, logger).isDisplayed();
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,1, Login_Doctor.driver, logger).getText().trim().equals("Checked Out"));
	ASpage.getListfromDropdownValue(slotId,2, Login_Doctor.driver, logger).isDisplayed();
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,2, Login_Doctor.driver, logger).getText().trim().equals("Cancel Bill"));
}
public synchronized void bookedAppointmentOptionCheckinAndGenerateBillAfterPreConsultationValidation()throws Exception{
	
	logger = Reports.extent.createTest("Booked appointment Option Checkin Generate Bill After Preconsult validation");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getListfromDropdownValue(slotId,1, Login_Doctor.driver, logger).isDisplayed();
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,1, Login_Doctor.driver, logger).getText().trim().equals("Pre Consulting"));
	ASpage.getListfromDropdownValue(slotId,2, Login_Doctor.driver, logger).isDisplayed();
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,2, Login_Doctor.driver, logger).getText().trim().equals("Cancel Bill"));
}
public synchronized void bookedAppointmentOptionCheckinAndNotGenerateBillAfterPreConsultationValidation()throws Exception{
	
	logger = Reports.extent.createTest("Booked appointment Option Checkin  and Not Generate Bill After Preconsult validation");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getListfromDropdownValue(slotId,1, Login_Doctor.driver, logger).isDisplayed();
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,1, Login_Doctor.driver, logger).getText().trim().equals("Pre Consulting"));
	ASpage.getListfromDropdownValue(slotId,2, Login_Doctor.driver, logger).isDisplayed();
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,2, Login_Doctor.driver, logger).getText().trim().equals("Generate bill"));
}

public synchronized void bookedAppointmentOptionCheckinAndGenerateBillAfterConsultValidation()throws Exception{
	
	logger = Reports.extent.createTest("Booked appointment Option Checkin Generate Bill After Preconsult validation");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getListfromDropdownValue(slotId,1, Login_Doctor.driver, logger).isDisplayed();
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,1, Login_Doctor.driver, logger).getText().trim().equals("Consulting"));
	ASpage.getListfromDropdownValue(slotId,2, Login_Doctor.driver, logger).isDisplayed();
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,2, Login_Doctor.driver, logger).getText().trim().equals("Cancel Bill"));
}
public synchronized void bookedAppointmentOptionCheckinAndNotGenerateBillAfterConsultValidation()throws Exception{
	
	logger = Reports.extent.createTest("Booked appointment Option Checkin  and Not Generate Bill After Preconsult validation");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getListfromDropdownValue(slotId,1, Login_Doctor.driver, logger).isDisplayed();
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,1, Login_Doctor.driver, logger).getText().trim().equals("Consulting"));
	ASpage.getListfromDropdownValue(slotId,2, Login_Doctor.driver, logger).isDisplayed();
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,2, Login_Doctor.driver, logger).getText().trim().equals("Generate bill"));
}

public synchronized void bookedAppointmentOptionCheckinAndNotGenerateBillAfterPreConsultingValidation()throws Exception{
	
	logger = Reports.extent.createTest("Booked appointment Option Checkin  and Not Generate Bill After Preconsulting validation");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getListfromDropdownValue(slotId,1, Login_Doctor.driver, logger).isDisplayed();
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,1, Login_Doctor.driver, logger).getText().trim().equals("Consult"));
	ASpage.getListfromDropdownValue(slotId,2, Login_Doctor.driver, logger).isDisplayed();
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,2, Login_Doctor.driver, logger).getText().trim().equals("Generate bill"));
}

public synchronized void bookedAppointmentOptionAfterConsultCancelBillValidation()throws Exception{
	
	logger = Reports.extent.createTest("Booked appointment Option PreBill AFter Consult Cancel Bill validation");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getListfromDropdownValue(slotId,1, Login_Doctor.driver, logger).isDisplayed();
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,1, Login_Doctor.driver, logger).getText().trim().equals("Checked Out"));
	ASpage.getListfromDropdownValue(slotId,2, Login_Doctor.driver, logger).isDisplayed();
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,2, Login_Doctor.driver, logger).getText().trim().equals("Generate bill"));
}
public synchronized void bookedAppointmentOptionAfterConsultGenerateBillValidation()throws Exception{
	
	logger = Reports.extent.createTest("Booked appointment Option PreBill AFter Consult Cancel Bill validation");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getListfromDropdownValue(slotId,1, Login_Doctor.driver, logger).isDisplayed();
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,1, Login_Doctor.driver, logger).getText().trim().equals("Checked Out"));
	ASpage.getListfromDropdownValue(slotId,2, Login_Doctor.driver, logger).isDisplayed();
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,2, Login_Doctor.driver, logger).getText().trim().equals("Cancel Bill"));
}
public synchronized void bookedAppointmentOptionPreBillAfterConsultShareFollowUpValidation()throws Exception{
	
	logger = Reports.extent.createTest("Booked appointment Option PreBill AFter Consult validation");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getListfromDropdownValue(slotId,1, Login_Doctor.driver, logger).isDisplayed();
	System.err.println(ASpage.getListfromDropdownValue(slotId,1, Login_Doctor.driver, logger).getText());
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,1, Login_Doctor.driver, logger).getText().trim().equals("Follow up"));
	ASpage.getListfromDropdownValue(slotId,2, Login_Doctor.driver, logger).isDisplayed();
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,2, Login_Doctor.driver, logger).getText().trim().equals("Generate bill"));
}
public synchronized void bookedAppointmentOptionPreBillGenerateAfterConsultShareFollowUpValidation()throws Exception{
	
	logger = Reports.extent.createTest("Booked appointment Option PreBill AFter Consult validation");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getListfromDropdownValue(slotId,1, Login_Doctor.driver, logger).isDisplayed();
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,1, Login_Doctor.driver, logger).getText().trim().equals("Follow up"));
	ASpage.getListfromDropdownValue(slotId,2, Login_Doctor.driver, logger).isDisplayed();
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,2, Login_Doctor.driver, logger).getText().trim().equals("Cancel Bill"));
}
public synchronized void checkOutAndBillPatientConsultPageClick()throws Exception{
	AllSlotsPage ASpage = new AllSlotsPage();
	Web_GeneralFunctions.wait(5);
	Web_GeneralFunctions.scrollToElement(ASpage.getCheckoutButtonInConsultPage(Login_Doctor.driver, logger,"Check Out and Bill Patient"), Login_Doctor.driver);
	ASpage.getCheckoutButtonInConsultPage(Login_Doctor.driver, logger,"Check Out and Bill Patient").click();
	Thread.sleep(4000);
	ASpage.getCheckoutYesButtonInConsultPage(Login_Doctor.driver, logger).click();
	try
	{
	if(ASpage.getCheckoutCloseButtonInConsultPage(Login_Doctor.driver, logger).isDisplayed())
	{
		ASpage.getCheckoutCloseButtonInConsultPage(Login_Doctor.driver, logger).click();
	}
	}
	catch(Exception e)
	{
		System.out.println(e);
	}
	Web_GeneralFunctions.wait(6);
	
}
public synchronized void getShareUpFollowConsultPageClick()throws Exception{
	AllSlotsPage ASpage = new AllSlotsPage();
	Web_GeneralFunctions.wait(4);
	Web_GeneralFunctions.scrollToElement(ASpage.getShareToFollowUpConsultPage(Login_Doctor.driver, logger), Login_Doctor.driver);
	ASpage.getShareToFollowUpConsultPage(Login_Doctor.driver, logger).click();
	Web_GeneralFunctions.wait(4);
	ASpage.getCheckoutYesButtonInConsultPage(Login_Doctor.driver, logger).click();
	Web_GeneralFunctions.wait(10);
	ArrayList tabs = new ArrayList(Login_Doctor.driver.getWindowHandles());
	if(tabs.size()>1)
	{
	 
		Web_GeneralFunctions.switchTabs("Move to new PDF tab",1,Login_Doctor.driver, logger);
		Thread.sleep(4000);
		Web_GeneralFunctions.closeDriver(Login_Doctor.driver);
		Thread.sleep(4000);
		Web_GeneralFunctions.switchTabs("Move to application tab",0, Login_Doctor.driver, logger);
		
	}
	Web_GeneralFunctions.wait(6);
	
}
public synchronized void getAppointmentDropdown()throws Exception{
	logger = Reports.extent.createTest("EMR Appointment Dropdown");
	AllSlotsPage ASpage = new AllSlotsPage();
	Web_GeneralFunctions.wait(5);
	Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Doctor.driver, logger), Login_Doctor.driver);
	Web_GeneralFunctions.wait(5);
	Web_GeneralFunctions.click(ASpage.getAppointmentDropdownButton(slotId, Login_Doctor.driver, logger), "Clicking on Dropdown of the  Appointment", Login_Doctor.driver, logger);
	
}
public synchronized void checkOutAndPrintConsultPageClick()throws Exception{
	logger = Reports.extent.createTest("EMR Pre CheckOut and Print Consult Page Validation");
	AllSlotsPage ASpage = new AllSlotsPage();
	ConsultationPage consultation = new ConsultationPage();
	Web_GeneralFunctions.wait(5);
	Web_GeneralFunctions.scrollToElement(ASpage.getCheckoutButtonInConsultPage(Login_Doctor.driver, logger,"Check Out and Print"), Login_Doctor.driver);
	ASpage.getCheckoutButtonInConsultPage(Login_Doctor.driver, logger,"Check Out and Print").click();
	Web_GeneralFunctions.wait(4);
	ASpage.getCheckoutYesButtonInConsultPage(Login_Doctor.driver, logger).click();
	Web_GeneralFunctions.wait(6);
	try
	{
	if(ASpage.getCheckoutCloseButtonInConsultPage(Login_Doctor.driver, logger).isDisplayed())
	{
		ASpage.getCheckoutCloseButtonInConsultPage(Login_Doctor.driver, logger).click();
	}
	}
	catch(Exception e)
	{
		System.out.println(e);
	}
	Web_GeneralFunctions.wait(6);
	}
public synchronized void getAppointmentDropDownValue(int x,String message)throws Exception{
	logger = Reports.extent.createTest("EMR Appointment Dropdown Value");
	AllSlotsPage ASpage = new AllSlotsPage();
	Web_GeneralFunctions.click(ASpage.getListfromDropdownValue(slotId,x, Login_Doctor.driver, logger),message,Login_Doctor.driver, logger);
	Web_GeneralFunctions.wait(4);
	
}
public synchronized void bookedAppointmentOptionPosttBillAfterConsultShareFollowUpValidation()throws Exception{
	
	logger = Reports.extent.createTest("Booked appointment Option PostBill AFter Consult validation");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getListfromDropdownValue(slotId,1, Login_Doctor.driver, logger).isDisplayed();
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,1, Login_Doctor.driver, logger).getText().trim().equals("Consulting"));
	ASpage.getListfromDropdownValue(slotId,2, Login_Doctor.driver, logger).isDisplayed();
	Assert.assertTrue(ASpage.getListfromDropdownValue(slotId,2, Login_Doctor.driver, logger).getText().trim().equals("Generate bill"));
}
public synchronized void appointmentBookedStatus(){
	logger = Reports.extent.createTest("EMR Appointment BookedStatus");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getAppointmentStatus(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentStatus(slotId,Login_Doctor.driver, logger).getText().trim().equals("Booked"));
}
public synchronized void appointmentCheckInStatus(){
	logger = Reports.extent.createTest("EMR Appointment CheckinStatus");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getAppointmentStatus(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentStatus(slotId,Login_Doctor.driver, logger).getText().trim().equals("Checked In"));
}
public synchronized void appointmentPreConsultingStatus(){
	logger = Reports.extent.createTest("EMR Appointment PreConsultionStatus");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getAppointmentStatus(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentStatus(slotId,Login_Doctor.driver, logger).getText().trim().equals("Pre Consulting"));
}
public synchronized void appointmentPreConsultedStatus(){
	logger = Reports.extent.createTest("EMR Appointment ConsultedStatus");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getAppointmentStatus(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentStatus(slotId,Login_Doctor.driver, logger).getText().trim().equals("Pre Consulted"));
}
public synchronized void appointmentConsultingStatus(){
	logger = Reports.extent.createTest("EMR Appointment ConsultingStatus");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getAppointmentStatus(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentStatus(slotId,Login_Doctor.driver, logger).getText().trim().equals("Consulting"));
}
public synchronized void appointmentFollowUpStatus(){
	logger = Reports.extent.createTest("EMR Appointment FollowUpStatus");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getAppointmentStatus(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentStatus(slotId,Login_Doctor.driver, logger).getText().trim().equals("Follow up"));
}
public synchronized void appointmentCheckedOutStatus(){
	logger = Reports.extent.createTest("EMR Appointment CheckedOutStatus");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getAppointmentStatus(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentStatus(slotId,Login_Doctor.driver, logger).getText().trim().equals("Checked Out"));
}
public synchronized void appointmentReschedule() throws Exception{
	logger = Reports.extent.createTest("EMR Appointment Reschedule");
	AllSlotsPage ASpage = new AllSlotsPage();
	int slot=Integer.parseInt(slotId)+1;
	String sloty=String.valueOf(slot);
	slotId=sloty;
	 ASpage.getRescheduleDropdown(Login_Doctor.driver, logger).click();
	 List<WebElement> timeList=ASpage.getRescheduleDropdownOptionList(Login_Doctor.driver, logger);
	 for(WebElement op:timeList)
	 {
		 if(op.getAttribute("value").trim().contains(sloty))
		 {
			 availableSlot=availableSlot+1;
			 Select select = new Select(ASpage.getRescheduleDropdown(Login_Doctor.driver, logger));
				select.selectByValue(op.getAttribute("value").trim());
				ASpage.getRescheduleDropdown(Login_Doctor.driver, logger).click();
			 ASpage.getRescheduleAppointmetOk(Login_Doctor.driver, logger).click();
			 break;
		 }
	 }
	 Web_GeneralFunctions.wait(5);
}
public synchronized void appointmentPaymentModeCashWalletStatus(){
	logger = Reports.extent.createTest("EMR Appointment CashWallets");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).getText().trim().equals("Cash, Wallet"));
}
public synchronized void appointmentPaymentModeOStatusBooked(){
	logger = Reports.extent.createTest("EMR Appointment Online");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).getText().trim().equals("Online"));
}
public synchronized void appointmentPaymentModeOStatusPaid(String paidStatus){
	logger = Reports.extent.createTest("EMR Appointment Online");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).getText().trim().equals("Online, "+paidStatus+""));
}
public synchronized void appointmentPaymentModePcStatusBooked(){
	logger = Reports.extent.createTest("EMR Appointment Pay At Clinic");
	AllSlotsPage ASpage = new AllSlotsPage();
	String paid=ConsumerAppointmentTest.Paid;
	 String payClinic=paid.substring(0,paid.lastIndexOf('/')).trim();
	ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).getText().trim().equals("Collect Rs. "+payClinic+""));
}
public synchronized void appointmentPaymentModePcStatusPaid(String paidStatus){
	logger = Reports.extent.createTest("EMR Appointment Pay At Clinic");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).getText().trim().equals(""+paidStatus+""));
}
public synchronized void appointmentPaymentModePStatusBooked(){
	logger = Reports.extent.createTest("EMR Appointment Package");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).getText().trim().equals("Package"));
}
public synchronized void appointmentPaymentModePStatusPaid(String paidStatus){
	logger = Reports.extent.createTest("EMR Appointment Package");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).getText().trim().equals("Package, "+paidStatus+""));
}
public synchronized void appointmentPaymentModeCStatusBooked(){
	logger = Reports.extent.createTest("EMR Appointment Coupon");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).getText().trim().equals("Coupon"));
}
public synchronized void appointmentPaymentModeCStatusPaid(String paidStatus){
	logger = Reports.extent.createTest("EMR Appointment Coupon");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).getText().trim().equals("Coupon, "+paidStatus+""));
}
public synchronized void appointmentPaymentModeFStatusBooked(){
	logger = Reports.extent.createTest("EMR Appointment Rewards or Fitcoins");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).getText().trim().equals("Rewards"));
}
public synchronized void appointmentPaymentModeFStatusPaid(String paidStatus){
	logger = Reports.extent.createTest("EMR Appointment Rewards or Fitcoins");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).getText().trim().equals("Rewards, "+paidStatus+""));
}
public synchronized void appointmentPaymentModeCOStatusBooked(){
	logger = Reports.extent.createTest("EMR Appointment Coupon +Online");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).getText().trim().equals("Coupon, Online"));
}
public synchronized void appointmentPaymentModeCOStatusPaid(String paidStatus){
	logger = Reports.extent.createTest("EMR Appointment Coupon +Online");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).getText().trim().equals("Coupon, Online, "+paidStatus+""));
}
public synchronized void appointmentPaymentModePOStatusBooked(){
	logger = Reports.extent.createTest("EMR Appointment Package +Online");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).getText().trim().equals("Package, Online"));
}
public synchronized void appointmentPaymentModePOStatusPaid(String paidStatus){
	logger = Reports.extent.createTest("EMR Appointment Package +Online");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).getText().trim().equals("Package, Online, "+paidStatus+""));
}
public synchronized void appointmentPaymentModeCPcStatusBooked(){
	logger = Reports.extent.createTest("EMR Appointment Coupon+Pay At Clinic");
	AllSlotsPage ASpage = new AllSlotsPage();
	String paid=ConsumerAppointmentTest.Paid;
	 String payClinic=paid.substring(paid.indexOf('+')+1,paid.lastIndexOf('/')).trim();
	ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).getText().trim().equals("Coupon, Collect Rs. "+payClinic+""));
}
public synchronized void appointmentPaymentModeCPcStatusPaid(String paidStatus){
	logger = Reports.extent.createTest("EMR Appointment Coupon+Pay At Clinic");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).getText().trim().equals("Coupon, "+paidStatus+""));
}
public synchronized void appointmentPaymentModePPcStatusBooked(){
	logger = Reports.extent.createTest("EMR Appointment Package+Pay At Clinic");
	AllSlotsPage ASpage = new AllSlotsPage();
	String paid=ConsumerAppointmentTest.Paid;
	 String payClinic=paid.substring(paid.indexOf('+')+1,paid.lastIndexOf('/')).trim();
	ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).getText().trim().equals("Package, Collect Rs. "+payClinic+""));
}
public synchronized void appointmentPaymentModePPcStatusPaid(String paidStatus){
	logger = Reports.extent.createTest("EMR Appointment Package+Pay At Clinic");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).getText().trim().equals("Package, "+paidStatus+""));
}
public synchronized void appointmentPaymentModeCFStatusBooked(){
	logger = Reports.extent.createTest("EMR Appointment Coupon+ Fitcoins or Rewards");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).getText().trim().equals("Coupon, Rewards"));
}
public synchronized void appointmentPaymentModeCFStatusPaid(String paidStatus){
	logger = Reports.extent.createTest("EMR Appointment Coupon+ Fitcoins or Rewards");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).getText().trim().equals("Coupon, Rewards, "+paidStatus+""));
}
public synchronized void appointmentPaymentModeCFOStatusBooked(){
	logger = Reports.extent.createTest("EMR Appointment Coupon+ Fitcoins or Rewards+Online");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).getText().trim().equals("Coupon, Rewards, Online"));
}
public synchronized void appointmentPaymentModeCFOStatusPaid(String paidStatus){
	logger = Reports.extent.createTest("EMR Appointment Coupon+ Fitcoins or Rewards+Online");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).getText().trim().equals("Coupon, Rewards, Online, "+paidStatus+""));
}
public synchronized void appointmentPaymentModeFPcStatusBooked(){
	logger = Reports.extent.createTest("EMR Appointment Fitcoins or Rewards+Pay At Clinic");
	AllSlotsPage ASpage = new AllSlotsPage();
	String paid=ConsumerAppointmentTest.Paid;
	 String payClinic=paid.substring(paid.indexOf('+')+1,paid.lastIndexOf('/')).trim();
	ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).getText().trim().equals("Rewards, Collect Rs. "+payClinic+""));
}
public synchronized void appointmentPaymentModeFPcStatusPaid(String paidStatus){
	logger = Reports.extent.createTest("EMR Appointment Fitcoins or Rewards+Pay At Clinic");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).getText().trim().equals("Rewards, "+paidStatus+""));
}
public synchronized void appointmentPaymentModeFOStatusBooked(){
	logger = Reports.extent.createTest("EMR Appointment Fitcoins or Rewards+Online");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).getText().trim().equals("Rewards, Online"));
}
public synchronized void appointmentPaymentModeFOStatusPaid(String paidStatus){
	logger = Reports.extent.createTest("EMR Appointment Fitcoins or Rewards+Online");
	AllSlotsPage ASpage = new AllSlotsPage();
	ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).getText().trim().equals("Rewards, Online, "+paidStatus+""));
}
public synchronized void appointmentPaymentModeCFPcStatusBooked(){
	logger = Reports.extent.createTest("EMR Appointment  Coupon+Fitcoins or Rewards+Pay at Clinic");
	AllSlotsPage ASpage = new AllSlotsPage();
	String paid=ConsumerAppointmentTest.Paid;
	String payClinic=paid.substring(paid.lastIndexOf('+')+1,paid.lastIndexOf('/')).trim();
	ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).getText().trim().equals("Coupon, Rewards, Collect Rs. "+payClinic+""));
}
public synchronized void appointmentPaymentModeCFPcStatusPaid(String paidStatus){
	logger = Reports.extent.createTest("EMR Appointment Coupon+Fitcoins or Rewards+Pay at Clinic");
	ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).isDisplayed();
    Assert.assertTrue(ASpage.getAppointmentPaymentMode(slotId,Login_Doctor.driver, logger).getText().trim().equals("Coupon, Rewards, "+paidStatus+""));
}
public synchronized void labStartConsultation(String patient)throws Exception{
	//This is a flag to check if the given appointment is in consulting state
	boolean isConsulting = false;
	logger = Reports.extent.createTest("EMR All slots + booked + pre-consulting + preconsulted");
	ConsultationPage consultation = new ConsultationPage();
	Appointment_Doctor appt = new Appointment_Doctor();
	PracticeManagementPages pmp = new PracticeManagementPages();
	//Web_GeneralFunctions.click(pmp.getOrgHeader(Login_Doctor.driver, logger), "Move to appointment dashboard", Login_Doctor.driver, logger);
	AllSlotsPage ASpage = new AllSlotsPage();
	System.out.println("appointment home doctor");
	Web_GeneralFunctions.click(consultation.clickAllSlots(Login_Doctor.driver, logger), "Click all slots", Login_Doctor.driver, logger);
	Web_GeneralFunctions.wait(10);	
	
	if(!slotId.isEmpty()){
		isConsulting = ASpage.getAppointmentStatus(slotId,Login_Doctor.driver, logger).getText().trim().equals("Consulting");
	}
	System.out.println("isConsulting------------------------------- "+isConsulting);
	if(!(slotId.isEmpty()&&prevSlotId.isEmpty())&&isConsulting){
		//Clicks on the drop-down of the appt
		getAppointmentDropdown();
		getAppointmentDropDownValue(1,"Clicking on Consulting");
		Web_GeneralFunctions.wait(3);	
	}else{
		//Getting Available Slot Code
		availableSlot = ASpage.getAvailableSlot(Login_Doctor.driver, logger);
		//availableSlot = n;
		System.out.println("available slot : "+availableSlot);
		slotId = ASpage.getSlotId(availableSlot, Login_Doctor.driver, logger);
		System.out.println("slotId : "+slotId);
		prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
		System.out.println("previous slot id : "+prevSlotId);
		//Scrolling to available slot and booking appointment
		ScrollAndStartConsultationA(prevSlotId, slotId, patient);
		appt.ConsultPopupCheck();
		Web_GeneralFunctions.wait(3);
	}
}
public synchronized void ScrollAndStartConsultationA(String prevSlotId,String slotId, String patient) throws Exception {
	
	System.out.println("search and start consultation");
	
	AllSlotsPage ASpage = new AllSlotsPage();
	
	Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Doctor.driver, logger), Login_Doctor.driver);
	Web_GeneralFunctions.wait(3);
	Web_GeneralFunctions.click(ASpage.getPlusIconforAvailableSlot(slotId, Login_Doctor.driver, logger), "Clicking on Plus Icon", Login_Doctor.driver, logger);
	Web_GeneralFunctions.sendkeys(ASpage.getSearchBoxinAvailableSlot(slotId, Login_Doctor.driver, logger), patient, "sending value in search box", Login_Doctor.driver, logger);
	Web_GeneralFunctions.wait(3);
	Web_GeneralFunctions.click(ASpage.getFirstAvailableConsumerStartConsultation(Login_Doctor.driver, logger), "Clicking start consultation on first available consumer", Login_Doctor.driver, logger);
	Web_GeneralFunctions.wait(6);
	
}

}
