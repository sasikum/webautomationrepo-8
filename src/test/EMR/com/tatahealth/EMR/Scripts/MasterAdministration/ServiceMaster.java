package com.tatahealth.EMR.Scripts.MasterAdministration;


import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.MasterAdministration.ServiceMasterPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class ServiceMaster {

	
	public static ExtentTest logger;
	
	ServiceMasterPage SMP = new ServiceMasterPage();
	
	public String getrandomservicename()
	{
		Random rnum = new Random();
		return "Cervical"+rnum.nextInt(20000); 
		
	}

	@BeforeClass(alwaysRun = true, groups= {"Regression","MasterAdministration"})
	@Parameters("role")
	public static void beforeClass(String role) throws Exception {
		Login_Doctor.executionName="Service Master";		
		Login_Doctor.LoginTestwithDiffrentUser(role);
		}
	
	@AfterClass(alwaysRun = true, groups = { "Regression", "MasterAdministration" })
	public static void afterClass() throws Exception {

		Login_Doctor.LogoutTest();
		Reports.extent.flush();
		
	}
	
	@Parameters ({"doctor1"})
	@Test(priority = 1,groups= {"Regression","Service_Close_Test"})
	public synchronized void Service_Close_Test(String doctor) throws Exception {

		logger = Reports.extent.createTest("Service Close Test");
		Web_GeneralFunctions.click(SMP.getleftmainmenu(Login_Doctor.driver, logger), "Clicking on Humburger menu", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(SMP.getsidelink(Login_Doctor.driver, logger), "Click on Master Admin Tab", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(SMP.getServiceMasterTab(Login_Doctor.driver,logger), "Clicking on Service Master Tab", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(SMP.getAddServicelink(Login_Doctor.driver, logger), "Clicking on Add service link", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(SMP.getServiceClosebtn(Login_Doctor.driver, logger), "Click on Close btn", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		
	    }
	
	@Parameters ({"doctor1"})
	@Test(priority = 2,groups= {"Regression","Service_List_Test1"})
	public synchronized void Service_List_Test1(String doctor) throws Exception {

		logger = Reports.extent.createTest("Service List Test");
	
		Assert.assertTrue(SMP.getServiceList_filter_service(Login_Doctor.driver, logger).isDisplayed(), "Service feild displayed");
		Assert.assertTrue(SMP.getServiceList_filter_cat(Login_Doctor.driver, logger).isDisplayed(), "Category feild is displayed");
		Assert.assertTrue(SMP.getServiceList_filter_class(Login_Doctor.driver, logger).isDisplayed(), "Classification feild is displayed");
		Assert.assertTrue(SMP.getServiceList_filter_tariff(Login_Doctor.driver, logger).isDisplayed(), "Tarrif feild is displayed");
		
		Web_GeneralFunctions.wait(3);
								    
	    }
	
	@Parameters ({"doctor1"})
	@Test(priority = 3,groups= {"Regression","Service_List_Test2"})
	public synchronized void Service_List_Test2(String doctor) throws Exception {

		logger = Reports.extent.createTest("Service List2 Test");
	
		Web_GeneralFunctions.click(SMP.getServiceList_Edit(Login_Doctor.driver, logger), "Click on Edit link", Login_Doctor.driver, logger);
		Assert.assertTrue(SMP.getServiceList_UpdateServicePoPup(Login_Doctor.driver, logger).isDisplayed(), "Update Service Pop up displayed");
		
		Web_GeneralFunctions.wait(3);
								    
	    }
	
	@Parameters ({"doctor1"})
	@Test(priority = 4,groups= {"Regression","Service_List_Test3"})
	public synchronized void Service_List_Test3(String doctor) throws Exception {

		logger = Reports.extent.createTest("Service List3 Test");
	    Web_GeneralFunctions.click(SMP.getServiceList_popupClosebtn(Login_Doctor.driver, logger), "Click on close button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(SMP.getServiceList_Edit(Login_Doctor.driver, logger), "Click on Edit link", Login_Doctor.driver, logger);
		Assert.assertTrue(SMP.getServiceTypeSelBox(Login_Doctor.driver, logger).isDisplayed(), "Service Type is displayed");
		Web_GeneralFunctions.isClickable(SMP.getServiceTypeSelBox(Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.click(SMP.getServiceList_PoPupSavebtn(Login_Doctor.driver, logger), "Click on Save btn", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
	    }
	
	@Parameters ({"doctor1"})
	@Test(priority = 5,groups= {"Regression","Service_List_Test3"})
	public synchronized void Service_List_Test4(String doctor) throws Exception {

		logger = Reports.extent.createTest("Service List4 Test");
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(SMP.getServiceList_filter_service(Login_Doctor.driver, logger), "Click on filter", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(SMP.getServiceList_filter_cat(Login_Doctor.driver, logger), "Click on Category Filter", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		
										    
	    }
	
	@Parameters ({"doctor1"})
	@Test(priority = 6,groups= {"Regression","Service_List_Test4"})
	public synchronized void Service_List_Test5(String doctor) throws Exception {

		logger = Reports.extent.createTest("Service List5 Test");
	
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.selectElementByVisibleText(SMP.getServiceList_dropdown(Login_Doctor.driver, logger), "10", "Select the entries", Login_Doctor.driver, logger);
		Assert.assertTrue(SMP.getServiceList_filter_service(Login_Doctor.driver, logger).isDisplayed(), "service list is dispalyed");
		Assert.assertTrue(SMP.getServiceList_nextbtn(Login_Doctor.driver, logger).isDisplayed(), "Next btn displayed");
		Assert.assertTrue(SMP.getServiceList_previousbtn(Login_Doctor.driver, logger).isDisplayed(), "Previous btn displayed");
		Web_GeneralFunctions.click(SMP.getServiceList_nextbtn(Login_Doctor.driver, logger), "Click on next btn", Login_Doctor.driver, logger);
		Assert.assertTrue(SMP.getServiceList_filter_service(Login_Doctor.driver, logger).isDisplayed(), "service list is dispalyed");
	    }
	
	@Parameters ({"doctor1"})
	@Test(priority = 8,groups= {"Regression","Service_List_Test4"})
	public synchronized void Service_List_Test6(String doctor) throws Exception {

		logger = Reports.extent.createTest("Service List6 Test");
	    String ServiceName1="Cervical";
	    String ServiceName2="dgfdhgd";
	    String ServiceName3="123#$abc";
		Web_GeneralFunctions.wait(3);
		
		
		Web_GeneralFunctions.sendkeys(SMP.getServiceList_Search(Login_Doctor.driver, logger), ServiceName1, "search service", Login_Doctor.driver, logger);
		Assert.assertTrue(SMP.getService_searchresult(Login_Doctor.driver, logger).isDisplayed(), "Search Result found");
		Web_GeneralFunctions.clear(SMP.getServiceList_Search(Login_Doctor.driver, logger), "Clear search", Login_Doctor.driver, logger);	
		Web_GeneralFunctions.sendkeys(SMP.getServiceList_Search(Login_Doctor.driver, logger), ServiceName2, "search service", Login_Doctor.driver, logger);
		Assert.assertTrue(SMP.getService_searchresult1(Login_Doctor.driver, logger).isDisplayed(), "Search Result not found");
		Web_GeneralFunctions.clear(SMP.getServiceList_Search(Login_Doctor.driver, logger), "Clear search", Login_Doctor.driver, logger);	
		Web_GeneralFunctions.sendkeys(SMP.getServiceList_Search(Login_Doctor.driver, logger), ServiceName3, "search service", Login_Doctor.driver, logger);
		Assert.assertTrue(SMP.getService_searchresult1(Login_Doctor.driver, logger).isDisplayed(), "Search Result not found");
		Web_GeneralFunctions.clear(SMP.getServiceList_Search(Login_Doctor.driver, logger), "Clear search", Login_Doctor.driver, logger);
	}
	
	@Parameters ({"doctor1"})
	@Test(priority = 7,groups= {"Regression","Service_List_Test7"})
	public synchronized void Service_List_Test7(String doctor) throws Exception {

		logger = Reports.extent.createTest("Service List6 Test");
	    String servicename="Cervical spine (AP / Leteral) Each";
	    
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(SMP.getService_ServiceName_edit(Login_Doctor.driver, logger, servicename), "Click on edit", Login_Doctor.driver, logger);
		Web_GeneralFunctions.selectElementByVisibleText(SMP.getServiceStatusSbx(Login_Doctor.driver, logger), "Inactive", "Service as Inactive", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(SMP.getServiceList_PoPupSavebtn(Login_Doctor.driver, logger), "Click on Save btn", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);

		Web_GeneralFunctions.click(SMP.getbilling(Login_Doctor.driver, logger), "Click on billing tab", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(SMP.getbilling_servicebox(Login_Doctor.driver, logger), servicename, "enter service name", Login_Doctor.driver, logger);
        Assert.assertTrue(SMP.getbilling_servicebox_error(Login_Doctor.driver, logger).isDisplayed(), "No Service found");	
	    Web_GeneralFunctions.click(SMP.getsidelink(Login_Doctor.driver, logger), "Click on MasterAdmin", Login_Doctor.driver, logger);
	    Web_GeneralFunctions.click(SMP.getServiceMasterTab(Login_Doctor.driver,logger), "Clicking on Service Master Tab", Login_Doctor.driver, logger);

	
	}
	
	
	
	@Parameters ({"doctor1"})
	@Test(priority = 9,groups= {"Regression","ServiceMasterTest"})
	public synchronized void ServiceMasterTest(String doctor) throws Exception {

		logger = Reports.extent.createTest("Service Master Test");
	
				
		Web_GeneralFunctions.click(SMP.getAddServicelink(Login_Doctor.driver, logger), "Clicking on Add service link", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.selectElementByVisibleText(SMP.getServiceCategorySelBox(Login_Doctor.driver, logger), "Medical", "Select the value from options", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.selectElementByVisibleText(SMP.getServiceClassifSelBox(Login_Doctor.driver, logger), "Procedure", "Select the option", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(SMP.getServiceSaveBtn(Login_Doctor.driver, logger), "Click on Save btn", Login_Doctor.driver, logger);
		Assert.assertTrue(SMP.getService_mandatorycheck(Login_Doctor.driver, logger).isDisplayed(), "Manatory check");
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.sendkeys(SMP.getServiceNametxtBox(Login_Doctor.driver, logger), "abcd&(jkl)", "Enter Service Name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.clear(SMP.getServiceNametxtBox(Login_Doctor.driver, logger), "Clear service name text box", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(SMP.getServiceNametxtBox(Login_Doctor.driver, logger), "asdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdf", "Enter Service Name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.clear(SMP.getServiceNametxtBox(Login_Doctor.driver, logger), "Clear service name text box", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(SMP.getServiceNametxtBox(Login_Doctor.driver, logger), "Cervical", "Enter Service Name", Login_Doctor.driver, logger);
		
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.selectElementByVisibleText(SMP.getServiceTypeSelBox(Login_Doctor.driver, logger), "Test", "Selecting Service type ", Login_Doctor.driver, logger);
	    Web_GeneralFunctions.wait(2);
	    Web_GeneralFunctions.selectElementByVisibleText(SMP.getService_ChargableSbx(Login_Doctor.driver, logger), "Yes", "Selecting the chargable", Login_Doctor.driver, logger);
	    Web_GeneralFunctions.wait(2);
	    Web_GeneralFunctions.selectElementByVisibleText(SMP.getServiceResultApplSbx(Login_Doctor.driver, logger), "Yes", "Selecting result aplicable", Login_Doctor.driver, logger);
	    Web_GeneralFunctions.wait(2);
	    Web_GeneralFunctions.selectElementByVisibleText(SMP.getServiceStatusSbx(Login_Doctor.driver, logger), "Active", "Selecting the status", Login_Doctor.driver, logger);
	    Web_GeneralFunctions.wait(2);
	    Web_GeneralFunctions.clear(SMP.getServiceTariffAmttxtbx(Login_Doctor.driver, logger),"Clear Tariff Amount", Login_Doctor.driver, logger);
	    Web_GeneralFunctions.sendkeys(SMP.getServiceTariffAmttxtbx(Login_Doctor.driver, logger), "10.00", "Enter Tariff Amount", Login_Doctor.driver, logger);
	    Web_GeneralFunctions.wait(3);
	    Web_GeneralFunctions.scrollDown(Login_Doctor.driver);
	    
	    }


	@Parameters ({"doctor1"})
	@Test(priority = 10,groups= {"Regression","Service_Doctor1_Test"})
	public synchronized void Service_Doctor1_Test(String doctor) throws Exception {

		logger = Reports.extent.createTest("Service Doctor1 Test");
	
		Web_GeneralFunctions.click(SMP.getServiceDoctorMultiSelect(Login_Doctor.driver, logger), "click on doctor multi sel dropdown", Login_Doctor.driver, logger);
	    Web_GeneralFunctions.sendkeys(SMP.getServicedoctorSearchbx(Login_Doctor.driver, logger), "S", "Enter doctor name", Login_Doctor.driver, logger);
	    Web_GeneralFunctions.sendkeys(SMP.getServicedoctorSearchbx(Login_Doctor.driver, logger), "a", "Enter doctor name", Login_Doctor.driver, logger);
	    Web_GeneralFunctions.sendkeys(SMP.getServicedoctorSearchbx(Login_Doctor.driver, logger), "m", "Enter doctor name", Login_Doctor.driver, logger);
	    Assert.assertTrue(SMP.getServiceNoResulttxt(Login_Doctor.driver, logger).isDisplayed(), "Checking for the Error message");
	    Web_GeneralFunctions.wait(3);
		}
	
	@Parameters ({"doctor1"})
	@Test(priority = 11,groups= {"Regression","Service_Doctor2_Test"})
	public synchronized void Service_Doctor2_Test(String doctor) throws Exception {

		logger = Reports.extent.createTest("Service Doctor2 Test");
	    Web_GeneralFunctions.click(SMP.getServiceTariffAmttxtbx(Login_Doctor.driver, logger), "Dummy Click", Login_Doctor.driver, logger);
		Web_GeneralFunctions.scrollDown(Login_Doctor.driver);
		Web_GeneralFunctions.click(SMP.getServiceDoctorMultiSelect(Login_Doctor.driver, logger), "click on doctor multi sel dropdown", Login_Doctor.driver, logger);
	   
		try
		{
			Web_GeneralFunctions.click(SMP.getServiceSelectAllbtn(Login_Doctor.driver, logger), "Click on Select All ", Login_Doctor.driver, logger);
		}
		catch (Exception e)
		{
        Assert.assertFalse(false,"Select All is not worked");	
		}
		
		Assert.assertTrue(true, "Select All is working");
		Web_GeneralFunctions.wait(3);
	    }
	
	@Parameters ({"doctor1"})
	@Test(priority = 12,groups= {"Regression","Service_Doctor3_Test"})
	public synchronized void Service_Doctor3_Test(String doctor) throws Exception {

		logger = Reports.extent.createTest("Service Doctor3 Test");
	
		try
		{
			Web_GeneralFunctions.click(SMP.getServiceDeSelectAllbtn(Login_Doctor.driver, logger), "Click on DeSelect All ", Login_Doctor.driver, logger);
		}
		
		catch (Exception e)
		{
        Assert.assertFalse(false,"DeSelect All is not worked");	
		}
		
		Assert.assertTrue(true, "DeSelect All is working");
		Web_GeneralFunctions.wait(3);
	    }

	
	
	@Parameters ({"doctor1"})
	@Test(priority = 13,groups= {"Regression","Service_Doctor4_Test"})
	public synchronized void Service_Doctor4_Test(String doctor) throws Exception {

		logger = Reports.extent.createTest("Service Doctor4 Test");
		Web_GeneralFunctions.click(SMP.getServiceTariffAmttxtbx(Login_Doctor.driver, logger), "Dummy Click", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(SMP.getServiceDoctorMultiSelect(Login_Doctor.driver, logger), "click on doctor multi sel dropdown", Login_Doctor.driver, logger);
	    Web_GeneralFunctions.sendkeys(SMP.getServicedoctorSearchbx(Login_Doctor.driver, logger), "R", "Enter doctor name", Login_Doctor.driver, logger);
	    Web_GeneralFunctions.sendkeys(SMP.getServicedoctorSearchbx(Login_Doctor.driver, logger), "a", "Enter doctor name", Login_Doctor.driver, logger);
	    Web_GeneralFunctions.sendkeys(SMP.getServicedoctorSearchbx(Login_Doctor.driver, logger), "m", "Enter doctor name", Login_Doctor.driver, logger);
	    Web_GeneralFunctions.waitForElement(Login_Doctor.driver, SMP.getServiceDoctorOptions(Login_Doctor.driver, logger, doctor));
	    Web_GeneralFunctions.click(SMP.getServiceDoctorOptions(Login_Doctor.driver, logger, doctor), "Click on doctor", Login_Doctor.driver, logger);
	    Web_GeneralFunctions.wait(2);
	    Web_GeneralFunctions.scrollToElement(SMP.getServiceDoctorOptions(Login_Doctor.driver, logger, doctor), Login_Doctor.driver);
	    Web_GeneralFunctions.wait(2);

		}
	
	@Parameters ({"doctor1"})
	@Test(priority = 14,groups= {"Regression","Service_Save_Test"})
	public synchronized void Service_Save_Test(String doctor) throws Exception {

		logger = Reports.extent.createTest("Service Save Test");
	
		Web_GeneralFunctions.click(SMP.getServiceSaveBtn(Login_Doctor.driver, logger), "Click on Save btn", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		Assert.assertTrue(SMP.getService_toastmsg(Login_Doctor.driver, logger).isDisplayed(), "Toast message displayed");
	    }
	
	}
