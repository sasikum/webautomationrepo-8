package com.tatahealth.API.Billing;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.JsonObject;
import com.tatahealth.API.Core.CommonFunctions;

public class EMRAPI extends Login{
	
	
	private JSONObject patient = null;
	private String aId = null;
	private JSONObject service = null;
	private JSONObject tariff = null;
	private Integer serviceDiscountAmount = 0;
	private Integer additionDiscountAmount = 0;
	private Integer serviceQunatity = 1;
	//private JSONObject fetchDefaultServiceObj = null;
	
	public JSONObject getPatientDetails(String appointmentId) throws Exception{
		
		url = baseurl + "/HPPClinicsAppointmentsRestApp/rest/clinicAppointments/3.0/appointmentHomeDoctor";
		System.out.println("========="+appointmentId);
		CommonFunctions func = new CommonFunctions();
		List<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new BasicNameValuePair("tata_auth_token", doctorToken));
		headers.add(new BasicNameValuePair("userId", doctorSSOId));
		headers.add(new BasicNameValuePair("orgId", orgId));
		
		JSONObject param = new JSONObject();
		param.put("token", doctorToken);
		param.put("userLoginName", doctorLoginName);
		param.put("userId", clinicUserId);
		param.put("organisationId", orgId);
		param.put("clinicUserId", clinicUserId);
		param.put("appointmentDate", getTodaysDate());
		
		JSONObject responseObj = func.PostHeadersWithJSONRequest(url, param, headers);
		System.out.println("Response from EMRAPI booked slots"+ responseObj);
	
		JSONArray arr = responseObj.getJSONObject("bookedSlots").getJSONArray("bookedSlot");
		JSONArray patientArr = null;
		JSONObject patientObj = null;
		for(int i=0;i<arr.length();i++) {
			patientArr = arr.getJSONObject(i).getJSONObject("patientList").getJSONArray("patient");
			for(int j=0;j<patientArr.length();j++) {
				patientObj = patientArr.getJSONObject(j);
				Integer aId = Integer.parseInt(appointmentId);
				if(patientObj.getInt("appointmentId") == aId) {
					patient = patientObj;
				}
			}
		}
		System.out.println("patient Object"+patient);
		return patient;
	}
	
	public void getUserBasicDetails() throws Exception{
		
		url = baseurl + "/HPPClinicsCARestApp/rest/clinicAdministration/3.1/getUserBasicDetails";
		
		CommonFunctions func = new CommonFunctions();
		List<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new BasicNameValuePair("tata_auth_token", doctorToken));
		headers.add(new BasicNameValuePair("userId", doctorSSOId));
		headers.add(new BasicNameValuePair("orgId", orgId));
		headers.add(new BasicNameValuePair("platformId","3"));
		headers.add(new BasicNameValuePair("visitorId", "2a990ced1ba67df665a6360f22c8de01"));
		
	//	System.out.println("headers : "+headers);
		JSONObject param = new JSONObject();
		param.put("userLoginName", doctorLoginName);
		param.put("token", doctorToken);
		param.put("clinicUserId", clinicUserId);
		param.put("organisationId", orgId);
	//	System.out.println("params : "+param);
		JSONObject responseObj = func.PostHeadersWithJSONRequest(url, param, headers);
		
	//	System.out.println("response get user basic detail : "+responseObj);
		
		JSONArray arr = responseObj.getJSONArray("location");
		
		locationObj = arr.getJSONObject(0);
		userName = responseObj.getString("userName");
		
		
	}
	
	public void createOrUpdateVisitHandler(PaymentDetails p) throws Exception{
		String[] apptId = p.getAppointmentId().split("-");
		aId = apptId[1];
		getPatientDetails(apptId[1]);
		//System.out.println("appointment id : "+apptId[1]);
		//System.out.println("headers : "+headers);
		
		url = baseurl +"/HPPConsultationRestApp/rest/HPPClinicsConsultation/3.0/createOrUpdatePatientVisitHandler";
		
		CommonFunctions func = new CommonFunctions();
		List<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new BasicNameValuePair("tata_auth_token", doctorToken));
		headers.add(new BasicNameValuePair("userId", doctorSSOId));
		headers.add(new BasicNameValuePair("orgId", orgId));
		//headers.add(new BasicNameValuePair("Content-Type", "application/json"));
		//headers.add(new BasicNameValuePair("platformId", "3"));
	//	headers.add(new BasicNameValuePair("visitorId","f22c31bdfd3d5e0104560c4b734de236"));
		
		
		
		JSONObject param = new JSONObject();
		param.put("token", doctorToken);
		param.put("userLoginName", doctorLoginName);
		param.put("userId", clinicUserId);
		
		param.put("organizationId", orgId);
		Integer id = patient.getInt("patientId");
		param.put("patientId",id.toString());
		param.put("role", "PARAMEDIC");
		param.put("currentStatus", "Booked");
		param.put("appointmentId", aId);
		param.put("patientVisitId", "");
		
		param.put("operation", "CHECK-IN");
		param.put("siteId",locationObj.getString("siteId") );
		param.put("locationId", locationObj.getString("locationId"));
		param.put("patientMobileNo", patient.getString("mobileNo"));
		param.put("patientEmailId","" );
		param.put("doctorTitle", "");
		
		param.put("doctorName", userName);
		param.put("organizationName",locationObj.getString("name") );
		param.put("currentVisit","Y");
		param.put("clinicUserId", clinicUserId);
		
	//	System.out.println("param : "+param);
		//System.out.println("url : "+url);
		
		JSONObject responseObj = func.PostHeadersWithJSONRequest(url, param, headers);
		//System.out.println("create or visit : "+responseObj);
		
		visitId = responseObj.getString("visitId");
	
		
	}
	
	public void searchService() throws Exception{
		
		String searchText = "service";
		url = baseurl + "/HPPClinicsBillingRestApp/rest/clinicBilling/3.0/searchService";
		
		CommonFunctions func = new CommonFunctions();
		List<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new BasicNameValuePair("tata_auth_token", doctorToken));
		headers.add(new BasicNameValuePair("userId", doctorSSOId));
		headers.add(new BasicNameValuePair("orgId", orgId));
		
		JSONObject param = new JSONObject();
		param.put("token", doctorToken);
		param.put("userLoginName", doctorLoginName);
		param.put("searchText", searchText);
		param.put("organisationId", orgId);
		param.put("clinicUserId", clinicUserId);
		
		JSONObject responseObj = func.PostHeadersWithJSONRequest(url, param, headers);
		
		//System.out.println("search service : "+responseObj);
		
		JSONArray serv = responseObj.getJSONArray("service");
		service = serv.getJSONObject(0);
		
	}
	
	public void populateTariff() throws Exception{
		
		url = baseurl + "/HPPClinicsBillingRestApp/rest/clinicBilling/3.0/populateTariff";
		
		CommonFunctions func = new CommonFunctions();
		List<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new BasicNameValuePair("tata_auth_token", doctorToken));
		headers.add(new BasicNameValuePair("userId", doctorSSOId));
		headers.add(new BasicNameValuePair("orgId", orgId));
		
		JSONObject param = new JSONObject();
		param.put("token", doctorToken);
		param.put("userLoginName", doctorLoginName);
		param.put("locationId", locationObj.getString("locationId"));
		param.put("serviceId", service.getString("serviceId"));
		param.put("clinicUserId", clinicUserId);
		param.put("fromLab", service.getString("fromLab"));
		
		JSONObject responseObj = func.PostHeadersWithJSONRequest(url, param, headers);
		
	//	System.out.println("tariff : "+responseObj);
		
		tariff = responseObj;
		
	}
	
	public void fetchDefaultService() throws Exception{
		
	
		url = baseurl + "/HPPClinicsBillingRestApp/rest/clinicBilling/3.0/fetchDefaultServices";
		
		CommonFunctions func = new CommonFunctions();
		List<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new BasicNameValuePair("tata_auth_token", doctorToken));
		headers.add(new BasicNameValuePair("userId", doctorSSOId));
		headers.add(new BasicNameValuePair("orgId", orgId));
		
		JSONObject param = new JSONObject();
		param.put("token", doctorToken);
		param.put("userLoginName", doctorLoginName);
		param.put("organizationId", orgId);
		param.put("userId", clinicUserId);
		
		param.put("patientId",patient.getInt("patientId"));
		param.put("clinicUserId", clinicUserId);
		param.put("patientVisitId", visitId);
		
		JSONObject responseObj = func.PostHeadersWithJSONRequest(url, param, headers);
		
		System.out.println("Headers:"+headers);
		System.out.println("Request:"+ param);
		System.out.println("Response:"+responseObj);
		fetchDefaultServiceObj = responseObj;
		System.out.println("fetchDef : "+fetchDefaultServiceObj);
	}
	
	/*
	 * totally 4 cases.....generate bill no service, generate bill with service, generate bill with service discount and with additional discount.
	 * these cases are handle in this single method
	 * 
	 */
	public PaymentDetails generateBillWithoutService(PaymentDetails p,String billType) throws Exception{
		int otherTariff = 0;
		Double total = 0.0;
		Double amountPayable = 0.0;
		Double netPayable = 0.0;
		Double getTariff = 0.0;
		//System.out.println("Mode : "+p.getModeOfPayment());
		JSONObject consultationServiceObject = new JSONObject();
		JSONObject otherServiceObject = new JSONObject();
	
		CommonFunctions func = new CommonFunctions();
		List<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new BasicNameValuePair("tata_auth_token", doctorToken));
		headers.add(new BasicNameValuePair("userId", doctorSSOId));
		headers.add(new BasicNameValuePair("orgId", orgId));
		
		JSONObject param = new JSONObject();
		param.put("token", doctorToken);
		param.put("userLoginName", doctorLoginName);
		param.put("organisationId", orgId);
		param.put("userId", clinicUserId);
		param.put("siteId", locationObj.getString("siteId"));
		param.put("locationId", locationObj.getString("locationId"));
		param.put("patientId", fetchDefaultServiceObj.getInt("patientId"));
		param.put("uhid", fetchDefaultServiceObj.getInt("patientId"));
		param.put("patientName", userProfile.getString("firstName"));
		param.put("age", patient.getString("age"));
		param.put("gender",patient.getString("gender"));
		param.put("doctorId", clinicUserId);
		param.put("doctorName", userName);
		param.put("patientVisitId", visitId);
		param.put("isDraftBill", false);
		
		
		
		
		if(!billType.equalsIgnoreCase("No Service")) {
			searchService();
			populateTariff();
						
			//add extra service in service array obj
			JSONObject servObj = new JSONObject();
			servObj.put("serviceName", service.getString("serviceName"));
			servObj.put("serviceId", service.getString("serviceId"));
			servObj.put("receiptDtlId", "");
			servObj.put("serviceQuantity",serviceQunatity);
			servObj.put("serviceSource", "LAB");
			servObj.put("appDiscount", 0);
			servObj.put("disabled", false);
			servObj.put("tariffAmount", tariff.getString("tariffAmount"));
			servObj.put("paymentGatewayAmount", "0");
			servObj.put("rewardsAmount", "0");
			servObj.put("packageDiscountAmount", 0.0);
			
			
			//calculate service discount percentage
			 getTariff = Double.parseDouble(tariff.getString("tariffAmount"));
			if(billType.equalsIgnoreCase("With Service Discount")) {
				serviceDiscountAmount = 10;
				additionDiscountAmount = 0;
				//System.out.println("inside service discount");	
			}else if(billType.equalsIgnoreCase("With Additional Discount")) {
				serviceDiscountAmount = 10;
				additionDiscountAmount = 10;
			}else {
				serviceDiscountAmount = 0;
				additionDiscountAmount = 0;
			}
			
			p.setServiceDiscount(serviceDiscountAmount);
			p.setAdditionalDiscount(additionDiscountAmount);
			
			Double servicePercentage = (double) ((serviceDiscountAmount * 100)/(getTariff*serviceQunatity));
			servObj.put("serviceDiscountAmount", serviceDiscountAmount);
			servObj.put("serviceDiscountPercentage", servicePercentage.toString());
			
			//calculate service net amount ( tariff - serviceDiscount )
			getTariff -= serviceDiscountAmount;
			servObj.put("serviceNetAmount", getTariff);
		
			JSONArray arr = fetchDefaultServiceObj.getJSONObject("services").getJSONArray("service");
			arr.put(servObj);
			JSONObject serviceObj = new JSONObject();
			serviceObj.put("service", arr);
			param.put("services", serviceObj);
			
			
			//calculate total
			total = Double.parseDouble(fetchDefaultServiceObj.getString("total"))+getTariff;
			

			amountPayable = fetchDefaultServiceObj.getDouble("amountPayable");
			amountPayable += getTariff;
			
			
		//	System.out.println("amount payable : "+amountPayable);
			netPayable = total;
			
			Double additionTariffAmount = Double.parseDouble(tariff.getString("tariffAmount"));
			int additionalAmo = (int) Math.round(additionTariffAmount);
			p.setAdditionalServiceAmount(additionalAmo);
		
			//set other gross amount
			Double otherTari =Double.parseDouble(tariff.getString("tariffAmount"));
			otherTariff = (int) Math.round(otherTari);
			othersGrossAmount += otherTariff;
			
			
			//for clinic billing
			otherServiceObject.put("serviceName", service.getString("serviceName"));
			otherServiceObject.put("serviceAmount", tariff.getString("tariffAmount"));
			otherServiceObject.put("serviceDiscountAmount", serviceDiscountAmount.toString());
		//	otherServiceObject.put("payByCash)
			//add lab service additional discount,calculation changes
			
			
			
		}else {
			param.put("services", fetchDefaultServiceObj.getJSONObject("services"));
			total = Double.parseDouble(fetchDefaultServiceObj.getString("total"));
			amountPayable =  fetchDefaultServiceObj.getDouble("amountPayable");
			netPayable = Double.parseDouble(fetchDefaultServiceObj.getString("netPayable"));
			additionDiscountAmount = 0;
			serviceDiscountAmount = 0;
			
			String mop = p.getModeOfPayment();
			if(mop.contains	("Pay At Clinic") ) {
				JSONObject mopObj = new JSONObject();
				mopObj.put("type", "cc");
				//card payment
				mopObj.put("amount",amountPayable);
				JSONArray mopArr = new JSONArray();
				mopArr.put(mopObj);
				param.put("modeOfPayment", mopArr);
			}else {
				param.put("modeOfPayment", fetchDefaultServiceObj.getJSONArray("modeOfPayment"));
			}
		}
		
		
		
		
		//set emr gross amount and consultation service object
		JSONArray getService = fetchDefaultServiceObj.getJSONObject("services").getJSONArray("service");
		JSONObject ser = getService.getJSONObject(0);
		System.out.println("Service Object::"+getService);
		Double tar = Double.parseDouble(ser.getString("tariffAmount"));
		int EmrTariff = (int) Math.round(tar);
		consultationServiceObject.put("serviceName", ser.getString("serviceName"));
		consultationServiceObject.put("serviceAmount", ser.getString("tariffAmount"));
		
		
		//get service discount of the consultation service
		Double serviceTar = Double.parseDouble(ser.getString("serviceDiscountAmount"));
		int emrServiceDis = (int) Math.round(serviceTar);  
		consultationServiceObject.put("serviceDiscountAmount",serviceTar.toString());
		
		//get app discount(package/coupon discount) of the consultation service
		Double serviceAppDisc = ser.getDouble("appDiscount");
		int appDisc = (int) Math.round(serviceAppDisc);
		
		
 		EMRGrossAmount += EmrTariff;
 		
 				
 		//get online pay
 		Double onlinePaidEMR = fetchDefaultServiceObj.getDouble("paidOnline");
 		int onlinePay = (int)Math.round(onlinePaidEMR);
		consultationServiceObject.put("paidOnline", onlinePaidEMR.toString());
 		
 		EmrTariff -= appDisc;
 		EmrTariff -= onlinePay;
 		
 		Double packageDiscountAmount = 0.0;
 		Integer couponAppDiscount = 0;
 		Double paymentGatewayAmount = 0.0;
 		Double rewardAmount = 0.0;
 		String packageCouponName = "";
 		
 		System.out.println("\n\n== MODE OF PAYMENT =="+ p.getModeOfPayment());
 		if(p.getModeOfPayment().contains("Fit Coin")) {
 			rewardAmount = (Double) ser.get("rewardsAmount");
 		}
 		//System.out.println("reward amount :"+rewardAmount);
 		
 		if(p.getModeOfPayment().contains("Package")) {
 			packageDiscountAmount = ser.getDouble("packageDiscountAmount");
 			packageCouponName = fetchDefaultServiceObj.getString("packageCouponName");
 		}
 		consultationServiceObject.put("packageDiscountAmount", packageDiscountAmount.toString());
 		
 		if(p.modeOfPayment.contains("Coupon")) {
 			couponAppDiscount = ser.getInt("appDiscount");
 			packageCouponName = fetchDefaultServiceObj.getString("packageCouponName");
 		}
 		consultationServiceObject.put("couponAppDiscount", couponAppDiscount.toString());
 		
 		if(p.modeOfPayment.contains("Online Payment")) {
 			paymentGatewayAmount = ser.getDouble("paymentGatewayAmount");
 		}
 		
 		consultationServiceObject.put("packageCouponName", packageCouponName);
 		
 		Double packageCouponDiscount = packageDiscountAmount + couponAppDiscount;
 		Double onlinePaidAmount = paymentGatewayAmount + rewardAmount;
 		//Integer appDiscount = (int) Math.round(onlinePaidAmount); //commented by deiva
 		Double additionOfDiscount =  packageCouponDiscount + onlinePaidAmount; 
 		
 		//Following 2 lines are added by Deiva - "do not know why appDiscount includes paymentGatewayAmount"
 		String appDiscountS = String.valueOf(packageDiscountAmount + couponAppDiscount+rewardAmount);
 		consultationServiceObject.put("appDiscount", appDiscountS);
 		
 		if(!p.modeOfPayment.contains("Pay At Clinic")) {
 			amountPayable -= additionOfDiscount;
 		}
 		
		//total
		param.put("total", total.toString());
		
		
		//calculate additional discount
		Double additionalPercentage = (double) ((additionDiscountAmount * 100)/(total));
		if(additionalPercentage.isNaN() || additionalPercentage.toString().equals("Nan")) {
			additionalPercentage = 0.0;
		}
		//System.out.println("discount percentage : "+additionalPercentage + " "+additionDiscountAmount);
		param.put("applicableDiscount", additionalPercentage.toString());
		param.put("discountAmount", additionDiscountAmount.toString());
		
		//calculate netPayable
		netPayable -= additionDiscountAmount;	
		param.put("netPayable", netPayable);
		
		
		param.put("paidOnline", fetchDefaultServiceObj.getInt("paidOnline"));
		//System.out.println("app discount : "+appDiscount + " amount payable : "+amountPayable);
		//calculate amount payable ( total - (paid online + appDiscount) - additional discountAmount
		//Integer amountPay = fetchDefaultServiceObj.getInt("paidOnline");
		//amountPayable -= amountPay;
		amountPayable -= additionDiscountAmount;
		param.put("amountPayable", amountPayable);
		
		//EMR Report calculation
		DecimalFormat df = new DecimalFormat("#.##");

		//calculate emr discount amount
		//System.out.println("EMR Discount : "+EMRDiscountAmount);
		Double addtionalDiscountCalculationPerService = discountCalulation(EmrTariff,emrServiceDis,additionalPercentage,appDisc);
		EMRDiscountAmount += addtionalDiscountCalculationPerService;
		EMRDiscountAmount = Double.valueOf(df.format(EMRDiscountAmount));
		//consultationServiceObject.put("additionalDiscountAmount", addDiscAmount.toString());
		consultationServiceObject.put("additionalDiscountAmount", addtionalDiscountCalculationPerService.toString());
		
		
		//calculate other discount amount
		if(EmrTariff == 0) {
			othersDiscountAmount += serviceDiscountAmount + additionDiscountAmount;
			consultationServiceObject.put("payByCash","0.00");
		}else {
			addtionalDiscountCalculationPerService = discountCalulation(otherTariff,serviceDiscountAmount,additionalPercentage,0);
			othersDiscountAmount += addtionalDiscountCalculationPerService;
			othersDiscountAmount = Double.valueOf(df.format(othersDiscountAmount));
			EmrTariff -= addtionalDiscountCalculationPerService;
			consultationServiceObject.put("payByCash",Double.toString(EmrTariff));
		}
		
	/*
	 * 
	 * 	below 4 line code is for emr reports
	 */
		//set emr net billed amount 
		EMRNetBilledAmount = EMRGrossAmount - EMRDiscountAmount;
		EMRNetBilledAmount = Double.valueOf(df.format(EMRNetBilledAmount));
		
		//set other net billed amount
		othersNetBilledAmount = othersGrossAmount - othersDiscountAmount;
		othersNetBilledAmount = Double.valueOf(df.format(othersNetBilledAmount));
		
		
		//set emr service object
	/*	int netAmount = 0;
		Double addDiscAmount = 0.0;
		//String payByCash = "";
		Double cashPaid = 0.0;
		if(!billType.equalsIgnoreCase("No Service")) {
			Integer serviceNetAmount = (int) Math.round(getTariff);
			 addDiscAmount = calculateAdditionalDiscount(serviceNetAmount, additionalPercentage);
			//String payByCash = "";
			 cashPaid = serviceNetAmount - addDiscAmount;
			otherServiceObject.put("additionalDiscountAmount", addDiscAmount.toString());
			otherServiceObject.put("payByCash", cashPaid.toString());
			p.setExtraService(otherServiceObject);
		}
		
		Double getTotalFromDefaultService = Double.parseDouble(fetchDefaultServiceObj.getString("total"));
		Integer getTotal = (int) Math.round(getTotalFromDefaultService);
		netAmount = (int) (getTotal - fetchDefaultServiceObj.getInt("paidOnline"));
		 addDiscAmount = calculateAdditionalDiscount(netAmount, additionalPercentage);
		//String payByCash = "";
		cashPaid = netAmount - addDiscAmount;
		
		
		//consultationServiceObject.put("payByCash", cashPaid.toString());*/
		p.setConsultationService(consultationServiceObject);
		
		/*
		 * the code inside condition generates type of payment done by consumer like "400C(coupon)+100Pc(pay by cash" 
		 * 
		 */
		if(!billType.equalsIgnoreCase("No Service")) {
			JSONObject mopObj = new JSONObject();
			mopObj.put("type", "cc");
			mopObj.put("amount",amountPayable);
			JSONArray mopArr = new JSONArray();
			mopArr.put(mopObj);
			param.put("modeOfPayment", mopArr);
			String getAmount = null;
			Double paidAmount = 0.0;
			String servicePaid = "";
			String x = "";
			//System.out.println("paid inside if : "+ p.getPaid());
			if(p.getPaid().contains("+")) {
				System.out.println("inside  + if");
				String[] paidupdate = p.getPaid().split("\\+");
				if(p.getPaid().contains("/PC") ) {
					//System.out.println("+ inside pc");
					for(int j=0;j<paidupdate.length;j++) {
						//System.out.println("service paid pc :"+servicePaid);
						if(paidupdate[j].contains("/PC")) {
							//System.out.println("inside if pc for loop :");
							// getAmount = paidupdate[j].substring(0, paidupdate[j].length()-3);
							 paidAmount = amountPayable;
							 x = paidAmount.toString().substring(0, paidAmount.toString().length()-2);
							// System.out.println("inside x : "+ x);
							 paidupdate[j] = x+"/PC";
							//p.setPaid();
						}
						servicePaid += paidupdate[j]+"+";
					}
					//System.out.println(" inside pc : "+servicePaid);
				}else {
					x = amountPayable.toString().substring(0, amountPayable.toString().length()-2) + "/PC+";
					//System.out.println(" + inside else x :"+x);
					for(int j=0;j<paidupdate.length;j++) {
						if(!servicePaid.contains("/PC")) {
							//System.out.println("+ inside else for if not contains pc");
							if(paidupdate[j].contains("/F") || paidupdate[j].contains("/O")) {
								String service = x + paidupdate[j]+"+";
								servicePaid += service;
								
							}else {
								servicePaid += paidupdate[j]+"+"+x;
							}
						}else {
							servicePaid += paidupdate[j]+"+";
						}
						
					}
				}
	
				servicePaid = servicePaid.substring(0,servicePaid.length()-1);
				//System.out.println("if service paid : "+ servicePaid);
				p.setPaid(servicePaid);
			}else if(p.getPaid().contains("/PC")) {
				//System.out.println("inside single pc");
				x = amountPayable.toString().substring(0, amountPayable.toString().length()-2);
				//System.out.println("inside single pc x value: "+x);
				servicePaid = x+"/PC+";
				servicePaid = servicePaid.substring(0,servicePaid.length()-1);
				//System.out.println("else if service paid : "+servicePaid);
				p.setPaid(servicePaid);
			}
			else {
				//System.out.println(" inside else add pc");
				x = amountPayable.toString().substring(0, amountPayable.toString().length()-2);
				if(p.getModeOfPayment().equalsIgnoreCase("Fit Coin") || p.getModeOfPayment().equalsIgnoreCase("Online Payment")) {
					//p.setPaid();
					servicePaid = x+"/PC+"+p.getPaid();
				}else {
					//p.setPaid();
					servicePaid = p.getPaid()+"+"+x+"/PC";
				}
				//servicePaid = servicePaid.substring(0,servicePaid.length()-1);
				//System.out.println("else service paid : "+servicePaid);
				p.setPaid(servicePaid);
			}
			//p.setPaid(p.getPaid()+"+"+amountPayable+"/cc");
		}
		
	
		url = baseurl + "/HPPClinicsBillingRestApp/rest/clinicBilling/3.0/createBill";
		
		JSONObject responseObj = func.PostHeadersWithJSONRequest(url, param, headers);
		
		//System.out.println("generate bill : "+responseObj);
		
		message = responseObj.getString("successMessage");
		
		billNumber = responseObj.getString("billNumber");
		
		p.setBillId(Integer.parseInt(billNumber));
		
		//System.out.println(message);
	
		return p;
	}
	
	/*

	public PaymentDetails generateBillWithAdditionalService(PaymentDetails p) throws Exception{
		
		//System.out.println("Mode : "+p.getModeOfPayment());
		
		searchService();
		populateTariff();
		
		JSONObject servObj = new JSONObject();
		servObj.put("serviceName", service.getString("serviceName"));
		servObj.put("serviceId", service.getString("serviceId"));
		servObj.put("receiptDtlId", "");
		servObj.put("serviceQuantity","1");
		servObj.put("serviceDiscountPercentage", "0.0");
		servObj.put("serviceDiscountAmount", "0.0");
		servObj.put("serviceNetAmount", tariff.getString("tariffAmount"));
		servObj.put("serviceSource", "EMR");
		servObj.put("appDiscount", 0);
		servObj.put("disabled", false);
		JSONArray arr = fetchDefaultServiceObj.getJSONObject("services").getJSONArray("service");
		arr.put(servObj);
		
		JSONObject serviceObj = new JSONObject();
		serviceObj.put("service", arr);
		
		Integer amountPayable = 0;
		if(fetchDefaultServiceObj.get("amountPayable") != null) {
			amountPayable = (Integer) fetchDefaultServiceObj.get("amountPayable");
		}
		amountPayable += Integer.parseInt(tariff.getString("tariffAmount"));
		url = environment + "/HPPClinicsBillingRestApp/rest/clinicBilling/3.0/createBill";
		
		CommonFunctions func = new CommonFunctions();
		List<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new BasicNameValuePair("tata_auth_token", doctorToken));
		headers.add(new BasicNameValuePair("userId", doctorSSOId));
		headers.add(new BasicNameValuePair("orgId", orgId));
		
		JSONObject param = new JSONObject();
		param.put("token", doctorToken);
		param.put("userLoginName", doctorLoginName);
		param.put("organisationId", orgId);
		param.put("userId", clinicUserId);
		param.put("siteId", locationObj.getString("siteId"));
		param.put("locationId", locationObj.getString("locationId"));
		
		param.put("patientId", fetchDefaultServiceObj.getInt("patientId"));
		param.put("uhid", fetchDefaultServiceObj.getInt("patientId"));
		param.put("patientName", userProfile.getString("firstName"));
		param.put("total", fetchDefaultServiceObj.getString("total"));
		param.put("age", patient.getString("age"));
		param.put("gender",patient.getString("gender"));
		param.put("applicableDiscount", fetchDefaultServiceObj.getString("applicableDiscount"));
		param.put("discountAmount", fetchDefaultServiceObj.getString("discountAmount"));
		param.put("netPayable", fetchDefaultServiceObj.getString("netPayable"));
		param.put("doctorId", clinicUserId);
		param.put("doctorName", userName);
		param.put("patientVisitId", visitId);
	
		param.put("services", serviceObj);
		param.put("isDraftBill", false);
		param.put("paidOnline", fetchDefaultServiceObj.getInt("paidOnline"));
		param.put("amountPayable", );
	
		
		String mop = p.getModeOfPayment();
		if(mop.contains	("Pay At Clinic") ) {
			JSONObject mopObj = new JSONObject();
			mopObj.put("type", "cc");
			mopObj.put("amount", fetchDefaultServiceObj.get("amountPayable"));
			JSONArray mopArr = new JSONArray();
			mopArr.put(mopObj);
			param.put("modeOfPayment", mopArr);
		}
	
		JSONObject responseObj = func.PostHeadersWithJSONRequest(url, param, headers);
		
		//System.out.println("generate bill : "+responseObj);
		
		message = responseObj.getString("successMessage");
		
		billNumber = responseObj.getString("billNumber");
		
		p.setAppointmentAndBillId(aId + " - "+billNumber);
		
		//System.out.println(message);
	
		return p;
	}
	
	
public PaymentDetails generateBillWithServiceDiscount(PaymentDetails p) throws Exception{
		
		Integer discountAmount = 10;
		String serviceDiscount = calculateDiscountPercentage(discountAmount, tariff.getString("tariffAmount"));
		//String percent = 
		
		//System.out.println("Mode : "+p.getModeOfPayment());
		
		searchService();
		populateTariff();
		
		JSONObject servObj = new JSONObject();
		servObj.put("serviceName", service.getString("serviceName"));
		servObj.put("serviceId", service.getString("serviceId"));
		servObj.put("receiptDtlId", "");
		servObj.put("serviceQuantity","1");
		servObj.put("serviceDiscountPercentage", serviceDiscount);
		servObj.put("serviceDiscountAmount", discountAmount.toString());
		servObj.put("serviceNetAmount", tariff.getString("tariffAmount"));
		servObj.put("serviceSource", "EMR");
		servObj.put("appDiscount", 0);
		servObj.put("disabled", false);
		JSONArray arr = fetchDefaultServiceObj.getJSONObject("services").getJSONArray("service");
		arr.put(servObj);
		
		JSONObject serviceObj = new JSONObject();
		serviceObj.put("service", arr);
		
		
		url = environment + "/HPPClinicsBillingRestApp/rest/clinicBilling/3.0/createBill";
		
		CommonFunctions func = new CommonFunctions();
		List<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new BasicNameValuePair("tata_auth_token", doctorToken));
		headers.add(new BasicNameValuePair("userId", doctorSSOId));
		headers.add(new BasicNameValuePair("orgId", orgId));
		
		JSONObject param = new JSONObject();
		param.put("token", doctorToken);
		param.put("userLoginName", doctorLoginName);
		param.put("organisationId", orgId);
		param.put("userId", clinicUserId);
		param.put("siteId", locationObj.getString("siteId"));
		param.put("locationId", locationObj.getString("locationId"));
		
		param.put("patientId", fetchDefaultServiceObj.getInt("patientId"));
		param.put("uhid", fetchDefaultServiceObj.getInt("patientId"));
		param.put("patientName", userProfile.getString("firstName"));
		param.put("total", fetchDefaultServiceObj.getString("total"));
		param.put("age", patient.getString("age"));
		param.put("gender",patient.getString("gender"));
		param.put("applicableDiscount", fetchDefaultServiceObj.getString("applicableDiscount"));
		param.put("discountAmount", fetchDefaultServiceObj.getString("discountAmount"));
		param.put("netPayable", fetchDefaultServiceObj.getString("netPayable"));
		param.put("doctorId", clinicUserId);
		param.put("doctorName", userName);
		param.put("patientVisitId", visitId);
	
		param.put("services", serviceObj);
		param.put("isDraftBill", false);
		param.put("paidOnline", fetchDefaultServiceObj.getInt("paidOnline"));
		param.put("amountPayable", fetchDefaultServiceObj.get("amountPayable"));
		
	
		
		String mop = p.getModeOfPayment();
		if(mop.contains	("Pay At Clinic") ) {
			JSONObject mopObj = new JSONObject();
			mopObj.put("type", "cc");
			mopObj.put("amount", fetchDefaultServiceObj.get("amountPayable"));
			JSONArray mopArr = new JSONArray();
			mopArr.put(mopObj);
			param.put("modeOfPayment", mopArr);
		}
	
		JSONObject responseObj = func.PostHeadersWithJSONRequest(url, param, headers);
		
		//System.out.println("generate bill : "+responseObj);
		
		message = responseObj.getString("successMessage");
		
		billNumber = responseObj.getString("billNumber");
		
		p.setAppointmentAndBillId(aId + " - "+billNumber);
		
		//System.out.println(message);
	
		return p;
	}




	public String calculateDiscountPercentage(Integer discountAmount,String tariff) throws Exception{
		String percent = null;
		Integer tar = Integer.parseInt(tariff);
		
		Double x = (double) ((discountAmount*100)/tar);
		
		percent = x.toString();
		
		return percent;
	}*/
	
	
	public Double discountCalulation(int serviceAmount,int serviceDiscount,Double additionalPercentage,int appDisc) {
		Double discAmount = 0.0;
		
		//System.out.println("additional percentage :"+additionalPercentage);
		
		//additionalPercentage = Double.parseDouble()
		DecimalFormat decimalFormat = new DecimalFormat("#.####");
		additionalPercentage = Double.valueOf(decimalFormat.format(additionalPercentage));
		//System.out.println("additional percentage after format:"+additionalPercentage);
		
		Double additionalPercentageAmount =  ((serviceAmount - serviceDiscount) * additionalPercentage/100);
		
		//System.out.println("Additional percentage amount for full payment : "+additionalPercentageAmount);
		
		discAmount = serviceDiscount + additionalPercentageAmount+appDisc; 
	//	System.out.println("Before decimal and ceil process : "+discAmount);
		
		decimalFormat = new DecimalFormat("#.###");
		discAmount = Double.valueOf(decimalFormat.format(discAmount));
		//System.out.println("Before Up : "+discAmount);
		decimalFormat.setRoundingMode(RoundingMode.UP);
		decimalFormat = new DecimalFormat("#.##");
		discAmount = Double.valueOf(decimalFormat.format(discAmount));
		//System.out.println("after up : "+discAmount);
		//discAmount = Math.ceil(discAmount);*/	
		
		//System.out.println("Discount calcultation : "+ discAmount);
		return discAmount;
	}
	

	
	public Double calculateAdditionalDiscount(int amountPayable,double additionalDiscountPercentage) {
		double servicePaid = 0.0;
		
		servicePaid = (amountPayable * additionalDiscountPercentage)/100;
		
		return servicePaid;
	}
	
	
}
