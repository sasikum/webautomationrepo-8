package com.tatahealth.API.Core;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import com.tatahealth.API.Billing.Login;

public class CCAvenueDetails extends Login{
	
	
	
	public void getCCAvenueResponse(String orderType) throws Exception{
		url = baseurl + "/consultation/api/v1/getCCavDetails";
		
		CommonFunctions func = new CommonFunctions();
		
		List<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new BasicNameValuePair("userId", consumerUserId));
		headers.add(new BasicNameValuePair("tata_auth_token", consumerToken));
		
		JSONObject param = new JSONObject();
		param.put("consumerUserId", consumerUserId);
		param.put("orderType", orderType);
		
		JSONObject responseObj = func.PostHeadersWithJSONRequest(url, param, headers);
		orderId = responseObj.getJSONObject("responseData").getString("orderId");
	}

	public void saveCCAvenueResponse(String amount) throws Exception{
		Random r = new Random();
		Integer randomNumber = 0;
		int high = 9999999;
		int low = 999999;
		String random = null;
		//StringBuilder sb = new StringBuilder();
		
		CommonFunctions func = new CommonFunctions();
		
		url = baseurl + "/consultation/api/v2/saveCCavResponse";
		
		List<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new BasicNameValuePair("userId", consumerUserId));
		headers.add(new BasicNameValuePair("tata_auth_token", consumerToken));
		headers.add(new BasicNameValuePair("misc", userDOB));
		
		JSONObject param = new JSONObject();
		param.put("action", "");
		param.put("amount", amount);
		
		//generate random bank refernce no
		randomNumber = r.nextInt(high-low)+low;
		random = "555555"+randomNumber.toString();
		bankRefernceNo = random;
		
		param.put("bankRefNo", random);
		
		
		JSONObject billingAddress = new JSONObject();
		random = RandomStringUtils.randomAlphabetic(6);
		billingAddress.put("billingAddress", random);
		
		random = RandomStringUtils.randomAlphabetic(6);
		billingAddress.put("billingCity", random);
		
		random = RandomStringUtils.randomAlphabetic(6);
		billingAddress.put("billingCountry", random);
		
		
		random = RandomStringUtils.randomAlphabetic(6);
		billingAddress.put("billingName", random);
		
		
		random = RandomStringUtils.randomAlphabetic(6);
		billingAddress.put("billingState", random);
		
		random = RandomStringUtils.randomAlphabetic(6);
		billingAddress.put("billingZip", random);
		
		random = RandomStringUtils.randomAlphabetic(6)+"@"+RandomStringUtils.randomAlphabetic(3)+"."+RandomStringUtils.randomAlphabetic(3);
		billingAddress.put("billingEmail", random);
	
		billingAddress.put("billingTel", mobileNumber);
		
		param.put("billingAddress", billingAddress);
		
		param.put("cardName", "AvenuesTest");
		param.put("consumerUserId", consumerUserId);
		param.put("currency", "INR");
		param.put("discountValue", "0.0");
		param.put("eciValue", "null");
		param.put("failureMsg", "");
		param.put("merAmount", packageAmount);
		param.put("offerCode", "null");
		param.put("offerType", "null");
		param.put("orderDescription", "");
		param.put("orderId", orderId);
		param.put("orderStatus", "Success");
		param.put("paymentMode", "Net Banking");
		param.put("responseCode", "0");
		param.put("retry", "N");
		param.put("statusCode", "null");
		param.put("statusMessage", "Y");
		
		randomNumber = r.nextInt(high-low)+low;
		random = "55555"+randomNumber.toString();
		trackingId = random;
		param.put("trackingId", random);
		param.put("vault", "N");
		
		
		JSONObject shiipingAddress = new JSONObject();
		shiipingAddress.put("shippingAddress", "");
		shiipingAddress.put("shippingCity", "");
		shiipingAddress.put("shippingCountry", "");
		shiipingAddress.put("shippingEmail", "");
		shiipingAddress.put("shippingName", "");
		shiipingAddress.put("shippingState", "");
		shiipingAddress.put("shippingTel", "");
		shiipingAddress.put("shippingZip", "");
		param.put("shippingAddress", shiipingAddress);
		
		JSONObject objParam1 = new JSONObject();
		objParam1.put("paramKey", "merchant_param1");
		objParam1.put("paramValue", "");
		
		JSONObject objParam2 = new JSONObject();
		objParam2.put("paramKey", "merchant_param2");
		objParam2.put("paramValue", "");
		
		JSONArray merchantParams = new JSONArray();
		merchantParams.put(objParam1);
		merchantParams.put(objParam2);
		param.put("merchantParams",merchantParams );
		
		String encKey = "TATADHP-"+userDOB;
		//System.out.println("key : "+encKey);
		Encryption enc = new Encryption();
		String convertJson = param.toString();
		
		String text = enc.encrypt(convertJson, encKey);
		
		//System.out.println("cc avencue headers : "+text);
		
		JSONObject responseObj = func.PostHeaderWithTextRequest(url, text, headers);
		message = responseObj.getJSONObject("status").getString("message");
			
		
	}
}
